# YourSimpleShop
It is a simple application to management your olx account, offers etc.

## Getting started
You can install as normal symfony application. Download code and run
```
composer install
```
Next configure your database in `.env.local` file and migrate database with
```
php bin/console doctrine:migrations:migrate
```
Application should works.

## Code quality
### Rules
Here is several rules I follow
- KISS rules
- SOLID rules
- PHPUnit tests
- design patterns
- clean architecture
- low coupling, high cohesion
- modularity / bounded contexts

### Architecture
Each module / bounded context has own architecture style. There can be simple one layer or more than one layers or mixture of different styles. For example layered and hexagonal.

### Modularity
Modules have own API to communicate with others. They also can do it with events.  
To keep modularity autonomic I used deptrac tool. It is attached to CI/CD and analyse borders between modules.

