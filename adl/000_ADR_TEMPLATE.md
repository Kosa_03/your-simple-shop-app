## Architecture decision title

**Created**: 2019-06-01 - Date when ARD was created

## Context
- Architecture drivers
- Description
- Some comparison

## Status
ACCEPTED (PROPOSED, REJECTED, EXPIRED, PAST DUE)

## Decision

**When** 2019-12-06 - Date when decision was accepted or not.

Decision and reason.

## Consequences

Describes the effects of the change. 
What becomes easier? What will be more difficult?

## Other options

Other possibilities.