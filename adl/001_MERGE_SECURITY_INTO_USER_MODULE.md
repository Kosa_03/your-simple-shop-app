## Merge security into user module

**Created**: 2023-07-31

## Context

We have security and user modules which communicate each other by Api layer in User module.
It is a partial boundary. We need to either change a partial on a full boundary (ACL)
or merge these modules into one.

### Drivers
- Simply and quality code.

## Status

ACCEPTED

## Decision

**When** 2023-07-31

We decided to merge these two modules, because they communicate frequently.
The second reason is the security is a part of the user.
They should live together.

## Consequences

One module with enriched user aggregate.


## Other options

- ACL User > Security
