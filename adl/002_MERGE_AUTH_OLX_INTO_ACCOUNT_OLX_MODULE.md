## Merge auth olx into account olx module

**Created**: 2023-08-07

## Context

We have auth olx and account olx modules which communicate each other by ACL.
It is a full boundary. Auth olx module needs almost all account properties.
So it looks like they should live in a one bounded context.

### Drivers
- Simply and quality code.
- Remove complexity.

## Status

ACCEPTED

## Decision

**When** 2023-08-07

We decided to merge these two modules, because one of them using almost all 
properties from the second one. So they should live together.

## Consequences

An account domain with enriched aggregate.
It enables moving an authorization part to an account subdomain in the future.

## Other options

Nothing