## CQRS with messenger

**Created**: 2023-08-12

## Context

We have CQRS without messenger, based on simple command and query objects.
Each command includes database transaction inside. This is a part of the code
you should remember about. We could move the transaction responsibility to
the symfony messenger.


### Drivers
- Simply and quality code.
- Remove complexity.

## Status

ACCEPTED

## Decision

**When** 2023-08-12

We decided to implement a symfony messenger to CQRS. Handlers will look simpler.
The messenger is a better tool than commands and queries wrote by own.

## Consequences

- Looks more professional.
- Fewer dependencies in other services.
- It enables to do commands asynchronously in simple way.
- Solution is more flexible.

## Other options

Nothing