## Application layer

**Created**: 2023-08-16

## Context

We dont have an application layer.  
Based on https://ademcatamak.medium.com/layers-in-ddd-projects-bd492aa2b8aa
and droganowoczesnegoarchitekta.pl and to be consistent with DDD approach,
we should add this layer to a project.

### Drivers
- Quality code consistent with Domain Driven Design.

## Status

ACCEPTED

## Decision

**When** 2023-08-16

We decided to separate the application layer from an infrastructure one.
The reason is an approach consistent with DDD.

## Consequences

- Looks more professional.
- Application services are separated from an infrastructure.
- Now the instrastructure can keep only objects related to framework
  or external services.

## Other options

Nothing
