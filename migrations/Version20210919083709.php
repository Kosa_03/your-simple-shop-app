<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210919083709 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE rest_api_olx_account (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(128) NOT NULL, client_id INT NOT NULL, client_secret VARCHAR(100) NOT NULL, redirect_url VARCHAR(100) NOT NULL, refresh_token LONGTEXT DEFAULT NULL, access_token VARCHAR(255) DEFAULT NULL, expiry_at DATETIME DEFAULT NULL, created DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL, UNIQUE INDEX UNIQ_E49C4187F85E0677 (name), UNIQUE INDEX UNIQ_E49C418719EB6921 (client_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE rest_api_olx_account');
    }
}
