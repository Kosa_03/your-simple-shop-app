<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211122191459 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE rest_api_olx_account ADD state VARCHAR(20) DEFAULT NULL AFTER redirect_url');
        $this->addSql(
            'ALTER TABLE rest_api_olx_account ADD state_created_at DATETIME NULL AFTER state'
        );
        $this->addSql('ALTER TABLE rest_api_olx_account RENAME INDEX uniq_e49c4187f85e0677 TO UNIQ_D4CD198F5E237E06');
        $this->addSql('ALTER TABLE rest_api_olx_account RENAME INDEX uniq_e49c418719eb6921 TO UNIQ_D4CD198F19EB6921');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE rest_api_olx_account DROP state');
        $this->addSql('ALTER TABLE rest_api_olx_account DROP state_created_at');
        $this->addSql('ALTER TABLE rest_api_olx_account RENAME INDEX uniq_d4cd198f19eb6921 TO UNIQ_E49C418719EB6921');
        $this->addSql('ALTER TABLE rest_api_olx_account RENAME INDEX uniq_d4cd198f5e237e06 TO UNIQ_E49C4187F85E0677');
    }
}
