<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211226221134 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE rest_api_olx_category_attribute (id INT UNSIGNED AUTO_INCREMENT NOT NULL, category_id INT UNSIGNED NOT NULL, code VARCHAR(128) NOT NULL, label VARCHAR(128) NOT NULL, unit VARCHAR(16) DEFAULT NULL, type VARCHAR(128) NOT NULL, required TINYINT(1) NOT NULL, `numeric` TINYINT(1) NOT NULL, min INT DEFAULT NULL, max INT DEFAULT NULL, multiple TINYINT(1) NOT NULL, `values` JSON NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE rest_api_olx_category_attribute');
    }
}
