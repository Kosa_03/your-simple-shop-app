<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220418101621 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE app_user RENAME COLUMN is_enabled TO active');
        $this->addSql('ALTER TABLE app_user RENAME COLUMN created TO created_at');
        $this->addSql('ALTER TABLE app_user RENAME COLUMN modified TO updated_at');

        $this->addSql('ALTER TABLE app_user_recovery_token MODIFY token_id INT NOT NULL');
        $this->addSql('ALTER TABLE app_user_recovery_token DROP PRIMARY KEY');

        $this->addSql('ALTER TABLE app_user_recovery_token CHANGE COLUMN token_id id INT NOT NULL AUTO_INCREMENT PRIMARY KEY');
        $this->addSql('ALTER TABLE app_user_recovery_token RENAME COLUMN expiry_date TO expiry_at');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE app_user RENAME COLUMN active TO is_enabled');
        $this->addSql('ALTER TABLE app_user RENAME COLUMN created_at TO created');
        $this->addSql('ALTER TABLE app_user RENAME COLUMN updated_at TO modified');

        $this->addSql('ALTER TABLE app_user_recovery_token MODIFY id INT NOT NULL');
        $this->addSql('ALTER TABLE app_user_recovery_token DROP PRIMARY KEY');

        $this->addSql('ALTER TABLE app_user_recovery_token CHANGE id token_id INT NOT NULL AUTO_INCREMENT PRIMARY KEY');
        $this->addSql('ALTER TABLE app_user_recovery_token RENAME COLUMN expiry_at TO expiry_date');
    }
}
