<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220418121940 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE rest_api_olx_account RENAME olx_account');
        $this->addSql('ALTER TABLE rest_api_olx_category RENAME olx_category');
        $this->addSql('ALTER TABLE rest_api_olx_category_attribute RENAME olx_category_attribute');

        $this->addSql('ALTER TABLE olx_account RENAME COLUMN created TO created_at');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE olx_account RENAME rest_api_olx_account');
        $this->addSql('ALTER TABLE olx_category RENAME rest_api_olx_category');
        $this->addSql('ALTER TABLE olx_category_attribute RENAME rest_api_olx_category_attribute');

        $this->addSql('ALTER TABLE rest_api_olx_account RENAME COLUMN created_at TO created');
    }
}
