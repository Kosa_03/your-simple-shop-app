<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220925202445 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE app_user RENAME user');
        $this->addSql('ALTER TABLE app_user_recovery_token RENAME user_recovery_token');
        $this->addSql('ALTER TABLE user_recovery_token CHANGE app_user_id user_id INT NOT NULL');
        $this->addSql('ALTER TABLE user RENAME INDEX uniq_88bdf3e9aa08cb10 TO UNIQ_8D93D649AA08CB10');
        $this->addSql('ALTER TABLE user RENAME INDEX uniq_88bdf3e9e7927c74 TO UNIQ_8D93D649E7927C74');
        $this->addSql('ALTER TABLE user_recovery_token RENAME INDEX uniq_7e5652dd4a3353d8 TO UNIQ_20E8BA18A76ED395');
        $this->addSql('ALTER TABLE user_recovery_token DROP FOREIGN KEY FK_7E5652DD4A3353D8');
        $this->addSql('ALTER TABLE user_recovery_token ADD CONSTRAINT FK_20E8BA18A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE user RENAME app_user');
        $this->addSql('ALTER TABLE user_recovery_token RENAME app_user_recovery_token');
        $this->addSql('ALTER TABLE app_user_recovery_token CHANGE user_id app_user_id INT NOT NULL');
        $this->addSql('ALTER TABLE app_user RENAME INDEX uniq_8d93d649aa08cb10 TO UNIQ_88BDF3E9AA08CB10');
        $this->addSql('ALTER TABLE app_user RENAME INDEX uniq_8d93d649e7927c74 TO UNIQ_88BDF3E9E7927C74');
        $this->addSql('ALTER TABLE app_user_recovery_token RENAME INDEX uniq_20e8ba18a76ed395 TO UNIQ_7E5652DD4A3353D8');
        $this->addSql('ALTER TABLE app_user_recovery_token DROP FOREIGN KEY FK_20E8BA18A76ED395');
        $this->addSql('ALTER TABLE app_user_recovery_token ADD CONSTRAINT FK_7E5652DD4A3353D8 FOREIGN KEY (app_user_id) REFERENCES app_user (id)');
    }
}
