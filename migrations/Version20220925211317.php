<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220925211317 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE olx_account RENAME INDEX uniq_d4cd198f5e237e06 TO UNIQ_44573E915E237E06');
        $this->addSql('ALTER TABLE olx_account RENAME INDEX uniq_d4cd198f19eb6921 TO UNIQ_44573E9119EB6921');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE olx_account RENAME INDEX uniq_44573e9119eb6921 TO UNIQ_D4CD198F19EB6921');
        $this->addSql('ALTER TABLE olx_account RENAME INDEX uniq_44573e915e237e06 TO UNIQ_D4CD198F5E237E06');
    }
}
