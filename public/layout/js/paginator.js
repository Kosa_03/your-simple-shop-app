const paginator = {
    redirect: function(oButton, iPage) {
        let sUrl = oButton.attr('data-url');

        location.href = sUrl + '/' + iPage.toString();
    },
    validate: function(oButton) {
        let oPageBar = oButton.parent();
        let sPageValue = oPageBar.find('.page-bar-value').val();
        let sLastPage = oButton.attr('data-last-page');

        if (null !== sPageValue.match(/^[0-9]+$/)) {
            let iPageValue = parseInt(sPageValue);
            let iLastPage = parseInt(sLastPage);

            if (iPageValue >= 1 && iPageValue <= iLastPage) {
                paginator.redirect(oButton, iPageValue);

                return;
            }
        }

        let sCurrentPage = oButton.attr('data-current-page');
        let iCurrentPage = parseInt(sCurrentPage);

        paginator.redirect(oButton, iCurrentPage);
    },
    init: function() {
        $('.page-bar-submit').on('click', function () {
            paginator.validate($(this));
        });
    }
};

$(document).ready(function() {
    paginator.init();
});
