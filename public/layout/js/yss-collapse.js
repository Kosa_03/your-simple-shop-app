yssCollapse = {
    collapse: function(oLink)
    {
        let sElementId = '#' + oLink.data('yss-id');
        let oElementTitle = $(sElementId + ' .yss-collapse-title');
        let oElementBody = $(sElementId + ' .yss-collapse-body');

        oLink.data('yss-expanded', false);
        oLink.html('Pokaż');
        oElementTitle.removeClass('mb-4');
        oElementBody.hide();
    },
    expand: function(oLink)
    {
        let sElementId = '#' + oLink.data('yss-id');
        let oElementTitle = $(sElementId + ' .yss-collapse-title');
        let oElementBody = $(sElementId + ' .yss-collapse-body');

        oLink.data('yss-expanded', true);
        oLink.html('Ukryj');
        oElementTitle.addClass('mb-4');
        oElementBody.show();
    },
    initialize: function()
    {
        $('.yss-collapse-link').each(function() {
            let oLink = $(this);
            let sFormId = oLink.data('yss-form-id');
            let bExpanded = oLink.data('yss-expanded');
            let sLocalStorageExpanded = localStorage.getItem(sFormId + 'ViewCriteriaExpanded');

            if (sLocalStorageExpanded !== null) {
                if (sLocalStorageExpanded === 'true') {
                    yssCollapse.expand(oLink);
                } else {
                    yssCollapse.collapse(oLink);
                }
            } else {
                if (bExpanded === true) {
                    yssCollapse.expand(oLink);
                } else {
                    yssCollapse.collapse(oLink);
                }
            }
        });

        $('.yss-collapse .yss-collapse-link').on('click', function(event) {
            event.preventDefault();

            let oLink = $(this);
            let sFormId = oLink.data('yss-form-id');
            let bExpanded = oLink.data('yss-expanded');

            if (bExpanded === true) {
                yssCollapse.collapse(oLink);
                localStorage.setItem(sFormId + 'ViewCriteriaExpanded', 'false');
            } else {
                yssCollapse.expand(oLink);
                localStorage.setItem(sFormId + 'ViewCriteriaExpanded', 'true');
            }
        });
    }
};

$(document).ready(function() {
    yssCollapse.initialize();
});
