<?php

declare(strict_types=1);

namespace ACL\AccountOlxToCategoryOlx;

use CategoryOlx\Presentation\TokenDto;

class AccountFacade implements AccountFacadeInterface
{
    public function __construct(
        private AccountServiceAdapter $accountServiceAdapter
    ) {
    }

    public function findAccessTokenFor(int $accountId): ?TokenDto
    {
        return $this->accountServiceAdapter->findAccessTokenFor($accountId);
    }

    /**
     * @return array<string, int>
     */
    public function accountListForSelectType(): array
    {
        return $this->accountServiceAdapter->accountListForSelectType();
    }
}
