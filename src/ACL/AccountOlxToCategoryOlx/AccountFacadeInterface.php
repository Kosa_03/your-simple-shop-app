<?php

declare(strict_types=1);

namespace ACL\AccountOlxToCategoryOlx;

use CategoryOlx\Presentation\TokenDto;

interface AccountFacadeInterface
{
    public function findAccessTokenFor(int $accountId): ?TokenDto;

    /**
     * @return array<string, int>
     */
    public function accountListForSelectType(): array;
}
