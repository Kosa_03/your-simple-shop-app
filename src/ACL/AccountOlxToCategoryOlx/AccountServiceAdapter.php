<?php

declare(strict_types=1);

namespace ACL\AccountOlxToCategoryOlx;

use AccountOlx\Application\FindingAll\FindAll;
use AccountOlx\Application\FindingById\FindById;
use AccountOlx\Domain\AccountDto;
use CategoryOlx\Presentation\TokenDto;
use Common\Messenger\QueryBusInterface;

class AccountServiceAdapter
{
    public function __construct(
        private QueryBusInterface $queryBus
    ) {
    }

    public function findAccessTokenFor(int $accountId): ?TokenDto
    {
        // TODO: Should implements access token expiration
        $dto = $this->findByIdQuery($accountId);
        if (null === $dto) {
            return null;
        }

        return TokenDto::from($dto->accessToken);
    }

    /**
     * @return array<string, int>
     */
    public function accountListForSelectType(): array
    {
        $accountList = $this->findAllAccountsQuery();
        $list = [];
        foreach ($accountList as $accountDto) {
            if (null !== $accountDto->id) {
                $list[$accountDto->name] = $accountDto->id;
            }
        }
        return $list;
    }

    private function findByIdQuery(int $accountId): ?AccountDto
    {
        return $this->queryBus->run(FindById::new($accountId));
    }

    /**
     * @return array<int, AccountDto>
     */
    private function findAllAccountsQuery(): array
    {
        return $this->queryBus->run(FindAll::new());
    }
}
