<?php

declare(strict_types=1);

namespace AccountOlx\Application\CreatingAccount;

use AccountOlx\Domain\AccountDto;
use Common\Messenger\Command;

class CreateAccount implements Command
{
    private AccountDto $accountDto;

    private function __construct()
    {
    }

    public static function new(AccountDto $accountDto): self
    {
        $command = new self();
        $command->accountDto = $accountDto;
        return $command;
    }

    public function accountDto(): AccountDto
    {
        return $this->accountDto;
    }
}
