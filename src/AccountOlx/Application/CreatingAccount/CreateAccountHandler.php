<?php

declare(strict_types=1);

namespace AccountOlx\Application\CreatingAccount;

use AccountOlx\Domain\AccessToken;
use AccountOlx\Domain\Account;
use AccountOlx\Domain\AccountStorageInterface;
use AccountOlx\Domain\Event\OlxAccountCreated;
use AccountOlx\Domain\RefreshToken;
use Common\Clock;
use Common\Messenger\CommandHandler;
use EventPublisher\Core\EventPublisherInterface;
use EventPublisher\Shared\EventId;

class CreateAccountHandler implements CommandHandler
{
    public function __construct(
        private Clock $clock,
        private EventPublisherInterface $eventPublisher,
        private AccountStorageInterface $accountStorage
    ) {
    }

    public function __invoke(CreateAccount $command): void
    {
        $dto = $command->accountDto();

        $account = Account::create(
            $dto->name,
            $dto->clientId,
            $dto->clientSecret,
            RefreshToken::create($dto->refreshToken),
            AccessToken::create($dto->accessToken, $dto->accessTokenExpiryAt),
            $dto->redirectUrl,
            $this->clock->now()
        );
        $this->accountStorage->save($account);
        $this->eventPublisher->publish([
            OlxAccountCreated::new(EventId::v4(), $account->getId())
        ]);
    }
}
