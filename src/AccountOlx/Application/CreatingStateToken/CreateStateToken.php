<?php

declare(strict_types=1);

namespace AccountOlx\Application\CreatingStateToken;

use Common\Messenger\Command;

class CreateStateToken implements Command
{
    private int $accountId;

    private function __construct()
    {
    }

    public static function new(int $accountId): self
    {
        $command = new self();
        $command->accountId = $accountId;
        return $command;
    }

    public function accountId(): int
    {
        return $this->accountId;
    }
}
