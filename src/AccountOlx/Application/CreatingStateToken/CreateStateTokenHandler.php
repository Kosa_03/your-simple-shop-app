<?php

declare(strict_types=1);

namespace AccountOlx\Application\CreatingStateToken;

use AccountOlx\Application\FindingByStateToken\FindByStateToken;
use AccountOlx\Domain\AccountDto;
use AccountOlx\Domain\AccountStorageInterface;
use AccountOlx\Domain\StateToken;
use AccountOlx\Domain\Token\TokenGenerator;
use Common\Clock;
use Common\Messenger\CommandHandler;
use Common\Messenger\QueryBusInterface;
use EventPublisher\Core\EventPublisherInterface;

class CreateStateTokenHandler implements CommandHandler
{
    public function __construct(
        private Clock $clock,
        private AccountStorageInterface $accountStorage,
        private EventPublisherInterface $eventPublisher,
        private QueryBusInterface $queryBus
    ) {
    }

    public function __invoke(CreateStateToken $command): void
    {
        $account = $this->accountStorage->find($command->accountId());
        if (null === $account) {
            return;
        }

        $result = $account->createStateToken($this->generateStateToken());
        $this->accountStorage->save($account);
        $this->eventPublisher->publish($result->events());
    }

    private function generateStateToken(): StateToken
    {
        $stateToken = StateToken::create(TokenGenerator::random(), $this->clock->now());
        $token = $stateToken->getToken();
        if (null === $token) {
            return $this->generateStateToken();
        }
        if (null === $this->findByStateTokenQuery($token)) {
            return $stateToken;
        }

        // tail recursion
        return $this->generateStateToken();
    }

    private function findByStateTokenQuery(string $stateToken): ?AccountDto
    {
        return $this->queryBus->run(FindByStateToken::new($stateToken));
    }
}
