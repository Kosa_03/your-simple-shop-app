<?php

declare(strict_types=1);

namespace AccountOlx\Application\DeletingAccount;

use AccountOlx\Domain\AccountStorageInterface;
use AccountOlx\Domain\Event\OlxAccountDeleted;
use Common\Messenger\CommandHandler;
use EventPublisher\Core\EventPublisherInterface;
use EventPublisher\Shared\EventId;

class DeleteAccountHandler implements CommandHandler
{
    public function __construct(
        private EventPublisherInterface $eventPublisher,
        private AccountStorageInterface $accountStorage
    ) {
    }

    public function __invoke(DeleteAccount $command): void
    {
        $account = $this->accountStorage->find($command->accountId());
        if (null === $account) {
            return;
        }
        $this->accountStorage->remove($account);
        $this->eventPublisher->publish([
            OlxAccountDeleted::new(EventId::v4(), $command->accountId())
        ]);
    }
}
