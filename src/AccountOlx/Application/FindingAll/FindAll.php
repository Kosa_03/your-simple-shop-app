<?php

declare(strict_types=1);

namespace AccountOlx\Application\FindingAll;

use Common\Messenger\Query;

class FindAll implements Query
{
    private function __construct()
    {
    }

    public static function new(): self
    {
        return new self();
    }
}
