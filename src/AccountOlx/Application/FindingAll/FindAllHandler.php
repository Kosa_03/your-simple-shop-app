<?php

declare(strict_types=1);

namespace AccountOlx\Application\FindingAll;

use AccountOlx\Domain\AccountDto;
use AccountOlx\Domain\AccountStorageInterface;
use Common\Messenger\QueryHandler;

class FindAllHandler implements QueryHandler
{
    public function __construct(
        private AccountStorageInterface $accountStorage
    ) {
    }

    /**
     * @return array<int, AccountDto>
     */
    public function __invoke(FindAll $query): array
    {
        $collection = [];
        foreach ($this->accountStorage->findAll() as $account) {
            $collection[] = AccountDto::entity($account);
        }
        return $collection;
    }
}
