<?php

declare(strict_types=1);

namespace AccountOlx\Application\FindingById;

use Common\Messenger\Query;

class FindById implements Query
{
    private int $accountId;

    private function __construct()
    {
    }

    public static function new(int $accountId): self
    {
        $query = new self();
        $query->accountId = $accountId;
        return $query;
    }

    public function accountId(): int
    {
        return $this->accountId;
    }
}
