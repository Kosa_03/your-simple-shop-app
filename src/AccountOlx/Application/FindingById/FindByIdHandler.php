<?php

declare(strict_types=1);

namespace AccountOlx\Application\FindingById;

use AccountOlx\Domain\Account;
use AccountOlx\Domain\AccountDto;
use AccountOlx\Domain\AccountStorageInterface;
use Common\Messenger\QueryHandler;

class FindByIdHandler implements QueryHandler
{
    public function __construct(
        private AccountStorageInterface $accountStorage
    ) {
    }

    public function __invoke(FindById $query): ?AccountDto
    {
        $account = $this->accountStorage->find($query->accountId());
        if ($account instanceof Account) {
            return AccountDto::entity($account);
        }
        return null;
    }
}
