<?php

declare(strict_types=1);

namespace AccountOlx\Application\FindingByStateToken;

use Common\Messenger\Query;

class FindByStateToken implements Query
{
    private ?string $stateToken;

    private function __construct()
    {
    }

    public static function new(?string $token): self
    {
        $query = new self();
        $query->stateToken = $token;
        return $query;
    }

    public function stateToken(): ?string
    {
        return $this->stateToken;
    }
}
