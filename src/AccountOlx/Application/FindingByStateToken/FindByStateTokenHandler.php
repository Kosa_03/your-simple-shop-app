<?php

declare(strict_types=1);

namespace AccountOlx\Application\FindingByStateToken;

use AccountOlx\Domain\Account;
use AccountOlx\Domain\AccountDto;
use AccountOlx\Domain\AccountStorageInterface;
use Common\Messenger\QueryHandler;

class FindByStateTokenHandler implements QueryHandler
{
    public function __construct(
        private AccountStorageInterface $accountStorage
    ) {
    }

    public function __invoke(FindByStateToken $query): ?AccountDto
    {
        if (null === $query->stateToken()) {
            return null;
        }

        $account = $this->accountStorage->findOneBy(['stateToken.token' => $query->stateToken()]);
        if ($account instanceof Account) {
            return AccountDto::entity($account);
        }
        return null;
    }
}
