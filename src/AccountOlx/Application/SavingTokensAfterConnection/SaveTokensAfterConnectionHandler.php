<?php

declare(strict_types=1);

namespace AccountOlx\Application\SavingTokensAfterConnection;

use AccountOlx\Domain\AccessToken;
use AccountOlx\Domain\AccountStorageInterface;
use AccountOlx\Domain\RefreshToken;
use Common\AppResult;
use Common\Messenger\CommandHandler;
use EventPublisher\Core\EventPublisherInterface;

class SaveTokensAfterConnectionHandler implements CommandHandler
{
    public function __construct(
        private EventPublisherInterface $eventPublisher,
        private AccountStorageInterface $accountStorage
    ) {
    }

    public function __invoke(SaveTokensAfterConnection $command): void
    {
        $account = $this->accountStorage->find($command->accountId());
        if (null === $account) {
            return;
        }

        $result = $account->addTokensAfterConnection(
            RefreshToken::create($command->refreshToken()),
            AccessToken::create($command->accessToken(), $command->accessTokenExpiryAt())
        );
        $this->accountStorage->save($account);
        $this->eventPublisher->publish($result->events());
    }
}
