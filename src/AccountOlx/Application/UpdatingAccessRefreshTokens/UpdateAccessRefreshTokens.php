<?php

declare(strict_types=1);

namespace AccountOlx\Application\UpdatingAccessRefreshTokens;

use Common\Messenger\Command;

class UpdateAccessRefreshTokens implements Command
{
    private int $accountId;
    private ?string $refreshToken;
    private ?string $accessToken;
    private ?\DateTimeImmutable $accessTokenExpiryAt;

    private function __construct()
    {
    }

    public static function new(
        int $accountId,
        ?string $refreshToken,
        ?string $accessToken,
        ?\DateTimeImmutable $accessTokenExpiryAt
    ): self {
        $command = new self();
        $command->accountId = $accountId;
        $command->refreshToken = $refreshToken;
        $command->accessToken = $accessToken;
        $command->accessTokenExpiryAt = $accessTokenExpiryAt;
        return $command;
    }

    public function accountId(): int
    {
        return $this->accountId;
    }

    public function refreshToken(): ?string
    {
        return $this->refreshToken;
    }

    public function accessToken(): ?string
    {
        return $this->accessToken;
    }

    public function accessTokenExpiryAt(): ?\DateTimeImmutable
    {
        return $this->accessTokenExpiryAt;
    }
}
