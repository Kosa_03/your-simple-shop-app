<?php

declare(strict_types=1);

namespace AccountOlx\Application\UpdatingAccessRefreshTokens;

use AccountOlx\Domain\AccessToken;
use AccountOlx\Domain\AccountStorageInterface;
use AccountOlx\Domain\RefreshToken;
use Common\Messenger\CommandHandler;
use EventPublisher\Core\EventPublisherInterface;

class UpdateAccessRefreshTokensHandler implements CommandHandler
{
    public function __construct(
        private AccountStorageInterface $accountStorage,
        private EventPublisherInterface $eventPublisher
    ) {
    }

    public function __invoke(UpdateAccessRefreshTokens $command): void
    {
        $account = $this->accountStorage->find($command->accountId());
        if (null === $account) {
            return;
        }

        $result1 = $account->updateRefreshToken(RefreshToken::create($command->refreshToken()));
        $result2 = $account->updateAccessToken(
            AccessToken::create($command->accessToken(), $command->accessTokenExpiryAt())
        );
        $this->accountStorage->save($account);
        $this->eventPublisher->publish(array_merge($result1->events(), $result2->events()));
    }
}
