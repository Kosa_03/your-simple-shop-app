<?php

declare(strict_types=1);

namespace AccountOlx\Application\UpdatingBasicFields;

use Common\Messenger\Command;

class UpdateBasicInfo implements Command
{
    private int $accountId;
    private string $name;
    private int $clientId;
    private string $clientSecret;
    private string $redirectUrl;

    private function __construct()
    {
    }

    public static function new(
        int $accountId,
        string $name,
        int $clientId,
        string $clientSecret,
        string $redirectUrl
    ): self {
        $command = new self();
        $command->accountId = $accountId;
        $command->name = $name;
        $command->clientId = $clientId;
        $command->clientSecret = $clientSecret;
        $command->redirectUrl = $redirectUrl;
        return $command;
    }

    public function accountId(): int
    {
        return $this->accountId;
    }

    public function name(): string
    {
        return $this->name;
    }

    public function clientId(): int
    {
        return $this->clientId;
    }

    public function clientSecret(): string
    {
        return $this->clientSecret;
    }

    public function redirectUrl(): string
    {
        return $this->redirectUrl;
    }
}
