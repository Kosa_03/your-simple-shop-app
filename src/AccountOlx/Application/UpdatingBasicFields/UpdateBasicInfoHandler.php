<?php

declare(strict_types=1);

namespace AccountOlx\Application\UpdatingBasicFields;

use AccountOlx\Domain\AccountStorageInterface;
use Common\Messenger\CommandHandler;
use EventPublisher\Core\EventPublisherInterface;

class UpdateBasicInfoHandler implements CommandHandler
{
    public function __construct(
        private AccountStorageInterface $accountStorage,
        private EventPublisherInterface $eventPublisher
    ) {
    }

    public function __invoke(UpdateBasicInfo $command): void
    {
        $account = $this->accountStorage->find($command->accountId());
        if (null === $account) {
            return;
        }

        $result = $account->updateBasicFields(
            $command->name(),
            $command->clientId(),
            $command->clientSecret(),
            $command->redirectUrl()
        );
        $this->accountStorage->save($account);
        $this->eventPublisher->publish($result->events());
    }
}
