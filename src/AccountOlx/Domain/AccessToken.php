<?php

declare(strict_types=1);

namespace AccountOlx\Domain;

use DateTimeImmutable;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Embeddable;

#[Embeddable]
class AccessToken
{
    #[Column(name: 'access_token', type: 'string', length: 255, nullable: true)]
    private ?string $token = null;

    #[Column(name: 'access_token_expiry_at', type: 'datetime_immutable', nullable: true)]
    private ?DateTimeImmutable $expiryAt = null;

    private function __construct()
    {
    }

    public static function create(?string $token, ?DateTimeImmutable $expiryAt): self
    {
        $accessToken = new self();
        $accessToken->token = $token;
        $accessToken->expiryAt = $expiryAt;
        return $accessToken;
    }

    public static function empty(): self
    {
        return new self();
    }

    public function getToken(): ?string
    {
        return $this->token;
    }

    public function getExpiryAt(): ?DateTimeImmutable
    {
        return $this->expiryAt;
    }
}
