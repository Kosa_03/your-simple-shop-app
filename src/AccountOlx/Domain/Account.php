<?php

declare(strict_types=1);

namespace AccountOlx\Domain;

use AccountOlx\Domain\Event\AccessTokenUpdated;
use AccountOlx\Domain\Event\OlxAccountUpdated;
use AccountOlx\Domain\Event\RefreshTokenUpdated;
use AccountOlx\Domain\Event\StateTokenCreated;
use DateTimeImmutable;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Embedded;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\Table;
use EventPublisher\Shared\EventId;
use EventPublisher\Shared\Result;

#[Entity]
#[Table(name: 'olx_account')]
class Account
{
    #[Id]
    #[GeneratedValue]
    #[Column(name: 'id', type: 'integer')]
    private int $id;

    #[Column(name: 'name', type: 'string', length: 128, unique: true)]
    private string $name;

    #[Column(name: 'client_id', type: 'integer', unique: true)]
    private int $clientId;

    #[Column(name: 'client_secret', type: 'string', length: 100)]
    private string $clientSecret;

    #[Column(name: 'redirect_url', type: 'string', length: 100)]
    private string $redirectUrl;

    #[Embedded(class: StateToken::class, columnPrefix: false)]
    private StateToken $stateToken;

    #[Embedded(class: RefreshToken::class, columnPrefix: false)]
    private RefreshToken $refreshToken;

    #[Embedded(class: AccessToken::class, columnPrefix: false)]
    private AccessToken $accessToken;

    #[Column(name: 'created_at', type: 'datetime_immutable')]
    private ?DateTimeImmutable $createdAt;

    public function __construct()
    {
    }

    public static function create(
        string $name,
        int $clientId,
        string $clientSecret,
        RefreshToken $refreshToken,
        AccessToken $accessToken,
        string $redirectUrl,
        DateTimeImmutable $when
    ): self {
        $account = new self();
        $account->name = $name;
        $account->clientId = $clientId;
        $account->clientSecret = $clientSecret;
        $account->refreshToken = $refreshToken;
        $account->accessToken = $accessToken;
        $account->redirectUrl = $redirectUrl;
        $account->createdAt = $when;
        $account->stateToken = StateToken::empty();
        return $account;
    }

    public function updateBasicFields(string $name, int $clientId, string $clientSecret, string $redirectUrl): Result
    {
        $this->name = $name;
        $this->clientId = $clientId;
        $this->clientSecret = $clientSecret;
        $this->redirectUrl = $redirectUrl;

        return Result::success([
            OlxAccountUpdated::new(EventId::v4(), $this->id)
        ]);
    }

    public function updateRefreshToken(RefreshToken $refreshToken): Result
    {
        $this->refreshToken = $refreshToken;

        return Result::success([
            RefreshTokenUpdated::new(EventId::v4(), $this->id, $refreshToken->getToken())
        ]);
    }

    public function updateAccessToken(AccessToken $accessToken): Result
    {
        $this->accessToken = $accessToken;

        return Result::success([
            AccessTokenUpdated::new(EventId::v4(), $this->id, $accessToken->getToken(), $accessToken->getExpiryAt())
        ]);
    }

    public function createStateToken(StateToken $token): Result
    {
        $this->stateToken = $token;

        return Result::success([
            StateTokenCreated::new(EventId::v4(), $this->id, $token->getToken(), $token->getCreatedAt())
        ]);
    }

    public function addTokensAfterConnection(RefreshToken $refreshToken, AccessToken $accessToken): Result
    {
        $this->refreshToken = $refreshToken;
        $this->accessToken = $accessToken;
        $this->stateToken = StateToken::empty();

        return Result::success([]);
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getClientId(): int
    {
        return $this->clientId;
    }

    public function getClientSecret(): string
    {
        return $this->clientSecret;
    }

    public function getRedirectUrl(): string
    {
        return $this->redirectUrl;
    }

    public function getStateToken(): ?string
    {
        return $this->stateToken->getToken();
    }

    public function getStateCreatedAt(): ?DateTimeImmutable
    {
        return $this->stateToken->getCreatedAt();
    }

    public function getRefreshToken(): ?string
    {
        return $this->refreshToken->getToken();
    }

    public function getAccessToken(): ?string
    {
        return $this->accessToken->getToken();
    }

    public function getExpiryAt(): ?DateTimeImmutable
    {
        return $this->accessToken->getExpiryAt();
    }

    public function getCreatedAt(): ?DateTimeImmutable
    {
        return $this->createdAt;
    }
}
