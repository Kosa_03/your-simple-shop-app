<?php

declare(strict_types=1);

namespace AccountOlx\Domain;

use DateTimeImmutable;

class AccountDto
{
    public ?int $id;
    public string $name;
    public int $clientId;
    public string $clientSecret;
    public string $redirectUrl;
    public ?string $stateToken;
    public ?DateTimeImmutable $stateTokenCreatedAt;
    public ?string $refreshToken;
    public ?string $accessToken;
    public ?DateTimeImmutable $accessTokenExpiryAt;
    public ?DateTimeImmutable $createdAt;

    private function __construct()
    {
    }

    public static function empty(): self
    {
        return new self();
    }

    public static function create(
        string $name,
        int $clientId,
        string $clientSecret,
        string $refreshToken,
        string $accessToken,
        ?DateTimeImmutable $expiryAt,
        string $redirectUrl,
    ): self {
        $dto = new self();
        $dto->name = $name;
        $dto->clientId = $clientId;
        $dto->clientSecret = $clientSecret;
        $dto->refreshToken = $refreshToken;
        $dto->accessToken = $accessToken;
        $dto->accessTokenExpiryAt = $expiryAt;
        $dto->redirectUrl = $redirectUrl;
        return $dto;
    }

    public static function entity(Account $account): self
    {
        $dto = new self();
        $dto->id = $account->getId();
        $dto->name = $account->getName();
        $dto->clientId = $account->getClientId();
        $dto->clientSecret = $account->getClientSecret();
        $dto->refreshToken = $account->getRefreshToken();
        $dto->accessToken = $account->getAccessToken();
        $dto->accessTokenExpiryAt = $account->getExpiryAt();
        $dto->redirectUrl = $account->getRedirectUrl();
        $dto->stateToken = $account->getStateToken();
        $dto->stateTokenCreatedAt = $account->getStateCreatedAt();
        $dto->createdAt = $account->getCreatedAt();
        return $dto;
    }

    public function withTokens(string $accessToken, DateTimeImmutable $expiryAt, string $refreshToken): void
    {
        $this->accessToken = $accessToken;
        $this->accessTokenExpiryAt = $expiryAt;
        $this->refreshToken = $refreshToken;
    }
}
