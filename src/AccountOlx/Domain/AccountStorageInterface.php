<?php

declare(strict_types=1);

namespace AccountOlx\Domain;

use Common\Pagination;

interface AccountStorageInterface
{
    public function find(int $accountId): ?Account;
    /** @param array<string, mixed> $criteria */
    public function findOneBy(array $criteria): ?Account;
    /** @return array<int, Account> */
    public function findAll(): array;
    /** @return array<int, Account> */
    public function findByPagination(int $page, Pagination $pagination): array;
    public function save(Account $account): Account;
    public function remove(Account $olxAccount): void;
    public function startTransaction(): void;
    public function commit(): void;
    public function rollback(): void;
}
