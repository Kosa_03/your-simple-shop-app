<?php

declare(strict_types=1);

namespace AccountOlx\Domain\Event;

use DateTimeImmutable;
use EventPublisher\Core\DomainEvent\DomainEvent;
use EventPublisher\Shared\EventId;

class AccessTokenUpdated implements DomainEvent
{
    private EventId $eventId;

    private int $accountId;

    private ?string $token;

    private ?DateTimeImmutable $expiryAt;

    private function __construct()
    {
    }

    public static function new(EventId $eventId, int $accountId, ?string $token, ?DateTimeImmutable $expiryAt): self
    {
        $event = new self();
        $event->eventId = $eventId;
        $event->accountId = $accountId;
        $event->token = $token;
        $event->expiryAt = $expiryAt;
        return $event;
    }

    public function eventId(): EventId
    {
        return $this->eventId;
    }

    public function getAccountId(): int
    {
        return $this->accountId;
    }

    public function getToken(): ?string
    {
        return $this->token;
    }

    public function getExpiryAt(): ?DateTimeImmutable
    {
        return $this->expiryAt;
    }
}
