<?php

declare(strict_types=1);

namespace AccountOlx\Domain\Event;

use EventPublisher\Core\DomainEvent\DomainEvent;
use EventPublisher\Shared\EventId;

class OlxAccountCreated implements DomainEvent
{
    private EventId $eventId;
    private int $accountId;

    private function __construct()
    {
    }

    public static function new(EventId $eventId, int $accountId): self
    {
        $event = new self();
        $event->eventId = $eventId;
        $event->accountId = $accountId;
        return $event;
    }

    public function eventId(): EventId
    {
        return $this->eventId;
    }

    public function getAccountId(): int
    {
        return $this->accountId;
    }
}
