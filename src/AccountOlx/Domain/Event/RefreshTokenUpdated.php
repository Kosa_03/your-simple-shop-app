<?php

declare(strict_types=1);

namespace AccountOlx\Domain\Event;

use EventPublisher\Core\DomainEvent\DomainEvent;
use EventPublisher\Shared\EventId;

class RefreshTokenUpdated implements DomainEvent
{
    private EventId $eventId;

    private int $accountId;

    private ?string $token;

    private function __construct()
    {
    }

    public static function new(EventId $eventId, int $accountId, ?string $token): self
    {
        $event = new self();
        $event->eventId = $eventId;
        $event->accountId = $accountId;
        $event->token = $token;
        return $event;
    }

    public function eventId(): EventId
    {
        return $this->eventId;
    }

    public function getAccountId(): int
    {
        return $this->accountId;
    }

    public function getToken(): ?string
    {
        return $this->token;
    }
}
