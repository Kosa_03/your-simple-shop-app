<?php

declare(strict_types=1);

namespace AccountOlx\Domain;

use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Embeddable;

#[Embeddable]
class RefreshToken
{
    #[Column(name: 'refresh_token', type: 'text', nullable: true)]
    private ?string $token = null;

    private function __construct()
    {
    }

    public static function create(?string $token): self
    {
        $refreshToken = new self();
        $refreshToken->token = $token;
        return $refreshToken;
    }

    public static function empty(): self
    {
        return new self();
    }

    public function getToken(): ?string
    {
        return $this->token;
    }
}
