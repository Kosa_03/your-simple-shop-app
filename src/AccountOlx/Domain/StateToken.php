<?php

declare(strict_types=1);

namespace AccountOlx\Domain;

use DateTimeImmutable;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Embeddable;

#[Embeddable]
class StateToken
{
    #[Column(name: 'state_token', type: 'string', length: 20, unique: true, nullable: true)]
    private ?string $token = null;

    #[Column(name: 'state_token_created_at', type: 'datetime_immutable', nullable: true)]
    private ?DateTimeImmutable $createdAt = null;

    private function __construct()
    {
    }

    public static function create(string $randomToken, DateTimeImmutable $createdAt): self
    {
        $stateToken = new self();
        $stateToken->token = $randomToken;
        $stateToken->createdAt = $createdAt;
        return $stateToken;
    }

    public static function empty(): self
    {
        return new self();
    }

    public function getToken(): ?string
    {
        return $this->token;
    }

    public function getCreatedAt(): ?DateTimeImmutable
    {
        return $this->createdAt;
    }
}
