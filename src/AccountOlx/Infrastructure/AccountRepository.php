<?php

declare(strict_types=1);

namespace AccountOlx\Infrastructure;

use AccountOlx\Domain\Account;
use AccountOlx\Domain\AccountStorageInterface;
use Common\Pagination;
use Doctrine\DBAL\Connection;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Doctrine\Persistence\ObjectRepository;

class AccountRepository implements AccountStorageInterface
{
    private Connection $connection;
    /** @var EntityRepository<Account>  */
    private ObjectRepository $repository;

    public function __construct(
        private EntityManagerInterface $entityManager
    ) {
        $this->connection = $entityManager->getConnection();
        $this->repository = $this->entityManager->getRepository(Account::class);
    }

    public function find(int $accountId): ?Account
    {
        return $this->repository->find($accountId);
    }

    /**
     * @param array<string, mixed> $criteria
     */
    public function findOneBy(array $criteria): ?Account
    {
        $olxAccount = $this->repository->findOneBy($criteria);
        if ($olxAccount instanceof Account) {
            return $olxAccount;
        }

        return null;
    }

    /**
     * @return array<int, Account>
     */
    public function findAll(): array
    {
        return $this->repository->findAll();
    }

    /**
     * @return array<int, Account>
     */
    public function findByPagination(int $page, Pagination $pagination): array
    {
        $accounts = $this->repository->createQueryBuilder('a')
            ->orderBy('a.' . $pagination->sortColumn, $pagination->sortMethod)
            ->setFirstResult(($page - 1) * $pagination->itemsPerPage)
            ->setMaxResults($pagination->itemsPerPage)
            ->getQuery()
            ->getResult();

        $count = $this->repository->createQueryBuilder('a')
            ->select('COUNT(a.id) AS amount')
            ->getQuery()
            ->getResult();

        $pagination->itemsTotal = $count[0]['amount'];

        return $accounts;
    }

    public function save(Account $account): Account
    {
        $this->entityManager->persist($account);
        $this->entityManager->flush();
        return $account;
    }

    public function remove(Account $olxAccount): void
    {
        $this->entityManager->remove($olxAccount);
        $this->entityManager->flush();
    }

    public function startTransaction(): void
    {
        $this->connection->beginTransaction();
    }

    public function commit(): void
    {
        $this->connection->commit();
    }

    public function rollback(): void
    {
        $this->connection->rollBack();
    }
}
