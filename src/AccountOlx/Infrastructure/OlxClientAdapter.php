<?php

declare(strict_types=1);

namespace AccountOlx\Infrastructure;

use AccountOlx\Infrastructure\OlxModel\Authorization;
use ApiRestOlx\Contract\OlxClientInterface;

class OlxClientAdapter
{
    public function __construct(
        private OlxClientInterface $olxClient
    ) {
    }

    public function authorizeUrl(int $clientId, string $redirectUrl, string $state): string
    {
        return $this->olxClient->authorizeUrl($clientId, $redirectUrl, $state);
    }

    public function authorizeByCode(int $clientId, string $clientSecret, string $code): Authorization
    {
        $response = $this->olxClient->authorizeByCode($clientId, $clientSecret, $code);
        return Authorization::data($response->toArray());
    }

    public function refreshAccessToken(int $clientId, string $clientSecret, string $refreshToken): Authorization
    {
        $response = $this->olxClient->refreshAccessToken($clientId, $clientSecret, $refreshToken);
        return Authorization::data($response->toArray());
    }
}
