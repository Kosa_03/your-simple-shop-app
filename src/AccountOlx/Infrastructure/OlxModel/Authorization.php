<?php

declare(strict_types=1);

namespace AccountOlx\Infrastructure\OlxModel;

class Authorization
{
    private string $accessToken;
    private int $expiresIn;
    private string $tokenType;
    private string $scope;
    private string $refreshToken;

    private function __construct()
    {
    }

    /**
     * @param array<string, mixed> $data
     */
    public static function data(array $data): self
    {
        $auth = new self();
        $auth->accessToken = $data['access_token'];
        $auth->expiresIn = $data['expires_in'];
        $auth->tokenType = $data['token_type'];
        $auth->scope = $data['scope'];
        $auth->refreshToken = $data['refresh_token'];
        return $auth;
    }

    public function accessToken(): string
    {
        return $this->accessToken;
    }

    public function expiresIn(): int
    {
        return $this->expiresIn;
    }

    public function tokenType(): string
    {
        return $this->tokenType;
    }

    public function scope(): string
    {
        return $this->scope;
    }

    public function refreshToken(): string
    {
        return $this->refreshToken;
    }
}
