<?php

declare(strict_types=1);

namespace AccountOlx\Presentation;

use AccountOlx\Presentation\CreatingAccount\CreateAccountViewService;
use AccountOlx\Presentation\DeletingAccount\DeleteAccountViewService;
use AccountOlx\Presentation\ListingAccount\ListAccountViewService;
use AccountOlx\Presentation\UpdatingAccount\UpdateAccountViewService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AccountController extends AbstractController
{
    public function __construct(
        private ListAccountViewService $listAccountViewService,
        private CreateAccountViewService $createAccountViewService,
        private DeleteAccountViewService $deleteAccountViewService,
        private UpdateAccountViewService $updateAccountViewService
    ) {
    }

    #[Route(
        '/rest_api/olx/account/list/{page}',
        name: 'rest_api_olx_account_list',
        requirements: ['page' => '\d+'],
        defaults: ['page' => 1]
    )]
    public function listAccountOlx(int $page, Request $request): Response
    {
        return $this->listAccountViewService->page($page, $request);
    }

    #[Route('/rest_api/olx/account/add', name: 'rest_api_olx_account_add')]
    public function addOlxAccount(Request $request): Response
    {
        return $this->createAccountViewService->create($request);
    }

    #[Route(
        '/rest_api/olx/account/{accountId}/delete',
        name: 'rest_api_olx_account_delete',
        requirements: ['accountId' => '\d+']
    )]
    public function deleteAccountIndex(int $accountId, Request $request): Response
    {
        return $this->deleteAccountViewService->delete($accountId, $request);
    }

    #[Route(
        '/rest_api/olx/account/{accountId}/edit',
        name: 'rest_api_olx_account_edit',
        requirements: ['accountId' => '\d+']
    )]
    public function editAccount(int $accountId, Request $request): Response
    {
        return $this->updateAccountViewService->update($accountId, $request);
    }
}
