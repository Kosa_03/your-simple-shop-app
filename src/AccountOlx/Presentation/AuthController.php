<?php

declare(strict_types=1);

namespace AccountOlx\Presentation;

use AccountOlx\Presentation\ConnectingToOlx\ConnectToOlxRequestService;
use AccountOlx\Presentation\RefreshingTokens\RefreshTokensRequestService;
use AccountOlx\Presentation\VerifyingFromOlx\VerifyAccountRequestService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AuthController extends AbstractController
{
    public function __construct(
        private ConnectToOlxRequestService $connectToOlxRequestService,
        private RefreshTokensRequestService $refreshTokensRequestService,
        private VerifyAccountRequestService $verifyAccountRequestService
    ) {
    }

    #[Route(
        '/rest_api/olx/auth/{accountId}/connect',
        name: 'rest_api_olx_auth_connect',
        requirements: ['accountId' => '\d+']
    )]
    public function connectToOlx(int $accountId): Response
    {
        return $this->connectToOlxRequestService->connect($accountId);
    }

    #[Route('/rest_api/olx/auth/verify', name: 'rest_api_olx_auth_verify')]
    public function verifyOlxAccount(Request $request): Response
    {
        return $this->verifyAccountRequestService->verify($request);
    }

    #[Route(
        '/rest_api/olx/auth/{accountId}/refresh_tokens',
        name: 'rest_api_olx_auth_refresh_tokens',
        requirements: ['accountId' => '\d+']
    )]
    public function refreshTokens(int $accountId): Response
    {
        return $this->refreshTokensRequestService->refresh($accountId);
    }
}
