<?php

declare(strict_types=1);

namespace AccountOlx\Presentation\ConnectingToOlx;

use AccountOlx\Application\CreatingStateToken\CreateStateToken;
use AccountOlx\Application\FindingById\FindById;
use AccountOlx\Domain\AccountDto;
use AccountOlx\Infrastructure\OlxClientAdapter;
use Common\Messenger\Command;
use Common\Messenger\CommandBusInterface;
use Common\Messenger\QueryBusInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class ConnectToOlxRequestService
{
    private FlashBagInterface $flashBag;

    public function __construct(
        private CommandBusInterface $commandBus,
        private QueryBusInterface $queryBus,
        private OlxClientAdapter $olxClient,
        private UrlGeneratorInterface $urlGenerator,
        RequestStack $requestStack
    ) {
        $this->flashBag = $requestStack->getSession()->getFlashBag();
    }

    public function connect(int $accountId): Response
    {
        $accountDto = $this->findByIdQuery($accountId);
        if (null === $accountDto) {
            $this->accountNotFoundFlashMsg();
            return $this->redirectTo('rest_api_olx_account_list');
        }

        $this->runCommand(CreateStateToken::new($accountId));
        $accountDto = $this->findByIdQuery($accountId);
        if (null === $accountDto) {
            $this->accountNotFoundFlashMsg();
            return $this->redirectTo('rest_api_olx_account_list');
        }

        return new RedirectResponse($this->olxClient->authorizeUrl(
            $accountDto->clientId,
            $accountDto->redirectUrl,
            $accountDto->stateToken ?? ''
        ));
    }

    private function findByIdQuery(int $accountId): ?AccountDto
    {
        return $this->queryBus->run(FindById::new($accountId));
    }

    private function runCommand(Command $command): void
    {
        $this->commandBus->dispatch($command);
    }

    private function redirectTo(string $routeName): RedirectResponse
    {
        return new RedirectResponse($this->urlGenerator->generate($routeName));
    }

    private function accountNotFoundFlashMsg(): void
    {
        $this->flashBag->add('danger', 'Konto olx nie zostało znalezione');
    }
}
