<?php

declare(strict_types=1);

namespace AccountOlx\Presentation\CreatingAccount;

use AccountOlx\Application\CreatingAccount\CreateAccount;
use AccountOlx\Domain\AccountDto;
use Common\Messenger\Command;
use Common\Messenger\CommandBusInterface;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Throwable;
use Twig\Environment;

class CreateAccountViewService
{
    private FlashBagInterface $flashBag;

    public function __construct(
        private CommandBusInterface $commandBus,
        private FormFactoryInterface $formFactory,
        private Environment $twig,
        private UrlGeneratorInterface $urlGenerator,
        RequestStack $requestStack,
    ) {
        $this->flashBag = $requestStack->getSession()->getFlashBag();
    }

    public function create(Request $request): Response
    {
        $accountDto = AccountDto::empty();
        $form = $this->formFactory->create(AddAccountType::class, $accountDto);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->runCommand(CreateAccount::new($accountDto));
            $this->createdSuccessfullyFlashMsg($accountDto->name);

            return $this->redirectTo('rest_api_olx_account_list');
        }

        return new Response($this->renderView($form));
    }

    private function renderView(FormInterface $form): string
    {
        return $this->twig->render('@account_olx/add_account.html.twig', [
            'olxAccountForm' => $form->createView()
        ]);
    }

    private function runCommand(Command $command): void
    {
        $this->commandBus->dispatch($command);
    }

    private function redirectTo(string $routeName): RedirectResponse
    {
        return new RedirectResponse($this->urlGenerator->generate($routeName));
    }

    private function createdSuccessfullyFlashMsg(?string $accountName): void
    {
        $this->flashBag->add('success', sprintf('Konto <strong>%s</strong> zostało utworzone', $accountName));
    }
}
