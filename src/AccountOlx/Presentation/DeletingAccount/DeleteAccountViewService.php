<?php

declare(strict_types=1);

namespace AccountOlx\Presentation\DeletingAccount;

use AccountOlx\Application\DeletingAccount\DeleteAccount;
use AccountOlx\Application\FindingById\FindById;
use AccountOlx\Domain\AccountDto;
use Common\Messenger\Command;
use Common\Messenger\CommandBusInterface;
use Common\Messenger\QueryBusInterface;
use Symfony\Component\Form\ClickableInterface;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Twig\Environment;

class DeleteAccountViewService
{
    private FlashBagInterface $flashBag;

    public function __construct(
        private CommandBusInterface $commandBus,
        private QueryBusInterface $queryBus,
        private FormFactoryInterface $formFactory,
        private Environment $twig,
        private UrlGeneratorInterface $urlGenerator,
        RequestStack $requestStack
    ) {
        $this->flashBag = $requestStack->getSession()->getFlashBag();
    }

    public function delete(int $accountId, Request $request): Response
    {
        $accountDto = $this->findByIdQuery($accountId);
        if (null === $accountDto) {
            $this->accountNotFoundFlashMsg();
            return $this->redirectTo('rest_api_olx_account_list');
        }

        $form = $this->formFactory->create(ConfirmType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            if ($this->isButtonClicked($form, 'delete')) {
                $this->runCommand(DeleteAccount::new($accountId));
                $this->deletedSuccessfullyFlashMsg($accountDto->name);
            }
            if ($this->isButtonClicked($form, 'goBack')) {
                $this->goBackFlashMsg($accountDto->name);
            }
            return $this->redirectTo('rest_api_olx_account_list');
        }

        return new Response($this->renderView($accountDto, $form));
    }

    private function renderView(AccountDto $accountDto, FormInterface $form): string
    {
        return $this->twig->render('@account_olx/delete_account.html.twig', [
            'olxAccount' => $accountDto,
            'form' => $form->createView()
        ]);
    }

    private function findByIdQuery(int $accountId): ?AccountDto
    {
        return $this->queryBus->run(FindById::new($accountId));
    }

    private function runCommand(Command $command): void
    {
        $this->commandBus->dispatch($command);
    }

    private function isButtonClicked(FormInterface $form, string $fieldName): bool
    {
        $field = $form->get($fieldName);
        if ($field instanceof ClickableInterface) {
            return $field->isClicked();
        }
        return false;
    }

    private function redirectTo(string $routeName): RedirectResponse
    {
        return new RedirectResponse($this->urlGenerator->generate($routeName));
    }

    private function accountNotFoundFlashMsg(): void
    {
        $this->flashBag->add('danger', 'Konto olx nie zostało znalezione');
    }

    private function goBackFlashMsg(string $accountName): void
    {
        $this->flashBag->add('warning', sprintf('Konto <strong>%s</strong> nie zostało usunięte', $accountName));
    }

    private function deletedSuccessfullyFlashMsg(string $accountName): void
    {
        $this->flashBag->add('success', sprintf('Konto <strong>%s</strong> zostało usunięte', $accountName));
    }
}
