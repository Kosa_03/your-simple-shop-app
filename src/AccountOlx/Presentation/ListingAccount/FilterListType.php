<?php

declare(strict_types=1);

namespace AccountOlx\Presentation\ListingAccount;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;

class FilterListType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('itemsPerPage', ChoiceType::class, [
                'label' => 'Pozycji na stronie',
                'choices' => [
                    '10' => 10,
                    '25' => 25,
                    '50' => 50,
                    '100' => 100
                ],
                'label_attr' => [
                    'class' => 'yss-form-label'
                ],
                'attr' => [
                    'class' => 'form-select'
                ]
            ])
            ->add('sortColumn', ChoiceType::class, [
                'label' => 'Sortuj po',
                'choices' => [
                    'Nazwa' => 'name',
                    'Data utworzenia' => 'createdAt'
                ],
                'label_attr' => [
                    'class' => 'yss-form-label'
                ],
                'attr' => [
                    'class' => 'form-select'
                ]
            ])
            ->add('sortMethod', ChoiceType::class, [
                'label' => 'Sortuj',
                'choices' => [
                    'Rosnąco' => 'ASC',
                    'Malejąco' => 'DESC'
                ],
                'label_attr' => [
                    'class' => 'yss-form-label'
                ],
                'attr' => [
                    'class' => 'form-select'
                ]
            ])
            ->add('submit', SubmitType::class, [
                'label' => 'Filtruj',
                'attr' => [
                    'class' => 'btn-outline-gold'
                ]
            ]);
    }
}
