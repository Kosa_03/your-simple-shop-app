<?php

declare(strict_types=1);

namespace AccountOlx\Presentation\ListingAccount;

use AccountOlx\Domain\Account;
use AccountOlx\Domain\AccountStorageInterface;
use Common\Pagination;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Twig\Environment;

class ListAccountViewService
{
    private const PAGINATION_SESSION_NAME = 'list_account/pagination';

    private SessionInterface $session;

    public function __construct(
        private FormFactoryInterface $formFactory,
        private Environment $twig,
        private AccountStorageInterface $accountStorage,
        RequestStack $requestStack
    ) {
        $this->session = $requestStack->getSession();
    }

    public function page(int $page, Request $request): Response
    {
        $pagination = $this->getPaginationFromSession();

        $form = $this->formFactory->create(FilterListType::class, $pagination);
        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            $this->saveSessionFor($pagination);
        }

        $accountList = $this->accountStorage->findByPagination($page, $pagination);

        return new Response($this->renderView($accountList, $form, $page, $pagination));
    }

    private function getPaginationFromSession(): Pagination
    {
        $pagination = unserialize($this->session->get(self::PAGINATION_SESSION_NAME, ''));
        if (false === $pagination) {
            $pagination = Pagination::create('name', 'ASC', 10);
        }
        return $pagination;
    }

    private function saveSessionFor(Pagination $pagination): void
    {
        $this->session->set(self::PAGINATION_SESSION_NAME, serialize($pagination));
    }

    /**
     * @param array<int, Account> $accountList
     */
    private function renderView(array $accountList, FormInterface $form, int $page, Pagination $pagination): string
    {
        return $this->twig->render('@account_olx/list_account.html.twig', [
            'accountList' => $accountList,
            'filterForm' => $form->createView(),
            'currentPage' => $page,
            'lastPage' => $pagination->lastPage()
        ]);
    }
}
