<?php

declare(strict_types=1);

namespace AccountOlx\Presentation\RefreshingTokens;

use AccountOlx\Application\FindingById\FindById;
use AccountOlx\Application\UpdatingAccessRefreshTokens\UpdateAccessRefreshTokens;
use AccountOlx\Domain\AccountDto;
use AccountOlx\Infrastructure\OlxClientAdapter;
use AccountOlx\Infrastructure\OlxModel\Authorization;
use Common\Clock;
use Common\Messenger\Command;
use Common\Messenger\CommandBusInterface;
use Common\Messenger\QueryBusInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpClient\Exception\ClientException;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Contracts\HttpClient\ResponseInterface;

class RefreshTokensRequestService
{
    private FlashBagInterface $flashBag;

    public function __construct(
        private CommandBusInterface $commandBus,
        private QueryBusInterface $queryBus,
        private Clock $clock,
        private LoggerInterface $logger,
        private OlxClientAdapter $olxClient,
        private UrlGeneratorInterface $urlGenerator,
        RequestStack $requestStack
    ) {
        $this->flashBag = $requestStack->getSession()->getFlashBag();
    }

    public function refresh(int $accountId): Response
    {
        $accountDto = $this->findByIdQuery($accountId);
        if (null === $accountDto) {
            $this->accountNotFoundFlashMsg();
            return $this->redirectTo('rest_api_olx_account_list');
        }

        try {
            $model = $this->olxClient->refreshAccessToken(
                $accountDto->clientId,
                $accountDto->clientSecret,
                $accountDto->refreshToken ?? ''
            );
            $this->saveAuthTokens($accountId, $model);
            $this->tokensRefreshedSuccessfullyFlashMsg($accountDto->clientId);
        } catch (ClientException $e) {
            $this->logger->error($e->getMessage(), ['exception' => $e]);
            $this->somethingWentWrongFlashMsg($e->getResponse());
        }

        return $this->redirectTo('rest_api_olx_account_list');
    }

    private function saveAuthTokens(int $accountId, Authorization $model): void
    {
        $this->runCommand(UpdateAccessRefreshTokens::new(
            $accountId,
            $model->refreshToken(),
            $model->accessToken(),
            $this->clock->now('+' . $model->expiresIn() . ' seconds')
        ));
    }

    private function findByIdQuery(int $accountId): ?AccountDto
    {
        return $this->queryBus->run(FindById::new($accountId));
    }

    private function runCommand(Command $command): void
    {
        $this->commandBus->dispatch($command);
    }

    private function redirectTo(string $routeName): RedirectResponse
    {
        return new RedirectResponse($this->urlGenerator->generate($routeName));
    }

    private function accountNotFoundFlashMsg(): void
    {
        $this->flashBag->add('danger', 'Konto olx nie zostało znalezione');
    }

    private function somethingWentWrongFlashMsg(ResponseInterface $response): void
    {
        $content = $response->toArray(false);
        $this->flashBag->add('danger', $content['error_human_title'] ?? 'Wystąpił nieoczekiwany błąd. Przepraszamy.');
    }

    private function tokensRefreshedSuccessfullyFlashMsg(?int $clientId): void
    {
        $this->flashBag->add('success', sprintf('Tokeny dla konta <b>%s</b> zostały odświeżone', $clientId));
    }
}
