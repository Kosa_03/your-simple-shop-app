<?php

declare(strict_types=1);

namespace AccountOlx\Presentation\UpdatingAccount;

use AccountOlx\Domain\Account;
use Common\Validator\UniqueEntity;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Regex;

class EditBasicInfoType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name', TextType::class, [
                'label' => 'Nazwa aplikacji',
                'required' => false,
                'empty_data' => '',
                'constraints' => [
                    new NotBlank([
                        'message' => 'Pole nie może być puste'
                    ]),
                    new Length([
                        'min' => 3,
                        'max' => 128,
                        'minMessage' => 'Pole powinno zawierać co najmniej 3 znaki',
                        'maxMessage' => 'Pole nie powinno zawierać więcej niż 128 znaków'
                    ]),
                    new Regex([
                        'pattern' => '/^[a-zA-Z0-9_\-]+$/i',
                        'message' => 'Pole powinno składać się ze znaków a-z, A-Z, 0-9, _, -'
                    ]),
                    new UniqueEntity([
                        'entityClass' => Account::class,
                        'entityField' => 'name',
                        'message' => 'Podana nazwa aplikacji istnieje w bazie danych',
                    ])
                ]
            ])
            ->add('clientId', NumberType::class, [
                'label' => 'Client Id',
                'required' => false,
                'invalid_message' => 'Pole powinno być liczbą całkowitą',
                'constraints' => [
                    new NotBlank([
                        'message' => 'Pole nie może być puste'
                    ]),
                    new Regex([
                        'pattern' => '/^[0-9]+$/i',
                        'message' => 'Pole powinno składać się ze znaków 0-9'
                    ]),
                    new UniqueEntity([
                        'entityClass' => Account::class,
                        'entityField' => 'clientId',
                        'message' => 'Podany client id istnieje w bazie danych'
                    ])
                ]
            ])
            ->add('clientSecret', TextType::class, [
                'label' => 'Client Secret',
                'required' => false,
                'empty_data' => '',
                'constraints' => [
                    new NotBlank([
                        'message' => 'Pole nie może być puste'
                    ]),
                    new Length([
                        'max' => 100,
                        'maxMessage' => 'Pole nie powinno zawierać więcej niż 100 znaków'
                    ])
                ]
            ])
            ->add('redirectUrl', UrlType::class, [
                'label' => 'Redirect Url',
                'required' => false,
                'default_protocol' => 'https',
                'constraints' => [
                    new NotBlank([
                        'message' => 'Pole nie może być puste'
                    ]),
                    new Length([
                        'max' => 100,
                        'maxMessage' => 'Pole nie powinno zawierać więcej niż 100 znaków'
                    ])
                ]
            ])
            ->add('submit', SubmitType::class, [
                'label' => 'Zapisz'
            ]);
    }
}
