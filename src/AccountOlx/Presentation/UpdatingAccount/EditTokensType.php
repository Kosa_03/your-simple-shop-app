<?php

declare(strict_types=1);

namespace AccountOlx\Presentation\UpdatingAccount;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\Length;

class EditTokensType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('refreshToken', TextareaType::class, [
                'label' => 'Refresh Token',
                'required' => false,
                'constraints' => [
                    new Length([
                        'max' => 2500,
                        'maxMessage' => 'Pole nie powinno zawierać więcej niż 2500 znaków'
                    ])
                ]
            ])
            ->add('accessToken', TextType::class, [
                'label' => 'Access Token',
                'required' => false,
                'constraints' => [
                    new Length([
                        'max' => 255,
                        'maxMessage' => 'Pole nie powinno zawierać więcej niż 255 znaków'
                    ])
                ]
            ])
            ->add('accessTokenExpiryAt', DateTimeType::class, [
                'input' => 'datetime_immutable',
                'label' => 'Ważność Access Tokenu',
                'widget' => 'single_text',
                'html5' => true,
                'invalid_message' => 'Podana data nie jest poprawna.',
                'required' => false
            ])
            ->add('submit', SubmitType::class, [
                'label' => 'Zapisz'
            ]);
    }
}
