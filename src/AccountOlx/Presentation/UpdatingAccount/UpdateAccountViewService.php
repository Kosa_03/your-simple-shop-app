<?php

declare(strict_types=1);

namespace AccountOlx\Presentation\UpdatingAccount;

use AccountOlx\Application\FindingById\FindById;
use AccountOlx\Application\UpdatingAccessRefreshTokens\UpdateAccessRefreshTokens;
use AccountOlx\Application\UpdatingBasicFields\UpdateBasicInfo;
use AccountOlx\Domain\AccountDto;
use Common\Messenger\Command;
use Common\Messenger\CommandBusInterface;
use Common\Messenger\QueryBusInterface;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Twig\Environment;

class UpdateAccountViewService
{
    private FlashBagInterface $flashBag;

    public function __construct(
        private CommandBusInterface $commandBus,
        private QueryBusInterface $queryBus,
        private FormFactoryInterface $formFactory,
        private Environment $twig,
        private UrlGeneratorInterface $urlGenerator,
        RequestStack $requestStack
    ) {
        $this->flashBag = $requestStack->getSession()->getFlashBag();
    }

    public function update(int $accountId, Request $request): Response
    {
        $accountDto = $this->findByIdQuery($accountId);
        if (null === $accountDto) {
            $this->accountNotFoundFlashMsg();
            return $this->redirectTo('rest_api_olx_account_list');
        }

        $basicForm = $this->formFactory->create(EditBasicInfoType::class, $accountDto);
        $basicForm->handleRequest($request);
        if ($basicForm->isSubmitted() && $basicForm->isValid()) {
            $this->runCommand(UpdateBasicInfo::new(
                $accountId,
                $accountDto->name,
                $accountDto->clientId,
                $accountDto->clientSecret,
                $accountDto->redirectUrl
            ));
            $this->accountSavedSuccessfullyFlashMsg($accountDto->name);

            return $this->redirectTo('rest_api_olx_account_edit', ['accountId' => $accountId]);
        }

        $tokensForm = $this->formFactory->create(EditTokensType::class, $accountDto);
        $tokensForm->handleRequest($request);
        if ($tokensForm->isSubmitted() && $tokensForm->isValid()) {
            $this->runCommand(UpdateAccessRefreshTokens::new(
                $accountId,
                $accountDto->refreshToken,
                $accountDto->accessToken,
                $accountDto->accessTokenExpiryAt
            ));
            $this->tokensSavedSuccessfullyFlashMsg();

            return $this->redirectTo('rest_api_olx_account_edit', ['accountId' => $accountId]);
        }

        return new Response($this->renderView($basicForm, $tokensForm));
    }

    private function renderView(FormInterface $basicFieldsForm, FormInterface $tokenFieldsForm): string
    {
        return $this->twig->render('@account_olx/edit_account.html.twig', [
            'editOlxAccountBasicForm' => $basicFieldsForm->createView(),
            'editOlxAccountTokensForm' => $tokenFieldsForm->createView(),
        ]);
    }

    private function findByIdQuery(int $accountId): ?AccountDto
    {
        return $this->queryBus->run(FindById::new($accountId));
    }

    private function runCommand(Command $command): void
    {
        $this->commandBus->dispatch($command);
    }

    /**
     * @param array<string, int> $params
     */
    private function redirectTo(string $routeName, array $params = []): RedirectResponse
    {
        return new RedirectResponse($this->urlGenerator->generate($routeName, $params));
    }

    private function accountNotFoundFlashMsg(): void
    {
        $this->flashBag->add('danger', 'Konto olx nie zostało znalezione');
    }

    private function tokensSavedSuccessfullyFlashMsg(): void
    {
        $this->flashBag->add('success', 'Tokeny zostały poprawnie zapisane');
    }

    private function accountSavedSuccessfullyFlashMsg(?string $name): void
    {
        $this->flashBag->add('success', sprintf('Konto <strong>%s</strong> zostało poprawnie zapisane', $name));
    }
}
