<?php

declare(strict_types=1);

namespace AccountOlx\Presentation\VerifyingFromOlx;

use AccountOlx\Application\FindingByStateToken\FindByStateToken;
use AccountOlx\Application\SavingTokensAfterConnection\SaveTokensAfterConnection;
use AccountOlx\Domain\AccountDto;
use AccountOlx\Infrastructure\OlxClientAdapter;
use AccountOlx\Infrastructure\OlxModel\Authorization;
use Common\Clock;
use Common\Messenger\Command;
use Common\Messenger\CommandBusInterface;
use Common\Messenger\QueryBusInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpClient\Exception\ClientException;
use Symfony\Component\HttpClient\Exception\ServerException;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Contracts\HttpClient\ResponseInterface;

class VerifyAccountRequestService
{
    private FlashBagInterface $flashBag;

    public function __construct(
        private CommandBusInterface $commandBus,
        private QueryBusInterface $queryBus,
        private Clock $clock,
        private LoggerInterface $logger,
        private OlxClientAdapter $olxClient,
        private UrlGeneratorInterface $urlGenerator,
        RequestStack $requestStack
    ) {
        $this->flashBag = $requestStack->getSession()->getFlashBag();
    }

    public function verify(Request $request): Response
    {
        $code = $request->get('code');
        $stateToken = $request->get('state');

        $accountDto = $this->findByStateTokenQuery($stateToken);
        if (null === $accountDto || null === $accountDto->id) {
            $this->accountNotFoundFlashMsg();
            return $this->redirectTo('rest_api_olx_account_list');
        }

        try {
            $model = $this->olxClient->authorizeByCode($accountDto->clientId, $accountDto->clientSecret, $code);
            $this->saveAuthTokens($accountDto->id, $model);
            $this->accountSuccessfullyAuthorizedFlashMsg($accountDto->clientId);
        } catch (ClientException | ServerException $e) {
            $this->logger->error($e->getMessage(), ['exception' => $e]);
            $this->somethingWentWrongFlashMsg($e->getResponse());
        }

        return $this->redirectTo('rest_api_olx_account_list');
    }

    private function saveAuthTokens(int $accountId, Authorization $model): void
    {
        $this->runCommand(SaveTokensAfterConnection::new(
            $accountId,
            $model->refreshToken(),
            $model->accessToken(),
            $this->clock->now('+' . $model->expiresIn() . ' seconds')
        ));
    }

    private function findByStateTokenQuery(?string $stateToken): ?AccountDto
    {
        return $this->queryBus->run(FindByStateToken::new($stateToken));
    }

    private function runCommand(Command $command): void
    {
        $this->commandBus->dispatch($command);
    }

    private function redirectTo(string $routeName): RedirectResponse
    {
        return new RedirectResponse($this->urlGenerator->generate($routeName));
    }

    private function accountNotFoundFlashMsg(): void
    {
        $this->flashBag->add('danger', 'Konto olx nie zostało znalezione');
    }

    private function accountSuccessfullyAuthorizedFlashMsg(int $clientId): void
    {
        $this->flashBag->add('success', sprintf('Konto <b>%s</b> zostało pomyślnie autoryzowane', $clientId));
    }

    private function somethingWentWrongFlashMsg(ResponseInterface $response): void
    {
        $content = $response->toArray(false);
        $this->flashBag->add('danger', $content['error_human_title'] ?? 'Wystąpił nieoczekiwany błąd. Przepraszamy.');
    }
}
