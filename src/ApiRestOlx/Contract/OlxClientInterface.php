<?php

declare(strict_types=1);

namespace ApiRestOlx\Contract;

use Symfony\Contracts\HttpClient\ResponseInterface;

interface OlxClientInterface
{
    /**
     * Creates full authorization url olx webpage.
     *
     * @param int $clientId
     * @param string $redirectUrl
     * @param string $state
     *
     * @return string
     */
    public function authorizeUrl(int $clientId, string $redirectUrl, string $state): string;

    /**
     * Authorize account by code.
     *
     * @param int $clientId
     * @param string $clientSecret
     * @param string $code
     *
     * @return ResponseInterface
     */
    public function authorizeByCode(int $clientId, string $clientSecret, string $code): ResponseInterface;

    /**
     * Refresh access token for account with client id.
     *
     * @param int $clientId
     * @param string $clientSecret
     * @param string $refreshToken
     *
     * @return ResponseInterface
     */
    public function refreshAccessToken(int $clientId, string $clientSecret, string $refreshToken): ResponseInterface;

    /**
     * Add access token for current request.
     *
     * @param string $token
     *
     * @return void
     */
    public function accessToken(string $token): void;

    /**
     * Get all categories.
     *
     * @return ResponseInterface
     */
    public function getCategories(): ResponseInterface;

    /**
     * Get category with id.
     *
     * @param string $categoryId
     *
     * @return ResponseInterface
     */
    public function getCategory(string $categoryId): ResponseInterface;

    /**
     * Get category attributes with category id.
     *
     * @param string $categoryId
     *
     * @return ResponseInterface
     */
    public function getCategoryAttributes(string $categoryId): ResponseInterface;
}
