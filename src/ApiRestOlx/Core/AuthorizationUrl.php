<?php

declare(strict_types=1);

namespace ApiRestOlx\Core;

use ApiRestOlx\Core\Enum\BaseUrl;
use ApiRestOlx\Core\Enum\Endpoint;

class AuthorizationUrl
{
    private string $url;
    private string $endpoint;

    public function __construct()
    {
        $this->url = BaseUrl::OLX_PL->url();
        $this->endpoint = Endpoint::OAUTH_AUTHORIZE->url();
    }

    public function generate(int $clientId, string $redirectUrl, string $state): string
    {
        return $this->url . $this->endpoint . '?' . http_build_query($this->params($clientId, $redirectUrl, $state));
    }

    /**
     * @return array<string, int|string>
     */
    private function params(int $clientId, string $redirectUrl, string $state): array
    {
        return [
            'client_id' => $clientId,
            'response_type' => 'code',
            'scope' => 'v2 read write',
            'redirect_url' => $redirectUrl,
            'state' => $state
        ];
    }
}
