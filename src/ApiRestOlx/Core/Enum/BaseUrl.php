<?php

declare(strict_types=1);

namespace ApiRestOlx\Core\Enum;

enum BaseUrl: string
{
    case OLX_PL = 'https://www.olx.pl';
    case OLX_EN = 'https://www.olx.en';
    case OLX_DE = 'https://www.olx.de';

    public function url(): string
    {
        return $this->value;
    }
}
