<?php

declare(strict_types=1);

namespace ApiRestOlx\Core\Enum;

enum Endpoint: string
{
    case OAUTH_AUTHORIZE = '/oauth/authorize';
    case API_OPEN_OAUTH_TOKEN = '/api/open/oauth/token';

    case CATEGORIES = '/api/partner/categories';
    case CATEGORIES_CID = '/api/partner/categories/{categoryId}';
    case CATEGORIES_CID_ATTRIBUTES = '/api/partner/categories/{categoryId}/attributes';

    public function url(): string
    {
        return $this->value;
    }
}
