<?php

declare(strict_types=1);

namespace ApiRestOlx\Core;

use ApiRestOlx\Contract\OlxClientInterface;
use ApiRestOlx\Core\Request\AuthorizationRequest;
use ApiRestOlx\Core\Request\CategoryRequest;
use Symfony\Contracts\HttpClient\ResponseInterface;

class OlxClient implements OlxClientInterface
{
    private ?string $accessToken = null;

    public function __construct(
        private AuthorizationUrl $authorizationUrl,
        private AuthorizationRequest $authorizationRequest,
        private CategoryRequest $categoryRequest
    ) {
    }

    public function authorizeUrl(int $clientId, string $redirectUrl, string $state): string
    {
        return $this->authorizationUrl->generate($clientId, $redirectUrl, $state);
    }

    public function authorizeByCode(int $clientId, string $clientSecret, string $code): ResponseInterface
    {
        return $this->authorizationRequest->authorizeByCode($clientId, $clientSecret, $code);
    }

    public function refreshAccessToken(int $clientId, string $clientSecret, string $refreshToken): ResponseInterface
    {
        return $this->authorizationRequest->refreshAccessToken($clientId, $clientSecret, $refreshToken);
    }

    public function accessToken(string $token): void
    {
        $this->accessToken = $token;
    }

    public function getCategories(): ResponseInterface
    {
        $this->categoryRequest->accessToken($this->accessToken);
        return $this->categoryRequest->getCategories();
    }

    public function getCategory(string $categoryId): ResponseInterface
    {
        $this->categoryRequest->accessToken($this->accessToken);
        return $this->categoryRequest->getCategory($categoryId);
    }

    public function getCategoryAttributes(string $categoryId): ResponseInterface
    {
        $this->categoryRequest->accessToken($this->accessToken);
        return $this->categoryRequest->getCategoryAttributes($categoryId);
    }
}
