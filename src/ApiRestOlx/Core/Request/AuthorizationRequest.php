<?php

declare(strict_types=1);

namespace ApiRestOlx\Core\Request;

use ApiRestOlx\Core\Enum\BaseUrl;
use ApiRestOlx\Core\Enum\Endpoint;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Contracts\HttpClient\ResponseInterface;

class AuthorizationRequest
{
    private string $url;

    public function __construct(
        private HttpClientInterface $client
    ) {
        $this->url = BaseUrl::OLX_PL->url();
    }

    public function authorizeByCode(int $clientId, string $clientSecret, string $code): ResponseInterface
    {
        return $this->client->request('POST', $this->url . Endpoint::API_OPEN_OAUTH_TOKEN->url(), [
            'json' => [
                'grant_type' => 'authorization_code',
                'client_id' => $clientId,
                'client_secret' => $clientSecret,
                'code' => $code,
                'scope' => 'v2 read write'
            ]
        ]);
    }

    public function refreshAccessToken(int $clientId, string $clientSecret, string $refreshToken): ResponseInterface
    {
        return $this->client->request('POST', $this->url . Endpoint::API_OPEN_OAUTH_TOKEN->url(), [
            'headers' => [
                'Content-Type' => 'application/json',
                'Version' => '2.0'
            ],
            'json' => [
                'grant_type' => 'refresh_token',
                'client_id' => $clientId,
                'client_secret' => $clientSecret,
                'refresh_token' => $refreshToken
            ]
        ]);
    }
}
