<?php

declare(strict_types=1);

namespace ApiRestOlx\Core\Request;

use ApiRestOlx\Core\Enum\BaseUrl;
use ApiRestOlx\Core\Enum\Endpoint;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Contracts\HttpClient\ResponseInterface;

class CategoryRequest
{
    private string $url;
    private string $token = '';

    public function __construct(
        private HttpClientInterface $client
    ) {
        $this->url = BaseUrl::OLX_PL->url();
    }

    public function accessToken(?string $token): void
    {
        $this->token = $token;
    }

    public function getCategories(): ResponseInterface
    {
        return $this->client->request('GET', $this->url . Endpoint::CATEGORIES->url(), [
            'headers' => [
                'Authorization' => 'Bearer ' . $this->token,
                'Version' => '2.0'
            ]
        ]);
    }

    public function getCategory(string $categoryId): ResponseInterface
    {
        $endpoint = $this->url . str_replace('{categoryId}', $categoryId, Endpoint::CATEGORIES_CID->url());
        return $this->client->request('GET', $endpoint, [
            'headers' => [
                'Authorization' => 'Bearer ' . $this->token,
                'Version' => '2.0'
            ]
        ]);
    }

    public function getCategoryAttributes(string $categoryId): ResponseInterface
    {
        $endpoint = $this->url . str_replace(
            '{categoryId}',
            $categoryId,
            Endpoint::CATEGORIES_CID_ATTRIBUTES->url()
        );
        return $this->client->request('GET', $endpoint, [
            'headers' => [
                'Authorization' => 'Bearer ' . $this->token,
                'Version' => '2.0'
            ]
        ]);
    }
}
