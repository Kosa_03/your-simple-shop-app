<?php

declare(strict_types=1);

namespace CategoryOlx\Application\DeletingAll;

use Common\Messenger\Command;

class DeleteAll implements Command
{
    private function __construct()
    {
    }

    public static function new(): self
    {
        return new self();
    }
}
