<?php

declare(strict_types=1);

namespace CategoryOlx\Application\DeletingAll;

use CategoryOlx\Domain\AttributeStorageInterface;
use CategoryOlx\Domain\CategoryStorageInterface;
use Common\Messenger\CommandHandler;
use EventPublisher\Core\EventPublisherInterface;

class DeleteAllHandler implements CommandHandler
{
    public function __construct(
        private EventPublisherInterface $eventPublisher,
        private AttributeStorageInterface $attributeStorage,
        private CategoryStorageInterface $categoryStorage
    ) {
    }

    public function __invoke(DeleteAll $command): void
    {
        $this->attributeStorage->deleteAll();
        $this->categoryStorage->deleteAll();

        $this->eventPublisher->publish([]);
    }
}
