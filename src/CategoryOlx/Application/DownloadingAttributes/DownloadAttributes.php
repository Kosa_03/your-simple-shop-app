<?php

declare(strict_types=1);

namespace CategoryOlx\Application\DownloadingAttributes;

use Common\Messenger\AsyncTransportInterface;
use Common\Messenger\Command;

class DownloadAttributes implements Command, AsyncTransportInterface
{
    private string $accessToken;

    public function __construct(string $accessToken)
    {
        $this->accessToken = $accessToken;
    }

    public function getAccessToken(): string
    {
        return $this->accessToken;
    }
}
