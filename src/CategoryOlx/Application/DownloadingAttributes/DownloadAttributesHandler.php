<?php

declare(strict_types=1);

namespace CategoryOlx\Application\DownloadingAttributes;

use ApiRestOlx\Contract\OlxClientInterface;
use CategoryOlx\Application\SavingAttributes\SaveAttributes;
use CategoryOlx\Domain\CategoryStorageInterface;
use Common\Messenger\CommandBusInterface;
use Common\Messenger\CommandHandler;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpClient\Exception\ClientException;

class DownloadAttributesHandler implements CommandHandler
{
    public function __construct(
        private CommandBusInterface $commandBus,
        private OlxClientInterface $olxApiClient,
        private CategoryStorageInterface $categoryStorage,
        private LoggerInterface $logger
    ) {
    }

    public function __invoke(DownloadAttributes $command): void
    {
        try {
            $this->olxApiClient->accessToken($command->getAccessToken());
            foreach ($this->categoryStorage->findAll() as $category) {
                $response = $this->olxApiClient->getCategoryAttributes((string) $category->getId());
                $this->commandBus->dispatch(new SaveAttributes($category->getId(), $response->toArray()['data']));
            }
        } catch (ClientException $e) {
            $this->logger->error($e->getMessage(), ['exception' => $e]);
        }
    }
}
