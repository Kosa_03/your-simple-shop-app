<?php

declare(strict_types=1);

namespace CategoryOlx\Application\SavingAttributes;

use Common\Messenger\AsyncTransportInterface;
use Common\Messenger\Command;

class SaveAttributes implements Command, AsyncTransportInterface
{
    private int $categoryId;

    /**
     * @var array<int, array<string, mixed>> $data
     */
    private array $data;

    /**
     * @param array<int, array<string, mixed>> $data
     */
    public function __construct(int $categoryId, array $data)
    {
        $this->categoryId = $categoryId;
        $this->data = $data;
    }

    public function getCategoryId(): int
    {
        return $this->categoryId;
    }

    /**
     * @return array<int, array<string, mixed>>
     */
    public function getData(): array
    {
        return $this->data;
    }
}
