<?php

declare(strict_types=1);

namespace CategoryOlx\Application\SavingAttributes;

use CategoryOlx\Domain\Attribute;
use CategoryOlx\Domain\AttributeStorageInterface;
use CategoryOlx\Infrastructure\AttributeDto;
use Common\Messenger\CommandHandler;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class SaveAttributesHandler implements CommandHandler
{
    public function __construct(
        private ValidatorInterface $validator,
        private AttributeStorageInterface $attributeStorage
    ) {
    }

    public function __invoke(SaveAttributes $command): void
    {
        foreach ($command->getData() as $attribute) {
            $attributeDto = AttributeDto::fromArray($command->getCategoryId(), $attribute);
            $errorList = $this->validator->validate($attributeDto, null, ['default']);
            if ($errorList->count() === 0) {
                $attribute = Attribute::create(
                    $attributeDto->categoryId,
                    $attributeDto->code,
                    $attributeDto->label,
                    $attributeDto->unit,
                    $attributeDto->type,
                    $attributeDto->required,
                    $attributeDto->numeric,
                    $attributeDto->min,
                    $attributeDto->max,
                    $attributeDto->multiple,
                    $attributeDto->values
                );
                $this->attributeStorage->persist($attribute);
                $this->attributeStorage->flush();
            }
        }
    }
}
