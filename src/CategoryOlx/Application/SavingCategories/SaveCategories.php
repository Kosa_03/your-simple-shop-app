<?php

declare(strict_types=1);

namespace CategoryOlx\Application\SavingCategories;

use Common\Messenger\AsyncTransportInterface;
use Common\Messenger\Command;

class SaveCategories implements Command, AsyncTransportInterface
{
    /**
     * @var array<string, mixed>
     */
    private array $data;

    /**
     * @param array<string, mixed> $data
     */
    public function __construct(array $data)
    {
        $this->data = $data;
    }

    /**
     * @return array<string, mixed>
     */
    public function getData(): array
    {
        return $this->data;
    }
}
