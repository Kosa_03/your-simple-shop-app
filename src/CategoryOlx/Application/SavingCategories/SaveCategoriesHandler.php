<?php

declare(strict_types=1);

namespace CategoryOlx\Application\SavingCategories;

use CategoryOlx\Domain\Category;
use CategoryOlx\Domain\CategoryStorageInterface;
use CategoryOlx\Infrastructure\CategoryDto;
use Common\Messenger\CommandHandler;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class SaveCategoriesHandler implements CommandHandler
{
    public function __construct(
        private ValidatorInterface $validator,
        private CategoryStorageInterface $categoryStorage
    ) {
    }

    public function __invoke(SaveCategories $command): void
    {
        foreach ($command->getData() as $category) {
            $categoryDto = CategoryDto::fromArray($category);
            $errorList = $this->validator->validate($categoryDto, null, ['default']);
            if ($errorList->count() === 0) {
                $category = Category::create(
                    $categoryDto->id,
                    $categoryDto->name,
                    $categoryDto->parentId,
                    $categoryDto->photosLimit,
                    $categoryDto->leaf
                );
                $this->categoryStorage->persist($category);
                $this->categoryStorage->flush();
            }
        }
    }
}
