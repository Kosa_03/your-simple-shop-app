<?php

declare(strict_types=1);

namespace CategoryOlx\Domain;

use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\Table;

#[Entity]
#[Table(name: 'olx_attribute')]
class Attribute
{
    #[Id]
    #[GeneratedValue(
        strategy: 'AUTO'
    )]
    #[Column(name: 'id', type: 'integer', options: ['unsigned' => true])]
    private int $id;

    #[Column(name: 'category_id', type: 'integer', options: ['unsigned' => true])]
    private int $categoryId;

    #[Column(name: 'code', type: 'string', length: 128)]
    private string $code;

    #[Column(name: 'label', type: 'string', length: 128)]
    private string $label;

    #[Column(name: 'unit', type: 'string', length: 16, nullable: true)]
    private ?string $unit;

    #[Column(name: 'type', type: 'string', length: 128)]
    private string $type;

    #[Column(name: 'required', type: 'boolean')]
    private bool $required;

    #[Column(name: '`numeric`', type: 'boolean')]
    private bool $numeric;

    #[Column(name: 'min', type: 'integer', nullable: true)]
    private ?int $min;

    #[Column(name: 'max', type: 'integer', nullable: true)]
    private ?int $max;

    #[Column(name: 'multiple', type: 'boolean')]
    private bool $multiple;

    /**
     * @var array<int, array<string, mixed>>
     */
    #[Column(name: '`values`', type: 'json')]
    private array $values;

    private function __construct()
    {
    }

    /**
     * @param array<int, array<string, mixed>> $values
     */
    public static function create(
        int $categoryId,
        string $code,
        string $label,
        ?string $unit,
        string $type,
        bool $required,
        bool $numeric,
        ?int $min,
        ?int $max,
        bool $multiple,
        array $values
    ): self {
        $attribute = new self();
        $attribute->categoryId = $categoryId;
        $attribute->code = $code;
        $attribute->label = $label;
        $attribute->unit = $unit;
        $attribute->type = $type;
        $attribute->required = $required;
        $attribute->numeric = $numeric;
        $attribute->min = $min;
        $attribute->max = $max;
        $attribute->multiple = $multiple;
        $attribute->values = $values;
        return $attribute;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getCategoryId(): int
    {
        return $this->categoryId;
    }

    public function getCode(): string
    {
        return $this->code;
    }

    public function getLabel(): string
    {
        return $this->label;
    }

    public function getUnit(): ?string
    {
        return $this->unit;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function isRequired(): bool
    {
        return $this->required;
    }

    public function isNumeric(): bool
    {
        return $this->numeric;
    }

    public function getMin(): ?int
    {
        return $this->min;
    }

    public function getMax(): ?int
    {
        return $this->max;
    }

    public function isMultiple(): bool
    {
        return $this->multiple;
    }

    /**
     * @return array<int, array<string, mixed>>
     */
    public function getValues(): array
    {
        return $this->values;
    }
}
