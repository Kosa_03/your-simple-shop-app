<?php

declare(strict_types=1);

namespace CategoryOlx\Domain;

use Doctrine\Common\Collections\Collection;

interface AttributeStorageInterface
{
    /** @return Collection<int, Attribute> */
    public function findAll(): Collection;
    public function persist(Attribute $attribute): void;
    public function deleteAll(): void;
    public function flush(): void;
}
