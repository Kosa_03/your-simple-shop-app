<?php

declare(strict_types=1);

namespace CategoryOlx\Domain;

use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\Table;

#[Entity]
#[Table(name: 'olx_category')]
class Category
{
    #[Id]
    #[GeneratedValue(strategy: 'NONE')]
    #[Column(name: 'id', type: 'integer')]
    private int $id;

    #[Column(name: 'name', type: 'string', length: 255)]
    private string $name;

    #[Column(name: 'parent_id', type: 'integer')]
    private int $parentId;

    #[Column(name: 'photos_limit', type: 'integer')]
    private int $photosLimit;

    #[Column(name: 'leaf', type: 'boolean')]
    private bool $leaf;

    private function __construct()
    {
    }

    public static function create(int $categoryId, string $name, int $parentId, int $photosLimit, bool $leaf): self
    {
        $category = new self();
        $category->id = $categoryId;
        $category->name = $name;
        $category->parentId = $parentId;
        $category->photosLimit = $photosLimit;
        $category->leaf = $leaf;
        return $category;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getParentId(): int
    {
        return $this->parentId;
    }

    public function getPhotosLimit(): int
    {
        return $this->photosLimit;
    }

    public function isLeaf(): bool
    {
        return $this->leaf;
    }
}
