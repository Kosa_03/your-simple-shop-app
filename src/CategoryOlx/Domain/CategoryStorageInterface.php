<?php

declare(strict_types=1);

namespace CategoryOlx\Domain;

use Doctrine\Common\Collections\Collection;

interface CategoryStorageInterface
{
    /** @return Collection<int, Category> */
    public function findAll(): Collection;
    public function persist(Category $category): void;
    public function deleteAll(): void;
    public function flush(): void;
}
