<?php

declare(strict_types=1);

namespace CategoryOlx\Infrastructure;

use Symfony\Component\Validator\Constraints as Assert;

class AttributeDto
{
    #[Assert\Regex(
        pattern: '/^[0-9]+$/i',
        message: 'Pole powinno składać się ze znaków 0-9',
        groups: ['default']
    )]
    public ?int $id;

    #[Assert\NotBlank(
        message: 'Pole nie może być puste',
        groups: ['default']
    )]
    #[Assert\Regex(
        pattern: '/^[0-9]+$/i',
        message: 'Pole powinno składać się ze znaków 0-9',
        groups: ['default']
    )]
    public ?int $categoryId;

    #[Assert\NotBlank(
        message: 'Pole nie może być puste',
        groups: ['default']
    )]
    #[Assert\Length(
        max: 128,
        maxMessage: 'Pole nie powinno zawierać więcej niż 128 znaków',
        groups: ['default']
    )]
    public ?string $code;

    #[Assert\NotBlank(
        message: 'Pole nie może być puste',
        groups: ['default']
    )]
    #[Assert\Length(
        max: 128,
        maxMessage: 'Pole nie powinno zawierać więcej niż 128 znaków',
        groups: ['default']
    )]
    public ?string $label;

    #[Assert\Length(
        max: 16,
        maxMessage: 'Pole nie powinno zawierać więcej niż 16 znaków',
        groups: ['default']
    )]
    public ?string $unit;

    #[Assert\NotBlank(
        message: 'Pole nie może być puste',
        groups: ['default']
    )]
    #[Assert\Length(
        max: 128,
        maxMessage: 'Pole nie powinno zawierać więcej niż 128 znaków',
        groups: ['default']
    )]
    public ?string $type;

    #[Assert\Type(
        type: 'bool',
        message: 'Pole powinno mieć wartość true / false',
        groups: ['default']
    )]
    public ?bool $required;

    #[Assert\Type(
        type: 'bool',
        message: 'Pole powinno mieć wartość true / false',
        groups: ['default']
    )]
    public ?bool $numeric;

    #[Assert\Regex(
        pattern: '/^[0-9]+$/i',
        message: 'Pole powinno składać się ze znaków 0-9',
        groups: ['default']
    )]
    public ?int $min;

    #[Assert\Regex(
        pattern: '/^[0-9]+$/i',
        message: 'Pole powinno składać się ze znaków 0-9',
        groups: ['default']
    )]
    public ?int $max;

    #[Assert\Type(
        type: 'bool',
        message: 'Pole powinno mieć wartość true / false',
        groups: ['default']
    )]
    public ?bool $multiple;

    /**
     * @var array<int, array<string, mixed>>|null
     */
    #[Assert\Type(
        type: 'array',
        message: 'Pole powinno być tablicą',
        groups: ['default']
    )]
    public ?array $values;

    public function __construct()
    {
        $this->clear();
    }

    public static function empty(int $categoryId): self
    {
        $dto = new self();
        $dto->categoryId = $categoryId;
        return $dto;
    }

    /**
     * @param array<string, mixed> $attribute
     */
    public static function fromArray(int $categoryId, array $attribute): self
    {
        $dto = new self();
        $dto->categoryId = $categoryId;
        $dto->code = $attribute['code'];
        $dto->label = $attribute['label'];
        $dto->unit = $attribute['unit'];
        $dto->type = $attribute['validation']['type'];
        $dto->required = $attribute['validation']['required'];
        $dto->numeric = $attribute['validation']['numeric'];
        $dto->min = $attribute['validation']['min'];
        $dto->max = $attribute['validation']['max'];
        $dto->multiple = $attribute['validation']['allow_multiple_values'];
        $dto->values = $attribute['values'];
        return $dto;
    }

    public function clear(): void
    {
        $this->id = null;
        $this->categoryId = null;
        $this->code = null;
        $this->label = null;
        $this->unit = null;
        $this->type = null;
        $this->required = null;
        $this->numeric = null;
        $this->min = null;
        $this->max = null;
        $this->multiple = null;
        $this->values = null;
    }
}
