<?php

declare(strict_types=1);

namespace CategoryOlx\Infrastructure;

use CategoryOlx\Domain\Attribute;
use CategoryOlx\Domain\AttributeStorageInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Doctrine\Persistence\ObjectRepository;

class AttributeRepository implements AttributeStorageInterface
{
    private EntityManagerInterface $entityManager;

    /**
     * @var EntityRepository<Attribute>
     */
    private ObjectRepository $repository;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
        $this->repository = $entityManager->getRepository(Attribute::class);
    }

    /**
     * @param array<string, mixed> $criteria
     * @param array<string, string>|null $orderBy
     * @return Collection<int, Attribute>
     */
    public function findBy(array $criteria, ?array $orderBy = null, ?int $limit = null, ?int $offset = null): Collection
    {
        $result = $this->repository->findBy($criteria, $orderBy, $limit, $offset);
        $collection = new ArrayCollection();

        foreach ($result as $olxAccount) {
            if ($olxAccount instanceof Attribute) {
                $collection->add($olxAccount);
            }
        }

        return $collection;
    }

    /**
     * @return Collection<int, Attribute>
     */
    public function findAll(): Collection
    {
        $result = $this->repository->findAll();
        $collection = new ArrayCollection();

        foreach ($result as $attribute) {
            if ($attribute instanceof Attribute) {
                $collection->add($attribute);
            }
        }

        return $collection;
    }

    public function persist(Attribute $attribute): void
    {
        $this->entityManager->persist($attribute);
    }

    public function deleteAll(): void
    {
        $dql = sprintf(
            'DELETE FROM %s',
            Attribute::class
        );
        $query = $this->entityManager->createQuery($dql);
        $query->getResult();
    }

    public function remove(Attribute $attribute): void
    {
        $this->entityManager->remove($attribute);
    }

    public function flush(): void
    {
        $this->entityManager->flush();
    }
}
