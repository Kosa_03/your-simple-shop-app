<?php

declare(strict_types=1);

namespace CategoryOlx\Infrastructure;

use Symfony\Component\Validator\Constraints as Assert;

class CategoryDto
{
    #[Assert\Regex(
        pattern: '/^[0-9]+$/i',
        message: 'Pole powinno składać się ze znaków 0-9',
        groups: ['default']
    )]
    public ?int $id;

    #[Assert\Length(
        max: 255,
        maxMessage: 'Pole nie powinno zawierać więcej niż 128 znaków',
        groups: ['default']
    )]
    public ?string $name;

    #[Assert\Regex(
        pattern: '/^[0-9]+$/i',
        message: 'Pole powinno składać się ze znaków 0-9',
        groups: ['default']
    )]
    public ?int $parentId;

    #[Assert\Regex(
        pattern: '/^[0-9]+$/i',
        message: 'Pole powinno składać się ze znaków 0-9',
        groups: ['default']
    )]
    public ?int $photosLimit;

    #[Assert\Type(
        type: 'bool',
        message: 'Pole powinno mieć wartość true / false',
        groups: ['default']
    )]
    public ?bool $leaf;

    public function __construct()
    {
        $this->clear();
    }

    public static function empty(): self
    {
        return new self();
    }

    /**
     * @param array<string, mixed> $category
     */
    public static function fromArray(array $category): self
    {
        $dto = new self();
        $dto->id = $category['id'];
        $dto->name = $category['name'];
        $dto->parentId = $category['parent_id'];
        $dto->photosLimit = $category['photos_limit'];
        $dto->leaf = $category['is_leaf'];
        return $dto;
    }

    public function clear(): void
    {
        $this->id = null;
        $this->name = null;
        $this->parentId = null;
        $this->photosLimit = null;
        $this->leaf = null;
    }
}
