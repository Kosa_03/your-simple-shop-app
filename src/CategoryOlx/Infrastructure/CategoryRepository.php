<?php

declare(strict_types=1);

namespace CategoryOlx\Infrastructure;

use CategoryOlx\Domain\Category;
use CategoryOlx\Domain\CategoryStorageInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Doctrine\Persistence\ObjectRepository;

class CategoryRepository implements CategoryStorageInterface
{
    private EntityManagerInterface $entityManager;

    /**
     * @var EntityRepository<Category>
     */
    private ObjectRepository $repository;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
        $this->repository = $entityManager->getRepository(Category::class);
    }

    /**
     * @return Collection<int, Category>
     */
    public function findAll(): Collection
    {
        $result = $this->repository->findAll();
        $collection = new ArrayCollection();

        foreach ($result as $category) {
            if ($category instanceof Category) {
                $collection->add($category);
            }
        }

        return $collection;
    }

    public function persist(Category $category): void
    {
        $this->entityManager->persist($category);
    }

    public function deleteAll(): void
    {
        $dql = sprintf(
            'DELETE FROM %s',
            Category::class
        );
        $query = $this->entityManager->createQuery($dql);
        $query->getResult();
    }

    public function remove(Category $category): void
    {
        $this->entityManager->remove($category);
    }

    public function flush(): void
    {
        $this->entityManager->flush();
    }
}
