<?php

declare(strict_types=1);

namespace CategoryOlx\Infrastructure;

use ApiRestOlx\Contract\OlxClientInterface;
use Symfony\Contracts\HttpClient\ResponseInterface;

class OlxClientAdapter
{
    public function __construct(
        private OlxClientInterface $olxClient
    ) {
    }

    public function accessToken(string $token): void
    {
        $this->olxClient->accessToken($token);
    }

    public function categories(): ResponseInterface
    {
        $response = $this->olxClient->getCategories();

        // model implementation

        return $response;
    }

    public function category(string $categoryId): ResponseInterface
    {
        $response = $this->olxClient->getCategory($categoryId);

        // model implementation

        return $response;
    }

    public function categoryAttributes(string $categoryId): ResponseInterface
    {
        $response = $this->olxClient->getCategoryAttributes($categoryId);

        // model implementation

        return $response;
    }
}
