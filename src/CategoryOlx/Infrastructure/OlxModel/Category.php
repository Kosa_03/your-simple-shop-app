<?php

declare(strict_types=1);

namespace CategoryOlx\Infrastructure\OlxModel;

class Category
{
    private int $id;
    private string $name;
    private int $parentId;
    private int $photosLimit;
    private bool $isLeaf;

    /**
     * @param array<string, mixed> $data
     */
    public static function data(array $data): self
    {
        $category = new self();
        $category->id = $data['id'];
        $category->name = $data['name'];
        $category->parentId = $data['parent_id'];
        $category->photosLimit = $data['photos_limit'];
        $category->isLeaf = $data['is_leaf'];
        return $category;
    }

    public function id(): int
    {
        return $this->id;
    }

    public function name(): string
    {
        return $this->name;
    }

    public function parentId(): int
    {
        return $this->parentId;
    }

    public function photosLimit(): int
    {
        return $this->photosLimit;
    }

    public function isLeaf(): bool
    {
        return $this->isLeaf;
    }
}
