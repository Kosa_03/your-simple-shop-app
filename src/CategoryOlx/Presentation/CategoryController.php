<?php

declare(strict_types=1);

namespace CategoryOlx\Presentation;

use CategoryOlx\Presentation\Dashboard\DashboardService;
use CategoryOlx\Presentation\DeletingAll\DeleteAllViewService;
use CategoryOlx\Presentation\DownloadAttributes\DownloadAttributesService;
use CategoryOlx\Presentation\DownloadCategories\DownloadCategoriesService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CategoryController extends AbstractController
{
    public function __construct(
        private DashboardService $dashboardService,
        private DeleteAllViewService $deleteAllViewService,
        private DownloadAttributesService $downloadAttributesService,
        private DownloadCategoriesService $downloadCategoriesService
    ) {
    }

    #[Route('/rest_api/olx/category', name: 'rest_api_olx_category')]
    public function dashboard(Request $request): Response
    {
        return $this->dashboardService->dashboard($request);
    }

    #[Route('/rest_api/olx/category/attribute/download', name: 'rest_api_olx_category_attribute_download')]
    public function downloadAttributes(Request $request): Response
    {
        return $this->downloadAttributesService->download($request);
    }

    #[Route('/rest_api/olx/category/download', name: 'rest_api_olx_category_download')]
    public function downloadCategories(Request $request): Response
    {
        return $this->downloadCategoriesService->download($request);
    }

    #[Route('/rest_api/olx/category/delete_all', name: 'rest_api_olx_category_delete_all')]
    public function deleteAll(Request $request): Response
    {
        return $this->deleteAllViewService->deleteAll($request);
    }
}
