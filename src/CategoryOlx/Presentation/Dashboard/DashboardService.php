<?php

namespace CategoryOlx\Presentation\Dashboard;

use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Twig\Environment;

class DashboardService
{
    public function __construct(
        private Environment $twig,
        private FormFactoryInterface $formFactory,
        private UrlGeneratorInterface $urlGenerator
    ) {
    }

    public function dashboard(Request $request): Response
    {
        $form = $this->formFactory->create(DownloadCategoryType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            $accountList = $form->getData();

            $categoryButton = $form->get('downloadCategoriesSubmit');
            if ($categoryButton->isClicked()) {
                return $this->redirectToDownloadCategories($accountList['olxAccountList']['olxAccount']);
            }

            $attributeButton = $form->get('downloadCategoryAttributesSubmit');
            if ($attributeButton->isClicked()) {
                return $this->redirectToDownloadCategoryAttributes($accountList['olxAccountList']['olxAccount']);
            }
        }

        return new Response($this->renderView($form));
    }

    private function redirectToDownloadCategories(int $olxAccount): RedirectResponse
    {
        return new RedirectResponse($this->urlGenerator->generate('rest_api_olx_category_download', [
            'olxAccountId' => $olxAccount
        ]));
    }

    private function redirectToDownloadCategoryAttributes(int $olxAccount): RedirectResponse
    {
        return new RedirectResponse($this->urlGenerator->generate('rest_api_olx_category_attribute_download', [
            'olxAccountId' => $olxAccount
        ]));
    }

    private function renderView(FormInterface $form): string
    {
        return $this->twig->render('@category_olx/dashboard.html.twig', [
            'downloadOlxCategoryForm' => $form->createView()
        ]);
    }
}
