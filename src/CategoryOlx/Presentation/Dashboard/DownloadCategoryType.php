<?php

declare(strict_types=1);

namespace CategoryOlx\Presentation\Dashboard;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;

class DownloadCategoryType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('olxAccountList', SelectAccountType::class, [])
            ->add('downloadCategoriesSubmit', SubmitType::class, [
                'label' => 'Pobierz',
                'attr' => [
                    'class' => 'btn-outline-gold'
                ]
            ])
            ->add('downloadCategoryAttributesSubmit', SubmitType::class, [
                'label' => 'Pobierz',
                'attr' => [
                    'class' => 'btn-outline-gold'
                ]
            ]);
    }
}
