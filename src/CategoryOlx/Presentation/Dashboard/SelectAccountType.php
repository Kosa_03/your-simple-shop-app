<?php

declare(strict_types=1);

namespace CategoryOlx\Presentation\Dashboard;

use ACL\AccountOlxToCategoryOlx\AccountFacadeInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;

class SelectAccountType extends AbstractType
{
    public function __construct(
        private AccountFacadeInterface $accountAclFacade
    ) {
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder->add('olxAccount', ChoiceType::class, [
            'label' => 'Wybierz konto Olx',
            'multiple' => false,
            'expanded' => false,
            'choices' => $this->accountAclFacade->accountListForSelectType(),
            'label_attr' => [
                'class' => 'yss-form-label'
            ],
            'attr' => [
                'class' => 'form-select'
            ]
        ]);
    }
}
