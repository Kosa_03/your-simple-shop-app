<?php

declare(strict_types=1);

namespace CategoryOlx\Presentation\DeletingAll;

use CategoryOlx\Application\DeletingAll\DeleteAll;
use Common\Messenger\Command;
use Common\Messenger\CommandBusInterface;
use Symfony\Component\Form\ClickableInterface;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Twig\Environment;

class DeleteAllViewService
{
    private FlashBagInterface $flashBag;

    public function __construct(
        private CommandBusInterface $commandBus,
        private Environment $twig,
        private FormFactoryInterface $formFactory,
        private UrlGeneratorInterface $urlGenerator,
        RequestStack $requestStack
    ) {
        $this->flashBag = $requestStack->getSession()->getFlashBag();
    }

    public function deleteAll(Request $request): Response
    {
        $form = $this->formFactory->create(ConfirmType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            if ($this->isButtonClicked($form, 'deleteAll')) {
                $this->runCommand(DeleteAll::new());
                $this->deletedSuccessfullyFlashMsg();
            }
            if ($this->isButtonClicked($form, 'goBack')) {
                $this->goBackFlashMsg();
            }

            return $this->redirectTo('rest_api_olx_category');
        }

        return new Response($this->renderView($form));
    }

    private function renderView(FormInterface $form): string
    {
        return $this->twig->render('@category_olx/delete_all_category.html.twig', [
            'form' => $form->createView()
        ]);
    }

    private function runCommand(Command $command): void
    {
        $this->commandBus->dispatch($command);
    }

    private function isButtonClicked(FormInterface $form, string $fieldName): bool
    {
        $field = $form->get($fieldName);
        if ($field instanceof ClickableInterface) {
            return $field->isClicked();
        }
        return false;
    }

    private function redirectTo(string $routeName): RedirectResponse
    {
        return new RedirectResponse($this->urlGenerator->generate($routeName));
    }

    private function deletedSuccessfullyFlashMsg(): void
    {
        $this->flashBag->add('success', 'Kategorie olx i parametry zostały usunięte pomyślnie');
    }

    private function goBackFlashMsg(): void
    {
        $this->flashBag->add('warning', 'Kategorie olx i atrybuty nie zostały usunięte');
    }
}
