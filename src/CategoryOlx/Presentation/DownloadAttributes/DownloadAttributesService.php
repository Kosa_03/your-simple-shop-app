<?php

declare(strict_types=1);

namespace CategoryOlx\Presentation\DownloadAttributes;

use ACL\AccountOlxToCategoryOlx\AccountFacadeInterface;
use CategoryOlx\Application\DownloadingAttributes\DownloadAttributes;
use Common\Messenger\CommandBusInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class DownloadAttributesService
{
    private FlashBagInterface $flashBag;

    public function __construct(
        private AccountFacadeInterface $accountFacade,
        private CommandBusInterface $commandBus,
        private UrlGeneratorInterface $urlGenerator,
        RequestStack $requestStack
    ) {
        $this->flashBag = $requestStack->getSession()->getFlashBag();
    }

    public function download(Request $request): Response
    {
        $accountId = $this->filterAccountIdFrom($request);
        if (null === $accountId) {
            $this->accountNotFoundFlashMsg();
            return new RedirectResponse($this->urlGenerator->generate('rest_api_olx_category'));
        }

        $tokenDto = $this->accountFacade->findAccessTokenFor($accountId);
        if (null === $tokenDto) {
            $this->accountNotFoundFlashMsg();
            return new RedirectResponse($this->urlGenerator->generate('rest_api_olx_category'));
        }

        $this->commandBus->dispatch(new DownloadAttributes($tokenDto->accessToken));
        $this->requestHasBeenAcceptedFlashMsg();

        return new RedirectResponse($this->urlGenerator->generate('rest_api_olx_category'));
    }

    private function filterAccountIdFrom(Request $request): ?int
    {
        return filter_var($request->get('olxAccountId'), FILTER_VALIDATE_INT, FILTER_NULL_ON_FAILURE);
    }

    private function accountNotFoundFlashMsg(): void
    {
        $this->flashBag->add('danger', 'Konto olx nie zostało znalezione');
    }

    private function requestHasBeenAcceptedFlashMsg(): void
    {
        $this->flashBag->add('success', 'Pobranie atrybutów zostało przyjęte.');
    }
}
