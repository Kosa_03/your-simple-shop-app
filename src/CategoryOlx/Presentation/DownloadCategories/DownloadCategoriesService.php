<?php

declare(strict_types=1);

namespace CategoryOlx\Presentation\DownloadCategories;

use ACL\AccountOlxToCategoryOlx\AccountFacadeInterface;
use ApiRestOlx\Contract\OlxClientInterface;
use CategoryOlx\Application\SavingCategories\SaveCategories;
use Common\Messenger\CommandBusInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpClient\Exception\ClientException;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Contracts\HttpClient\ResponseInterface;

class DownloadCategoriesService
{
    private FlashBagInterface $flashBag;

    public function __construct(
        private AccountFacadeInterface $accountFacade,
        private CommandBusInterface $commandBus,
        private OlxClientInterface $olxClient,
        private LoggerInterface $logger,
        private UrlGeneratorInterface $urlGenerator,
        RequestStack $requestStack
    ) {
        $this->flashBag = $requestStack->getSession()->getFlashBag();
    }

    public function download(Request $request): Response
    {
        $accountId = $this->filterAccountIdFrom($request);
        if (null === $accountId) {
            $this->accountNotFoundFlashMsg();
            return new RedirectResponse($this->urlGenerator->generate('rest_api_olx_category'));
        }

        $tokenDto = $this->accountFacade->findAccessTokenFor($accountId);
        if (null === $tokenDto) {
            $this->accountNotFoundFlashMsg();
            return new RedirectResponse($this->urlGenerator->generate('rest_api_olx_category'));
        }

        try {
            $this->downloadCategories($tokenDto->accessToken);
            $this->requestHasBeenAcceptedFlashMsg();
        } catch (ClientException $e) {
            $this->logger->error($e->getMessage(), ['exception' => $e]);
            $this->somethingWentWrongFlashMsg($e->getResponse());
        }

        return new RedirectResponse($this->urlGenerator->generate('rest_api_olx_category'));
    }

    private function filterAccountIdFrom(Request $request): ?int
    {
        return filter_var($request->get('olxAccountId'), FILTER_VALIDATE_INT, FILTER_NULL_ON_FAILURE);
    }

    private function downloadCategories(string $accessToken): void
    {
        $this->olxClient->accessToken($accessToken);
        $response = $this->olxClient->getCategories();
        $this->commandBus->dispatch(new SaveCategories($response->toArray()['data']));
    }

    private function accountNotFoundFlashMsg(): void
    {
        $this->flashBag->add('danger', 'Konto olx nie zostało znalezione');
    }

    private function requestHasBeenAcceptedFlashMsg(): void
    {
        $this->flashBag->add('success', 'Żądanie zostało przyjęte.');
    }

    private function somethingWentWrongFlashMsg(ResponseInterface $response): void
    {
        $content = $response->toArray(false);
        $this->flashBag->add('danger', $content['error_human_title'] ?? 'Wystąpił nieoczekiwany błąd. Przepraszamy.');
    }
}
