<?php

declare(strict_types=1);

namespace CategoryOlx\Presentation;

class TokenDto
{
    public ?string $accessToken;

    private function __construct()
    {
    }

    public static function from(?string $accessToken): self
    {
        $dto = new self();
        $dto->accessToken = $accessToken;
        return $dto;
    }
}
