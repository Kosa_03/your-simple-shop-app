<?php

declare(strict_types=1);

namespace Common;

class AppResult
{
    public const SUCCESS = 'success';
    public const FAILURE = 'failure';

    private string $result;
    private ?string $reason;

    private function __construct()
    {
    }

    public static function success(): self
    {
        $result = new self();
        $result->result = self::SUCCESS;
        $result->reason = null;
        return $result;
    }

    public static function failure(string $reason): self
    {
        $result = new self();
        $result->result = self::FAILURE;
        $result->reason = $reason;
        return $result;
    }

    public function isSuccessful(): bool
    {
        return $this->result === self::SUCCESS;
    }

    public function isFailure(): bool
    {
        return $this->result === self::FAILURE;
    }

    public function reason(): string
    {
        return $this->reason ?? 'ok';
    }
}
