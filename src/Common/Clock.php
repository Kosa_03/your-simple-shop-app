<?php

declare(strict_types=1);

namespace Common;

use DateTimeImmutable;

interface Clock
{
    public function now(string $string = 'now'): DateTimeImmutable;
}
