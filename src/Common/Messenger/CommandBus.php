<?php

declare(strict_types=1);

namespace Common\Messenger;

use Common\AppResult;
use Symfony\Component\Messenger\HandleTrait;
use Symfony\Component\Messenger\MessageBusInterface;

class CommandBus implements CommandBusInterface
{
    use HandleTrait;

    public function __construct(MessageBusInterface $transactionalBus)
    {
        $this->messageBus = $transactionalBus;
    }

    public function dispatch(Command $command): AppResult
    {
        $result = $this->handle($command);
        if ($result instanceof AppResult) {
            return $result;
        }
        return AppResult::success();
    }
}
