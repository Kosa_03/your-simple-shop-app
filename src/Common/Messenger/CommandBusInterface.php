<?php

namespace Common\Messenger;

use Common\AppResult;

interface CommandBusInterface
{
    public function dispatch(Command $command): AppResult;
}
