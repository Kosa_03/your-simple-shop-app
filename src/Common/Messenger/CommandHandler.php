<?php

declare(strict_types=1);

namespace Common\Messenger;

interface CommandHandler
{
    // Used to tagging transactional bus
}
