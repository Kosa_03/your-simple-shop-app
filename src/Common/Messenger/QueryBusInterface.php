<?php

declare(strict_types=1);

namespace Common\Messenger;

interface QueryBusInterface
{
    public function run(Query $query): mixed;
}
