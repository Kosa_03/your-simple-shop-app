<?php

declare(strict_types=1);

namespace Common;

class Pagination
{
    public string $sortColumn;
    public string $sortMethod;
    public int $itemsPerPage;
    public int $itemsTotal = 0;

    private function __construct()
    {
    }

    public function __serialize(): array
    {
        return [
            'sortColumn' => $this->sortColumn,
            'sortMethod' => $this->sortMethod,
            'itemsPerPage' => $this->itemsPerPage,
            'itemsTotal' => $this->itemsTotal
        ];
    }

    /**
     * @param array<string|int, mixed> $data
     */
    public function __unserialize(array $data): void
    {
        $this->sortColumn = $data['sortColumn'];
        $this->sortMethod = $data['sortMethod'];
        $this->itemsPerPage = $data['itemsPerPage'];
        $this->itemsTotal = $data['itemsTotal'];
    }

    public static function create(string $sortColumn, string $sortMethod, int $itemsPerPage): self
    {
        $pagination = new self();
        $pagination->sortColumn = $sortColumn;
        $pagination->sortMethod = $sortMethod;
        $pagination->itemsPerPage = $itemsPerPage;
        return $pagination;
    }

    public function lastPage(): float
    {
        return ceil($this->itemsTotal / $this->itemsPerPage);
    }
}
