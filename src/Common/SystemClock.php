<?php

declare(strict_types=1);

namespace Common;

use DateTimeImmutable;

class SystemClock implements Clock
{
    public function now(string $string = 'now'): DateTimeImmutable
    {
        return new DateTimeImmutable($string);
    }
}
