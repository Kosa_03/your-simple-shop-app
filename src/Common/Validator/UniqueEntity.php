<?php

declare(strict_types=1);

namespace Common\Validator;

use Symfony\Component\Validator\Constraint;

#[\Attribute]
class UniqueEntity extends Constraint
{
    public string $message = 'This value is already used.';

    /**
     * @var class-string<object>
     */
    public string $entityClass;
    public string $entityField;
}
