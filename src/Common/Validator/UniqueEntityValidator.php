<?php

declare(strict_types=1);

namespace Common\Validator;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;

class UniqueEntityValidator extends ConstraintValidator
{
    private EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function validate($value, Constraint $constraint): void
    {
        if (!$constraint instanceof UniqueEntity) {
            throw new UnexpectedTypeException($constraint, UniqueEntity::class);
        }

        $result = $this->entityManager->getRepository($constraint->entityClass)->findBy([
            $constraint->entityField => $value
        ]);

        if (count($result) == 0) {
            return;
        }

        $entityDto = $this->context->getObject();
        if (!is_null($entityDto) && property_exists($entityDto, 'id')) {
            $entityDtoId = $entityDto->id;
        } else {
            $entityDtoId = null;
        }

        foreach ($result as $entity) {
            if (method_exists($entity, 'getId') && $entity->getId() != $entityDtoId) {
                $this->context->buildViolation($constraint->message)
                    ->setParameter('{{ value }}', (string) $value)
                    ->addViolation();
            }
        }
    }
}
