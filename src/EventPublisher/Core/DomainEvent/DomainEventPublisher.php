<?php

declare(strict_types=1);

namespace EventPublisher\Core\DomainEvent;

use Symfony\Component\Messenger\MessageBusInterface;

class DomainEventPublisher
{
    public function __construct(
        private MessageBusInterface $eventBus
    ) {
    }

    /**
     * @param array<int, mixed> $domainEvents
     */
    public function publish(array $domainEvents): void
    {
        foreach ($domainEvents as $event) {
            if ($event instanceof DomainEvent) {
                $this->eventBus->dispatch($event);
            }
        }
    }
}
