<?php

declare(strict_types=1);

namespace EventPublisher\Core;

use EventPublisher\Core\DomainEvent\DomainEvent;
use EventPublisher\Core\DomainEvent\DomainEventPublisher;
use EventPublisher\Core\IntegrationEvent\IntegrationEvent;
use EventPublisher\Core\IntegrationEvent\IntegrationEventPublisher;

class EventPublisher implements EventPublisherInterface
{
    public function __construct(
        private DomainEventPublisher $domainEventPublisher,
        private IntegrationEventPublisher $integrationEventPublisher
    ) {
    }

    public function publish(array $events): void
    {
        $domainEvents = array_values(
            array_filter($events, fn($event) => $event instanceof DomainEvent)
        );
        $integrationEvents = array_values(
            array_filter($events, fn($event) => $event instanceof IntegrationEvent)
        );
        $this->domainEventPublisher->publish($domainEvents);
        $this->integrationEventPublisher->storeToOutbox($integrationEvents);
    }
}
