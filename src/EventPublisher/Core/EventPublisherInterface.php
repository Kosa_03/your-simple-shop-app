<?php

declare(strict_types=1);

namespace EventPublisher\Core;

interface EventPublisherInterface
{
    /**
     * @param array<int, mixed> $events
     */
    public function publish(array $events): void;
}
