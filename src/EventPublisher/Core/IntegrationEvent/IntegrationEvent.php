<?php

declare(strict_types=1);

namespace EventPublisher\Core\IntegrationEvent;

use EventPublisher\Shared\EventId;

interface IntegrationEvent
{
    public function eventId(): EventId;
}
