<?php

declare(strict_types=1);

namespace EventPublisher\Core\IntegrationEvent;

use Common\Clock;
use Symfony\Component\Messenger\MessageBusInterface;
use Throwable;

class IntegrationEventPublisher
{
    public function __construct(
        private MessageBusInterface $eventBus,
        private OutboxMessageRepository $outboxMessageRepository,
        private Clock $clock
    ) {
    }

    /**
     * @param array<int, mixed> $integrationEvents
     */
    public function storeToOutbox(array $integrationEvents): void
    {
        foreach ($integrationEvents as $event) {
            if ($event instanceof IntegrationEvent) {
                $this->outboxMessageRepository->save(OutboxMessage::create($event, $this->clock->now()));
            }
        }
    }

    public function publishNotProcessedYet(): void
    {
        $this->outboxMessageRepository->startTransaction();
        try {
            $messageList = $this->outboxMessageRepository->findNotProcessed();
            foreach ($messageList as $message) {
                $this->eventBus->dispatch($message->getEvent());
            }
            $this->outboxMessageRepository->markAsSent($messageList);

            $this->outboxMessageRepository->commit();
        } catch (Throwable $e) {
            $this->outboxMessageRepository->rollback();
            throw $e;
        }
    }
}
