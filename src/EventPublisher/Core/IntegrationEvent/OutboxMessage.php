<?php

declare(strict_types=1);

namespace EventPublisher\Core\IntegrationEvent;

use DateTimeImmutable;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\Table;

#[Entity]
#[Table(name: 'outbox_message')]
class OutboxMessage
{
    #[Id]
    #[GeneratedValue]
    #[Column(name: 'id', type: 'integer')]
    private int $id;

    #[Column(name: 'event_id', type: 'string', length: 80)]
    private string $eventId;

    #[Column(name: 'data', type: 'text')]
    private string $data;

    #[Column(name: 'occurred_at', type: 'datetime_immutable')]
    private DateTimeImmutable $occurredAt;

    #[Column(name: 'processed_at', type: 'datetime_immutable', nullable: true)]
    private ?DateTimeImmutable $processedAt = null;

    private function __construct()
    {
    }

    public static function create(IntegrationEvent $domainEvent, DateTimeImmutable $occurredAt): self
    {
        $event = new self();
        $event->eventId = $domainEvent->eventId()->toString();
        $event->data = serialize($domainEvent);
        $event->occurredAt = $occurredAt;
        return $event;
    }

    public function markAsSent(DateTimeImmutable $processedAt): void
    {
        $this->processedAt = $processedAt;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getEventId(): string
    {
        return $this->eventId;
    }

    public function getEvent(): IntegrationEvent
    {
        return unserialize($this->data);
    }

    public function getOccurredAt(): DateTimeImmutable
    {
        return $this->occurredAt;
    }

    public function getProcessedAt(): ?DateTimeImmutable
    {
        return $this->processedAt;
    }
}
