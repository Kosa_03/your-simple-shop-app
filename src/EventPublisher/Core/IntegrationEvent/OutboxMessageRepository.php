<?php

declare(strict_types=1);

namespace EventPublisher\Core\IntegrationEvent;

use Common\Clock;
use Doctrine\DBAL\Connection;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Doctrine\Persistence\ObjectRepository;

class OutboxMessageRepository
{
    private Connection $connection;

    /**
     * @var EntityRepository<OutboxMessage>
     */
    private ObjectRepository $repository;

    public function __construct(
        private EntityManagerInterface $entityManager,
        private Clock $clock
    ) {
        $this->repository = $this->entityManager->getRepository(OutboxMessage::class);
        $this->connection = $this->entityManager->getConnection();
    }

    /**
     * @return array<int, OutboxMessage>
     */
    public function findNotProcessed(): array
    {
        return $this->repository->findBy([
            'processedAt' => null
        ]);
    }

    /**
     * @return array<int, OutboxMessage>
     */
    public function findAll(): array
    {
        return $this->repository->findAll();
    }

    /**
     * @param array<int, OutboxMessage> $outboxMessages
     */
    public function markAsSent(array $outboxMessages): void
    {
        foreach ($outboxMessages as $message) {
            if ($message instanceof OutboxMessage) {
                $message->markAsSent($this->clock->now());
                $this->save($message);
            }
        }
    }

    public function save(OutboxMessage $outboxMessage): void
    {
        $this->entityManager->persist($outboxMessage);
        $this->entityManager->flush();
    }

    public function startTransaction(): void
    {
        $this->connection->beginTransaction();
    }

    public function commit(): void
    {
        $this->connection->commit();
    }

    public function rollback(): void
    {
        $this->connection->rollback();
    }
}
