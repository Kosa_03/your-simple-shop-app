<?php

declare(strict_types=1);

namespace EventPublisher\Core\IntegrationEvent;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class PublishIntegrationEventsCommand extends Command
{
    public function __construct(
        private IntegrationEventPublisher $eventPublisher
    ) {
        parent::__construct();
    }

    protected function configure(): void
    {
        $this->setName('domain-event:publish')
            ->setDescription('Publish domain events these have not published yet.');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->eventPublisher->publishNotProcessedYet();

        return Command::SUCCESS;
    }
}
