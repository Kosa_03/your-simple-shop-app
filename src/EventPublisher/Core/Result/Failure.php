<?php

declare(strict_types=1);

namespace EventPublisher\Core\Result;

use EventPublisher\Shared\Result;

class Failure extends Result
{
    protected string $reason;

    public function __construct(string $reason)
    {
        $this->reason = $reason;
    }
}
