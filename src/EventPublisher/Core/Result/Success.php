<?php

declare(strict_types=1);

namespace EventPublisher\Core\Result;

use EventPublisher\Core\DomainEvent\DomainEvent;
use EventPublisher\Core\IntegrationEvent\IntegrationEvent;
use EventPublisher\Shared\Result;

class Success extends Result
{
    /**
     * @var array<int, DomainEvent|IntegrationEvent>
     */
    protected array $events = [];

    /**
     * @param array<int, mixed> $events
     */
    public function __construct(array $events)
    {
        $domainEvents = array_values(
            array_filter($events, fn($event) => $event instanceof DomainEvent)
        );
        $integrationEvents = array_values(
            array_filter($events, fn($event) => $event instanceof IntegrationEvent)
        );

        $this->events = array_merge($domainEvents, $integrationEvents);
    }
}
