<?php

declare(strict_types=1);

namespace EventPublisher\Shared;

use Symfony\Component\Uid\Uuid;

final class EventId
{
    private Uuid $uuid;

    private function __construct()
    {
    }

    public static function v4(): self
    {
        $eventId = new self();
        $eventId->uuid = Uuid::v4();
        return $eventId;
    }

    public function toString(): string
    {
        return $this->uuid->toRfc4122();
    }

    public function isEquals(EventId $eventId): bool
    {
        return $this->uuid->equals($eventId->uuid);
    }
}
