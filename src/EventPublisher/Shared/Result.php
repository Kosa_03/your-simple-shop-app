<?php

declare(strict_types=1);

namespace EventPublisher\Shared;

use EventPublisher\Core\DomainEvent\DomainEvent;
use EventPublisher\Core\IntegrationEvent\IntegrationEvent;
use EventPublisher\Core\Result\Failure;
use EventPublisher\Core\Result\Success;

abstract class Result
{
    /**
     * @param array<int, mixed> $events
     */
    public static function success(array $events): Success
    {
        return new Success($events);
    }

    public static function failure(string $reason): Failure
    {
        return new Failure($reason);
    }

    public function isFailure(): bool
    {
        return $this instanceof Failure;
    }

    public function isSuccessful(): bool
    {
        return $this instanceof Success;
    }

    public function reason(): string
    {
        if ($this instanceof Failure) {
            return $this->reason;
        }

        return 'OK';
    }

    /**
     * @return array<int, DomainEvent|IntegrationEvent>
     */
    public function events(): array
    {
        if ($this instanceof Success) {
            return $this->events;
        }

        return [];
    }
}
