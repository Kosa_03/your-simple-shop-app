<?php

declare(strict_types=1);

namespace Mail;

use Mail\Type\EmailInterface;
use Twig\Environment;

abstract class AbstractEmail implements EmailInterface
{
    protected Environment $twig;

    /**
     * @var array<string|int, string> $sendTo
     */
    protected array $sendTo;

    /**
     * @var array<string|int, string> $sendFrom
     */
    protected array $sendFrom;
    protected string $subject;

    /**
     * @var array<string, mixed> $data
     */
    protected array $data;

    public function __construct(Environment $twig)
    {
        $this->twig = $twig;
    }

    public function emailSettings(array $sendTo, array $sendFrom, string $subject, array $data = []): void
    {
        $this->sendTo = $sendTo;
        $this->sendFrom = $sendFrom;
        $this->subject = $subject;
        $this->data = $data;
    }

    public function sendTo(): array
    {
        return $this->sendTo;
    }

    public function sendFrom(): array
    {
        return $this->sendFrom;
    }

    public function subject(): string
    {
        return $this->subject;
    }
}
