<?php

declare(strict_types=1);

namespace Mail;

use Mail\Type\EmailInterface;
use Twig\Environment;

class EmailFactory
{
    public function __construct(
        private Environment $twig
    ) {
    }

    /**
     * @param array<string|int, string> $sendTo
     * @param array<string|int, string> $sendFrom
     * @param array<string, mixed> $data
     */
    public function create(
        string $emailClass,
        array $sendTo,
        array $sendFrom,
        string $subject,
        array $data = []
    ): EmailInterface {
        try {
            $email = new $emailClass($this->twig);
            if (!$email instanceof EmailInterface) {
                throw new InvalidEmailException(
                    sprintf('Email class "%s" doesn\'t implement "%s"!', $emailClass, EmailInterface::class)
                );
            }
            $email->emailSettings($sendTo, $sendFrom, $subject, $data);
        } catch (\Throwable $e) {
            if ($e instanceof InvalidEmailException) {
                throw $e;
            }
            throw new InvalidEmailException(sprintf('Email class "%s" is invalid!', $emailClass), 0, $e);
        }

        return $email;
    }
}
