<?php

declare(strict_types=1);

namespace Mail;

use Mail\Type\EmailInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;

class EmailSender
{
    public function __construct(
        private MailerInterface $mailer
    ) {
    }

    public function send(EmailInterface $email): void
    {
        $message = new Email();
        $message->from(...array_values($email->sendFrom()))
            ->to(...array_values($email->sendTo()))
            ->subject($email->subject())
            ->html($email->htmlEmail())
            ->text($email->plainEmail());
        $this->mailer->send($message);
    }
}
