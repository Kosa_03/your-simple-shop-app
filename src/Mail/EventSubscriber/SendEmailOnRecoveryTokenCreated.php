<?php

declare(strict_types=1);

namespace Mail\EventSubscriber;

use Mail\EmailFactory;
use Mail\EmailSender;
use Mail\Type\RecoverPasswordEmail;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use User\Contract\Event\RecoveryTokenCreated;

class SendEmailOnRecoveryTokenCreated implements MessageHandlerInterface
{
    public function __construct(
        private EmailFactory $emailFactory,
        private EmailSender $emailSender,
        private UrlGeneratorInterface $urlGenerator
    ) {
    }

    public function __invoke(RecoveryTokenCreated $event): void
    {
        $data = [
            'firstName' => $event->getFirstName(),
            'createPasswordUrl' => $this->createPasswordUrl($event->getToken())
        ];

        $email = $this->emailFactory->create(
            RecoverPasswordEmail::class,
            [$event->getEmail()],
            ['no-reply@yoursimpleshop.pl' => 'YourSimpleShop'],
            'Prośba o nowe hasło',
            $data
        );

        $this->emailSender->send($email);
    }

    private function createPasswordUrl(string $token): string
    {
        return $this->urlGenerator->generate(
            'app_create_password',
            ['token' => $token],
            UrlGeneratorInterface::ABSOLUTE_URL
        );
    }
}
