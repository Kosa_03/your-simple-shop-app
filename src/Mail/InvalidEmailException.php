<?php

declare(strict_types=1);

namespace Mail;

use LogicException;
use Throwable;

class InvalidEmailException extends LogicException implements Throwable
{
}
