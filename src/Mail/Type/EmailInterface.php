<?php

declare(strict_types=1);

namespace Mail\Type;

interface EmailInterface
{
    /**
     * @param array<string|int, string> $sendTo
     * @param array<string|int, string> $sendFrom
     * @param array<string, mixed> $data
     */
    public function emailSettings(array $sendTo, array $sendFrom, string $subject, array $data = []): void;

    /**
     * @return array<string|int, string>
     */
    public function sendTo(): array;
    /**
     * @return array<string|int, string>
     */
    public function sendFrom(): array;
    public function subject(): string;
    public function plainEmail(): string;
    public function htmlEmail(): string;
}
