<?php

declare(strict_types=1);

namespace Mail\Type;

use Mail\AbstractEmail;

class RecoverPasswordEmail extends AbstractEmail
{
    public function htmlEmail(): string
    {
        return $this->twig->render('@mail/type/recover_password.html.twig', [
            'firstName' => $this->data['firstName'],
            'createPasswordUrl' => $this->data['createPasswordUrl']
        ]);
    }

    public function plainEmail(): string
    {
        return $this->twig->render('@mail/type/recover_password.plain.twig', [
            'firstName' => $this->data['firstName'],
            'createPasswordUrl' => $this->data['createPasswordUrl']
        ]);
    }
}
