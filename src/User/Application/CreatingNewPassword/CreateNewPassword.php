<?php

declare(strict_types=1);

namespace User\Application\CreatingNewPassword;

use Common\Messenger\Command;

class CreateNewPassword implements Command
{
    private string $login;
    private string $token;
    private string $plainPassword;

    private function __construct()
    {
    }

    public static function new(string $login, string $token, string $plainPassword): self
    {
        $command = new self();
        $command->login = $login;
        $command->token = $token;
        $command->plainPassword = $plainPassword;
        return $command;
    }

    public function login(): string
    {
        return $this->login;
    }

    public function token(): string
    {
        return $this->token;
    }

    public function plainPassword(): string
    {
        return $this->plainPassword;
    }
}
