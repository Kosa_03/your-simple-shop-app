<?php

declare(strict_types=1);

namespace User\Application\CreatingNewPassword;

use Common\AppResult;
use Common\Clock;
use Common\Messenger\CommandHandler;
use EventPublisher\Core\EventPublisherInterface;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use User\Domain\Password;
use User\Domain\RecoveryTokenStorageInterface;
use User\Domain\User;
use User\Domain\UserStorageInterface;

class CreateNewPasswordHandler implements CommandHandler
{
    public function __construct(
        private Clock $clock,
        private EventPublisherInterface $eventPublisher,
        private UserPasswordHasherInterface $passwordHasher,
        private RecoveryTokenStorageInterface $recoveryTokenStorage,
        private UserStorageInterface $userStorage
    ) {
    }

    public function __invoke(CreateNewPassword $command): AppResult
    {
        $user = $this->userStorage->findByLogin($command->login());
        if (null === $user) {
            return AppResult::failure(User::REASON_USER_NOT_FOUND);
        }

        try {
            $password = Password::create($user, $this->passwordHasher, $command->plainPassword());
        } catch (\InvalidArgumentException $e) {
            return AppResult::failure(User::REASON_PASSWORD_TOO_SHORT);
        }

        $result = $user->createNewPassword($password, $command->token(), $this->clock->now());
        if ($user->hasRecoveryToken() && $result->isSuccessful()) {
            $this->recoveryTokenStorage->remove($user->getRecoveryToken());
            $user->removeRecoveryToken();
        }
        if ($user->hasRecoveryToken() && $result->reason() === User::REASON_EXPIRED_TOKEN) {
            $this->recoveryTokenStorage->remove($user->getRecoveryToken());
            $user->removeRecoveryToken();
        }
        $this->eventPublisher->publish($result->events());

        if ($result->isFailure()) {
            return AppResult::failure($result->reason());
        }
        return AppResult::success();
    }
}
