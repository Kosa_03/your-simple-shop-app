<?php

declare(strict_types=1);

namespace User\Application\CreatingUser;

use Common\Messenger\Command;
use User\Domain\UserDto;

class CreateUser implements Command
{
    private UserDto $userDto;

    private function __construct()
    {
    }

    public static function new(UserDto $userDto): self
    {
        $command = new self();
        $command->userDto = $userDto;
        return $command;
    }

    public function userDto(): UserDto
    {
        return $this->userDto;
    }
}
