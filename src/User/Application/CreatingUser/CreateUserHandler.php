<?php

declare(strict_types=1);

namespace User\Application\CreatingUser;

use Common\Messenger\CommandHandler;
use EventPublisher\Core\EventPublisherInterface;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use User\Domain\Roles;
use User\Domain\User;
use User\Domain\UserStorageInterface;

class CreateUserHandler implements CommandHandler
{
    public function __construct(
        private EventPublisherInterface $eventPublisher,
        private UserStorageInterface $userStorage,
        private UserPasswordHasherInterface $passwordHasher
    ) {
    }

    public function __invoke(CreateUser $command): void
    {
        $userDto = $command->userDto();

        $user = User::create(
            $userDto->login,
            $userDto->email,
            $userDto->firstName,
            $userDto->lastName,
            Roles::create($userDto->roles),
            $userDto->active,
            $this->passwordHasher,
            $userDto->plainPassword
        );
        $this->userStorage->save($user);
        $this->eventPublisher->publish([]);
    }
}
