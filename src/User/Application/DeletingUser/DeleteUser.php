<?php

declare(strict_types=1);

namespace User\Application\DeletingUser;

use Common\Messenger\Command;

class DeleteUser implements Command
{
    private int $userId;

    private function __construct()
    {
    }

    public static function new(int $userId): self
    {
        $command = new self();
        $command->userId = $userId;
        return $command;
    }

    public function userId(): int
    {
        return $this->userId;
    }
}
