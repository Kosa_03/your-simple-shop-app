<?php

declare(strict_types=1);

namespace User\Application\DeletingUser;

use Common\Messenger\CommandHandler;
use EventPublisher\Core\EventPublisherInterface;
use User\Domain\UserStorageInterface;

class DeleteUserHandler implements CommandHandler
{
    public function __construct(
        private EventPublisherInterface $eventPublisher,
        private UserStorageInterface $userStorage
    ) {
    }

    public function __invoke(DeleteUser $command): void
    {
        $user = $this->userStorage->findById($command->userId());
        if (null === $user) {
            return;
        }
        $this->userStorage->remove($user);
        $this->eventPublisher->publish([]);
    }
}
