<?php

declare(strict_types=1);

namespace User\Application\FindingById;

use Common\Messenger\Query;

class FindById implements Query
{
    private int $userId;

    private function __construct()
    {
    }

    public static function new(int $userId): self
    {
        $query = new self();
        $query->userId = $userId;
        return $query;
    }

    public function userId(): int
    {
        return $this->userId;
    }
}
