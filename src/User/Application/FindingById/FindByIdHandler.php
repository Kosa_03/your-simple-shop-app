<?php

declare(strict_types=1);

namespace User\Application\FindingById;

use Common\Messenger\QueryHandler;
use User\Domain\User;
use User\Domain\UserDto;
use User\Domain\UserStorageInterface;

class FindByIdHandler implements QueryHandler
{
    public function __construct(
        private UserStorageInterface $userStorage
    ) {
    }

    public function __invoke(FindById $query): ?UserDto
    {
        $user = $this->userStorage->findById($query->userId());
        if ($user instanceof User) {
            return UserDto::entity($user);
        }
        return null;
    }
}
