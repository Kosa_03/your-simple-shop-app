<?php

declare(strict_types=1);

namespace User\Application\FindingByLogin;

use Common\Messenger\Query;

class FindByLogin implements Query
{
    private string $login;

    private function __construct()
    {
    }

    public static function new(string $login): self
    {
        $query = new self();
        $query->login = $login;
        return $query;
    }

    public function login(): string
    {
        return $this->login;
    }
}
