<?php

declare(strict_types=1);

namespace User\Application\FindingByLogin;

use Common\Messenger\QueryHandler;
use User\Domain\User;
use User\Domain\UserDto;
use User\Domain\UserStorageInterface;

class FindByLoginHandler implements QueryHandler
{
    public function __construct(
        private UserStorageInterface $userStorage
    ) {
    }

    public function __invoke(FindByLogin $query): ?UserDto
    {
        $user = $this->userStorage->findByLogin($query->login());
        if ($user instanceof User) {
            return UserDto::entity($user);
        }
        return null;
    }
}
