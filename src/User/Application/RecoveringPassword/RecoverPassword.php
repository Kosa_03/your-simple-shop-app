<?php

declare(strict_types=1);

namespace User\Application\RecoveringPassword;

use Common\Messenger\Command;

class RecoverPassword implements Command
{
    private string $login;
    private string $email;

    private function __construct()
    {
    }

    public static function new(string $login, string $email): self
    {
        $recoverPassword = new self();
        $recoverPassword->login = $login;
        $recoverPassword->email = $email;
        return $recoverPassword;
    }

    public function login(): string
    {
        return $this->login;
    }

    public function email(): string
    {
        return $this->email;
    }
}
