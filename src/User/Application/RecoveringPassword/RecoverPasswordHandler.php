<?php

declare(strict_types=1);

namespace User\Application\RecoveringPassword;

use Common\AppResult;
use Common\Clock;
use Common\Messenger\CommandHandler;
use EventPublisher\Core\EventPublisherInterface;
use User\Domain\RecoveryTokenStorageInterface;
use User\Domain\User;
use User\Domain\UserStorageInterface;

class RecoverPasswordHandler implements CommandHandler
{
    public function __construct(
        private Clock $clock,
        private EventPublisherInterface $eventPublisher,
        private RecoveryTokenStorageInterface $recoveryTokenStorage,
        private UserStorageInterface $userStorage
    ) {
    }

    public function __invoke(RecoverPassword $command): AppResult
    {
        $user = $this->userStorage->findByLoginAndEmail($command->login(), $command->email());
        if (null === $user) {
            return AppResult::failure(User::REASON_USER_NOT_FOUND);
        }

        $result = $user->createRecoveryToken($this->clock->now('+24 hours'));
        if ($result->reason() === User::REASON_ACTIVE_TOKEN) {
            return AppResult::failure($result->reason());
        }
        if ($user->hasRecoveryToken()) {
            $this->recoveryTokenStorage->save($user->getRecoveryToken());
        }
        $this->eventPublisher->publish($result->events());

        return AppResult::success();
    }
}
