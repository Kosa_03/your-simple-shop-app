<?php

declare(strict_types=1);

namespace User\Application\UpdatingBasicInfo;

use Common\Messenger\Command;

class UpdateBasicInfo implements Command
{
    private int $userId;
    private string $firstName;
    private string $lastName;
    /** @var array<int, string> $roles */
    private array $roles;
    private bool $active;

    private function __construct()
    {
    }

    /**
     * @param array<int, string> $roles
     */
    public static function new(int $userId, string $firstName, string $lastName, array $roles, bool $active): self
    {
        $command = new self();
        $command->userId = $userId;
        $command->firstName = $firstName;
        $command->lastName = $lastName;
        $command->roles = $roles;
        $command->active = $active;
        return $command;
    }

    public function userId(): int
    {
        return $this->userId;
    }

    public function firstName(): string
    {
        return $this->firstName;
    }

    public function lastName(): string
    {
        return $this->lastName;
    }

    /**
     * @return array<int, string>
     */
    public function roles(): array
    {
        return $this->roles;
    }

    public function active(): bool
    {
        return $this->active;
    }
}
