<?php

declare(strict_types=1);

namespace User\Application\UpdatingBasicInfo;

use Common\Clock;
use Common\Messenger\CommandHandler;
use EventPublisher\Core\EventPublisherInterface;
use User\Domain\Roles;
use User\Domain\UserStorageInterface;

class UpdateBasicInfoHandler implements CommandHandler
{
    public function __construct(
        private Clock $clock,
        private EventPublisherInterface $eventPublisher,
        private UserStorageInterface $userStorage
    ) {
    }

    public function __invoke(UpdateBasicInfo $command): void
    {
        $user = $this->userStorage->findById($command->userId());
        if (null === $user) {
            return;
        }

        $result = $user->updateBasicInfo(
            $command->firstName(),
            $command->lastName(),
            Roles::create($command->roles()),
            $command->active(),
            $this->clock->now()
        );
        $this->userStorage->save($user);
        $this->eventPublisher->publish($result->events());
    }
}
