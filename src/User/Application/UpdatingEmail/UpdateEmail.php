<?php

declare(strict_types=1);

namespace User\Application\UpdatingEmail;

use Common\Messenger\Command;

class UpdateEmail implements Command
{
    private int $userId;
    private string $email;

    private function __construct()
    {
    }

    public static function new(int $userId, string $email): self
    {
        $command = new self();
        $command->userId = $userId;
        $command->email = $email;
        return $command;
    }

    public function userId(): int
    {
        return $this->userId;
    }

    public function email(): string
    {
        return $this->email;
    }
}
