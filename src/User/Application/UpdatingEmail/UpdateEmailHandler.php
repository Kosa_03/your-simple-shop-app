<?php

declare(strict_types=1);

namespace User\Application\UpdatingEmail;

use Common\Clock;
use Common\Messenger\CommandHandler;
use EventPublisher\Core\EventPublisherInterface;
use User\Domain\UserStorageInterface;

class UpdateEmailHandler implements CommandHandler
{
    public function __construct(
        private Clock $clock,
        private EventPublisherInterface $eventPublisher,
        private UserStorageInterface $userStorage
    ) {
    }

    public function __invoke(UpdateEmail $command): void
    {
        $user = $this->userStorage->findById($command->userId());
        if (null === $user) {
            return;
        }

        $result = $user->updateEmail($command->email(), $this->clock->now());
        $this->userStorage->save($user);
        $this->eventPublisher->publish($result->events());
    }
}
