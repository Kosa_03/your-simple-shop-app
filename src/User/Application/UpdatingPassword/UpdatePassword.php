<?php

declare(strict_types=1);

namespace User\Application\UpdatingPassword;

use Common\Messenger\Command;

class UpdatePassword implements Command
{
    private int $userId;
    private string $plainPassword;

    private function __construct()
    {
    }

    public static function new(int $userId, string $plainPassword): self
    {
        $command = new self();
        $command->userId = $userId;
        $command->plainPassword = $plainPassword;
        return $command;
    }

    public function userId(): int
    {
        return $this->userId;
    }

    public function plainPassword(): string
    {
        return $this->plainPassword;
    }
}
