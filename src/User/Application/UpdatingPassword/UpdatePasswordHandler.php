<?php

declare(strict_types=1);

namespace User\Application\UpdatingPassword;

use Common\Clock;
use Common\Messenger\CommandHandler;
use EventPublisher\Core\EventPublisherInterface;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use User\Domain\Password;
use User\Domain\UserStorageInterface;

class UpdatePasswordHandler implements CommandHandler
{
    public function __construct(
        private Clock $clock,
        private EventPublisherInterface $eventPublisher,
        private UserPasswordHasherInterface $passwordHasher,
        private UserStorageInterface $userStorage
    ) {
    }

    public function __invoke(UpdatePassword $command): void
    {
        $user = $this->userStorage->findById($command->userId());
        if (null === $user) {
            return;
        }

        $result = $user->updatePassword(
            Password::create($user, $this->passwordHasher, $command->plainPassword()),
            $this->clock->now()
        );
        $this->userStorage->save($user);
        $this->eventPublisher->publish($result->events());
    }
}
