<?php

declare(strict_types=1);

namespace User\Contract\Event;

use Common\Messenger\AsyncTransportInterface;
use EventPublisher\Core\IntegrationEvent\IntegrationEvent;
use EventPublisher\Shared\EventId;

class RecoveryTokenCreated implements IntegrationEvent, AsyncTransportInterface
{
    private EventId $eventId;
    private int $userId;
    private string $firstName;
    private string $email;
    private string $token;

    private function __construct()
    {
    }

    public static function new(EventId $eventId, int $userId, string $firstName, string $email, string $token): self
    {
        $event = new self();
        $event->eventId = $eventId;
        $event->userId = $userId;
        $event->firstName = $firstName;
        $event->email = $email;
        $event->token = $token;
        return $event;
    }

    public function eventId(): EventId
    {
        return $this->eventId;
    }

    public function getUserId(): int
    {
        return $this->userId;
    }

    public function getFirstName(): string
    {
        return $this->firstName;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function getToken(): string
    {
        return $this->token;
    }
}
