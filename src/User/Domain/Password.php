<?php

declare(strict_types=1);

namespace User\Domain;

use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Embeddable;
use InvalidArgumentException;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

#[Embeddable]
class Password
{
    #[Column(name: 'password', type: 'string', length: 128)]
    private string $password;

    private function __construct()
    {
    }

    public static function create(User $user, UserPasswordHasherInterface $hasher, string $plainPassword): self
    {
        if (strlen($plainPassword) < 10) {
            throw new InvalidArgumentException('Password is too short. It should has min 10 chars.');
        }

        $password = new self();
        $password->password = $hasher->hashPassword($user, $plainPassword);
        return $password;
    }

    public function hashedPassword(): string
    {
        return $this->password;
    }
}
