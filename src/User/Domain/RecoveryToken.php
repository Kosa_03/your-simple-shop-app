<?php

declare(strict_types=1);

namespace User\Domain;

use DateTimeImmutable;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\OneToOne;
use Doctrine\ORM\Mapping\Table;
use User\Infrastructure\Exception\RecoveryTokenExistsException;

#[Entity]
#[Table(name: 'user_recovery_token')]
class RecoveryToken
{
    #[Id]
    #[GeneratedValue]
    #[Column(name: 'id', type: 'integer')]
    private int $id;

    #[OneToOne(inversedBy: 'recoveryToken', targetEntity: User::class)]
    #[JoinColumn(name: 'user_id', referencedColumnName: 'id', nullable: false)]
    private User $user;

    #[Column(name: 'token', type: 'string', length: 15)]
    private string $token;

    #[Column(name: 'expiry_at', type: 'datetime_immutable')]
    private ?DateTimeImmutable $expiryAt;

    public static function create(User $user, string $token, DateTimeImmutable $expiryAt): self
    {
        $recoveryToken = new self();
        $recoveryToken->token = $token;
        $recoveryToken->user = $user;
        $recoveryToken->expiryAt = $expiryAt;
        $user->updateRecoveryToken($recoveryToken);

        return $recoveryToken;
    }

    public function update(string $token, DateTimeImmutable $expiryAt): void
    {
        if (!$this->isExpired()) {
            throw new RecoveryTokenExistsException('Recovery token exists in database.');
        }

        $this->token = $token;
        $this->expiryAt = $expiryAt;
    }

    public function isExpired(): bool
    {
        return $this->expiryAt < new DateTimeImmutable();
    }

    public function isCorrect(string $token): bool
    {
        return !$this->isExpired() && $this->token === $token;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getToken(): string
    {
        return $this->token;
    }

    public function getExpiryAt(): ?DateTimeImmutable
    {
        return $this->expiryAt;
    }
}
