<?php

declare(strict_types=1);

namespace User\Domain;

use DateTimeImmutable;

class RecoveryTokenDto
{
    public ?int $id;
    public ?string $token;
    public ?DateTimeImmutable $expiryAt;

    private function __construct()
    {
    }

    public static function entity(RecoveryToken $recoveryToken): self
    {
        $dto = new self();
        $dto->id = $recoveryToken->getId();
        $dto->token = $recoveryToken->getToken();
        $dto->expiryAt = $recoveryToken->getExpiryAt();
        return $dto;
    }
}
