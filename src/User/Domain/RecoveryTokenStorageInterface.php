<?php

declare(strict_types=1);

namespace User\Domain;

interface RecoveryTokenStorageInterface
{
    public function save(RecoveryToken $token): RecoveryToken;
    public function remove(RecoveryToken $token): void;
}
