<?php

declare(strict_types=1);

namespace User\Domain;

use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Embeddable;

#[Embeddable]
class Roles
{
    public const ROLE_ADMIN = 'ROLE_ADMIN';
    public const ROLE_OWNER = 'ROLE_OWNER';
    public const ROLE_USER = 'ROLE_USER';

    public const ALL = [
        self::ROLE_ADMIN,
        self::ROLE_OWNER,
        self::ROLE_USER
    ];

    /** @var array<int, string> $roles */
    #[Column(name: 'roles', type: 'json')]
    private array $roles;

    private function __construct()
    {
    }

    /**
     * @param array<int, string> $roles
     */
    public static function create(array $roles): self
    {
        $filteredNotIn = array_filter($roles, fn(string $role) => !in_array($role, self::ALL));
        if (count($filteredNotIn) > 0) {
            throw new \InvalidArgumentException(
                sprintf('Invalid user roles. "%s" are not correct.', implode(', ', $filteredNotIn))
            );
        }

        $userRoles = new self();
        $userRoles->roles = $roles;
        return $userRoles;
    }

    /**
     * @return array<int, string>
     */
    public function getRoles(): array
    {
        return $this->roles;
    }
}
