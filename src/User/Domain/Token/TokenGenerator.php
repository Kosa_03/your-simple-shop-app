<?php

declare(strict_types=1);

namespace User\Domain\Token;

class TokenGenerator
{
    public static function random(): string
    {
        return bin2hex(random_bytes(5));
    }
}
