<?php

declare(strict_types=1);

namespace User\Domain;

use DateTimeImmutable;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Embedded;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\OneToOne;
use Doctrine\ORM\Mapping\Table;
use EventPublisher\Shared\EventId;
use EventPublisher\Shared\Result;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use User\Contract\Event\RecoveryTokenCreated;
use User\Domain\Token\TokenGenerator;

#[Entity]
#[Table(name: 'user')]
class User implements UserInterface, PasswordAuthenticatedUserInterface
{
    public const REASON_USER_NOT_FOUND = 'reason-user-not-found';
    public const REASON_NON_EXISTING_TOKEN  = 'reason-non-existing-token';
    public const REASON_INCORRECT_TOKEN  = 'reason-incorrect-token';
    public const REASON_EXPIRED_TOKEN = 'reason-expired-token';
    public const REASON_ACTIVE_TOKEN = 'reason-active-token';
    public const REASON_PASSWORD_TOO_SHORT = 'reason-password-too-short';

    #[Id]
    #[GeneratedValue(strategy: 'AUTO')]
    #[Column(name: 'id', type: 'integer')]
    private int $id;

    #[Column(name: 'login', type: 'string', length: 50, unique: true)]
    private string $login;

    #[Column(name: 'email', type: 'string', length: 120, unique: true)]
    private string $email;

    #[Column(name: 'first_name', type: 'string', length: 80)]
    private string $firstName;

    #[Column(name: 'last_name', type: 'string', length: 100)]
    private string $lastName;

    #[Embedded(class: Password::class, columnPrefix: false)]
    private Password $password;

    #[Embedded(class: Roles::class, columnPrefix: false)]
    private Roles $roles;

    #[Column(name: 'active', type: 'boolean', options: ['default' => 0])]
    private bool $active;

    #[Column(name: 'created_at', type: 'datetime_immutable')]
    private DateTimeImmutable $createdAt;

    #[Column(name: 'updated_at', type: 'datetime_immutable')]
    private DateTimeImmutable $updatedAt;

    #[Column(name: 'last_login_1', type: 'datetime_immutable')]
    private DateTimeImmutable $lastLogin1;

    #[Column(name: 'last_login_2', type: 'datetime_immutable')]
    private DateTimeImmutable $lastLogin2;

    #[OneToOne(mappedBy: 'user', targetEntity: RecoveryToken::class, cascade: ['persist', 'remove'])]
    private ?RecoveryToken $recoveryToken = null;

    public static function create(
        string $login,
        string $email,
        string $firstName,
        string $lastName,
        Roles $roles,
        bool $active,
        UserPasswordHasherInterface $passwordHasher,
        string $plainPassword
    ): self {
        $user = new self();
        $user->login = $login;
        $user->email = $email;
        $user->firstName = $firstName;
        $user->lastName = $lastName;
        $user->password = Password::create($user, $passwordHasher, $plainPassword);
        $user->roles = $roles;
        $user->active = $active;
        $user->createdAt = new DateTimeImmutable();
        $user->updatedAt = new DateTimeImmutable();
        $user->lastLogin1 = new DateTimeImmutable();
        $user->lastLogin2 = new DateTimeImmutable();
        return $user;
    }

    public function updateBasicInfo(
        string $firstName,
        string $lastName,
        Roles $roles,
        bool $active,
        DateTimeImmutable $when
    ): Result {
        $this->firstName = $firstName;
        $this->lastName = $lastName;
        $this->roles = $roles;
        $this->active = $active;
        $this->updatedAt = $when;

        return Result::success([]);
    }

    public function updateEmail(string $email, DateTimeImmutable $when): Result
    {
        $this->email = $email;
        $this->updatedAt = $when;

        return Result::success([]);
    }

    public function updatePassword(Password $password, DateTimeImmutable $when): Result
    {
        $this->password = $password;
        $this->updatedAt = $when;

        return Result::success([]);
    }

    public function updateLastLogInTime(DateTimeImmutable $when): void
    {
        $this->lastLogin2 = $this->lastLogin1;
        $this->lastLogin1 = $when;
    }

    public function createRecoveryToken(DateTimeImmutable $expiryAt): Result
    {
        if ($this->hasRecoveryToken()) {
            if (!$this->isTokenExpired()) {
                return Result::failure(self::REASON_ACTIVE_TOKEN);
            }
            $this->recoveryToken->update(TokenGenerator::random(), $expiryAt);
        } else {
            $this->recoveryToken = RecoveryToken::create($this, TokenGenerator::random(), $expiryAt);
        }

        return Result::success([
            RecoveryTokenCreated::new(
                EventId::v4(),
                $this->id,
                $this->firstName,
                $this->email,
                $this->recoveryToken->getToken()
            )
        ]);
    }

    public function createNewPassword(Password $password, string $token, DateTimeImmutable $when): Result
    {
        if (!$this->hasRecoveryToken()) {
            return Result::failure(User::REASON_NON_EXISTING_TOKEN);
        }
        if ($this->isTokenExpired()) {
            return Result::failure(User::REASON_EXPIRED_TOKEN);
        }
        if (!($this->isTokenCorrect($token))) {
            return Result::failure(User::REASON_INCORRECT_TOKEN);
        }

        $this->password = $password;
        $this->updatedAt = $when;

        return Result::success([]);
    }

    public function isTokenCorrect(string $token): bool
    {
        if ($this->hasRecoveryToken()) {
            return $this->recoveryToken->isCorrect($token);
        }
        return false;
    }

    public function isTokenExpired(): bool
    {
        if ($this->hasRecoveryToken()) {
            return $this->recoveryToken->isExpired();
        }
        return true;
    }

    /**
     * @phpstan-assert-if-true RecoveryToken $this->recoveryToken
     * @phpstan-assert-if-true !null $this->getRecoveryToken()
     */
    public function hasRecoveryToken(): bool
    {
        return $this->recoveryToken instanceof RecoveryToken;
    }

    public function updateRecoveryToken(RecoveryToken $token): void
    {
        $this->recoveryToken = $token;
    }

    public function removeRecoveryToken(): void
    {
        $this->recoveryToken = null;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getUserIdentifier(): string
    {
        return $this->getLogin();
    }

    public function getUsername(): string
    {
        return $this->getLogin();
    }

    public function getLogin(): string
    {
        return $this->login;
    }

    public function setLogin(string $login): self
    {
        $this->login = $login;
        return $this;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function getFirstName(): string
    {
        return $this->firstName;
    }

    public function getLastName(): string
    {
        return $this->lastName;
    }

    public function getSalt(): ?string
    {
        return null;
    }

    public function getPassword(): string
    {
        return $this->password->hashedPassword();
    }

    public function eraseCredentials(): ?string
    {
        return null;
    }

    public function isActive(): bool
    {
        return $this->active;
    }

    public function getRoles(): array
    {
        return $this->roles->getRoles();
    }

    public function getCreatedAt(): DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function getUpdatedAt(): DateTimeImmutable
    {
        return $this->updatedAt;
    }

    public function getLastLogin1(): DateTimeImmutable
    {
        return $this->lastLogin1;
    }

    public function getLastLogin2(): DateTimeImmutable
    {
        return $this->lastLogin2;
    }

    public function getRecoveryToken(): ?RecoveryToken
    {
        return $this->recoveryToken;
    }
}
