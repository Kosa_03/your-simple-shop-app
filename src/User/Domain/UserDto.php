<?php

declare(strict_types=1);

namespace User\Domain;

use DateTimeImmutable;

class UserDto
{
    public ?int $id = null;
    public string $login;
    public string $email;
    public string $firstName;
    public string $lastName;
    public ?string $plainPassword = null;
    /** @var array<int, string> $roles */
    public array $roles = [];
    public bool $active = false;
    public ?RecoveryTokenDto $recoveryToken = null;
    public ?DateTimeImmutable $updatedAt = null;

    public static function empty(): self
    {
        return new self();
    }

    /**
     * @param array<int, string> $roles
     */
    public static function create(
        string $login,
        string $email,
        string $firstName,
        string $lastName,
        ?string $plainPassword,
        array $roles,
        bool $active
    ): self {
        $dto = new self();
        $dto->login = $login;
        $dto->email = $email;
        $dto->firstName = $firstName;
        $dto->lastName = $lastName;
        $dto->roles = $roles;
        $dto->active = $active;
        $dto->plainPassword = $plainPassword;
        return $dto;
    }

    public static function entity(User $user): self
    {
        $dto = new self();
        $dto->id = $user->getId();
        $dto->login = $user->getLogin();
        $dto->email = $user->getEmail();
        $dto->firstName = $user->getFirstName();
        $dto->lastName = $user->getLastName();
        $dto->roles = $user->getRoles();
        $dto->active = $user->isActive();
        $dto->updatedAt = $user->getUpdatedAt();
        if ($user->hasRecoveryToken()) {
            $dto->recoveryToken = RecoveryTokenDto::entity($user->getRecoveryToken());
        }
        return $dto;
    }
}
