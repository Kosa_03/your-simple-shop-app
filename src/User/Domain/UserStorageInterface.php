<?php

declare(strict_types=1);

namespace User\Domain;

interface UserStorageInterface
{
    public function findById(int $userId): ?User;
    public function findByLogin(string $login): ?User;
    public function findByLoginAndEmail(string $login, string $email): ?User;
    public function save(User $user): User;
    public function remove(User $user): void;
}
