<?php

declare(strict_types=1);

namespace User\Infrastructure\Exception;

use LogicException;

class RecoveryTokenExistsException extends LogicException
{
}
