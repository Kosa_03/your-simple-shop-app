<?php

declare(strict_types=1);

namespace User\Infrastructure;

use Doctrine\ORM\EntityManagerInterface;
use User\Domain\RecoveryToken;
use User\Domain\RecoveryTokenStorageInterface;

class RecoveryTokenRepository implements RecoveryTokenStorageInterface
{
    public function __construct(
        private EntityManagerInterface $entityManager
    ) {
    }

    public function save(RecoveryToken $token): RecoveryToken
    {
        $this->entityManager->persist($token);
        $this->entityManager->flush();
        return $token;
    }

    public function remove(RecoveryToken $token): void
    {
        $this->entityManager->remove($token);
        $this->entityManager->flush();
    }
}
