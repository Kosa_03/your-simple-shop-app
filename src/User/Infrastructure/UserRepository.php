<?php

declare(strict_types=1);

namespace User\Infrastructure;

use Common\Pagination;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Doctrine\Persistence\ObjectRepository;
use User\Domain\RecoveryToken;
use User\Domain\User;
use User\Domain\UserStorageInterface;

class UserRepository implements UserStorageInterface
{
    /**
     * @var EntityRepository<User>
     */
    private ObjectRepository $repository;

    public function __construct(
        private EntityManagerInterface $entityManager
    ) {
        $this->repository = $entityManager->getRepository(User::class);
    }

    /**
     * @return array<int, User>
     */
    public function findUsersByPagination(int $page, Pagination $pagination): array
    {
        $users = $this->repository->createQueryBuilder('u')
            ->orderBy('u.' . $pagination->sortColumn, $pagination->sortMethod)
            ->setFirstResult(($page - 1) * $pagination->itemsPerPage)
            ->setMaxResults($pagination->itemsPerPage)
            ->getQuery()
            ->getResult();

        $count = $this->repository->createQueryBuilder('u')
            ->select('COUNT(u.id) AS amount')
            ->getQuery()
            ->getResult();

        $pagination->itemsTotal = $count[0]['amount'];

        return $users;
    }

    public function find(int $id): ?User
    {
        return $this->repository->find($id);
    }

    public function findById(int $userId): ?User
    {
        return $this->repository->find($userId);
    }

    /**
     * @param array<string, mixed> $criteria
     */
    public function findOneBy(array $criteria): ?User
    {
        return $this->repository->findOneBy($criteria);
    }

    public function findByLogin(string $login): ?User
    {
        return $this->repository->findOneBy(['login' => $login]);
    }

    public function findByLoginAndEmail(string $login, string $email): ?User
    {
        return $this->repository->findOneBy(['login' => $login, 'email' => $email]);
    }

    public function save(User $user): User
    {
        $this->entityManager->persist($user);
        $this->entityManager->flush();
        return $user;
    }

    public function remove(User $user): void
    {
        $this->entityManager->remove($user);
        $this->entityManager->flush();
    }
}
