<?php

declare(strict_types=1);

namespace User\Presentation\CreatingNewPassword;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

class CreatePasswordType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('login', TextType::class, [
                'label' => 'Login',
                'required' => false,
                'empty_data' => '',
                'constraints' => [
                    new NotBlank([
                        'message' => 'Pole nie może być puste'
                    ])
                ]
            ])
            ->add('token', TextType::class, [
                'label' => 'Token',
                'required' => false,
                'empty_data' => '',
                'constraints' => [
                    new NotBlank([
                        'message' => 'Pole nie może być puste'
                    ])
                ]
            ])
            ->add('plainPassword', RepeatedType::class, [
                'type' => PasswordType::class,
                'required' => false,
                'invalid_message' => 'Pola "Hasło" i "Powtórz hasło" nie są takie same',
                'first_options'  => [
                    'label' => 'Hasło',
                    'empty_data' => ''
                ],
                'second_options' => [
                    'label' => 'Powtórz hasło',
                    'empty_data' => ''
                ],
                'constraints' => [
                    new NotBlank([
                        'message' => 'Pole nie może być puste'
                    ]),
                    new Length([
                        'min' => 10,
                        'minMessage' => 'Pole powinno zawierać co najmniej 10 znaków'
                    ])
                ]
            ])
            ->add('submit', SubmitType::class, [
                'label' => 'Utwórz hasło'
            ]);
    }
}
