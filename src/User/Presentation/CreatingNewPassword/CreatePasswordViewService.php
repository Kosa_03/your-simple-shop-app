<?php

declare(strict_types=1);

namespace User\Presentation\CreatingNewPassword;

use Common\AppResult;
use Common\Messenger\Command;
use Common\Messenger\CommandBusInterface;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Twig\Environment;
use User\Application\CreatingNewPassword\CreateNewPassword;
use User\Domain\User;

class CreatePasswordViewService
{
    public function __construct(
        private CommandBusInterface $commandBus,
        private Environment $twig,
        private FormFactoryInterface $formFactory
    ) {
    }

    public function createPassword(string $token, Request $request): Response
    {
        $formDto = FormDto::with($token);

        $form = $this->formFactory->create(CreatePasswordType::class, $formDto);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $flashBag = $this->create($formDto->login, $token, $formDto->plainPassword);
        } else {
            $flashBag = null;
        }

        return new Response($this->createView($form, $flashBag));
    }

    /**
     * @param array<string, string>|null $flashBag
     */
    private function createView(FormInterface $form, ?array $flashBag): string
    {
        return $this->twig->render('@user/create_password.html.twig', [
            'createPasswordForm' => $form->createView(),
            'flashBag' => $flashBag
        ]);
    }

    private function runCommand(Command $command): AppResult
    {
        return $this->commandBus->dispatch($command);
    }

    /**
     * @return array<string, string>
     */
    private function create(string $login, string $token, string $plainPassword): array
    {
        $result = $this->runCommand(CreateNewPassword::new($login, $token, $plainPassword));
        if ($result->reason() === User::REASON_USER_NOT_FOUND) {
            return $this->userNotFoundFlashMsg();
        }
        if (in_array($result->reason(), [User::REASON_INCORRECT_TOKEN, User::REASON_NON_EXISTING_TOKEN])) {
            return $this->incorrectTokenFlashMsg();
        }
        if ($result->reason() === User::REASON_EXPIRED_TOKEN) {
            return $this->tokenHasExpiredFlashMsg();
        }

        return $this->passwordHasBeenChangedFlashMsg();
    }

    /**
     * @return array<string, string>
     */
    private function userNotFoundFlashMsg(): array
    {
        return ['type' => 'danger', 'message' => 'Podany użytkownik nie istnieje'];
    }

    /**
     * @return array<string, string>
     */
    private function incorrectTokenFlashMsg(): array
    {
        return ['type' => 'danger', 'message' => 'Token jest niepoprawny'];
    }

    /**
     * @return array<string, string>
     */
    private function tokenHasExpiredFlashMsg(): array
    {
        return ['type' => 'danger', 'message' => 'Token wygasł'];
    }

    /**
     * @return array<string, string>
     */
    private function passwordHasBeenChangedFlashMsg(): array
    {
        return ['type' => 'success', 'message' => 'Hasło zostało zmienione!'];
    }
}
