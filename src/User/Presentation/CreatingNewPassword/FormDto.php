<?php

declare(strict_types=1);

namespace User\Presentation\CreatingNewPassword;

class FormDto
{
    public string $login;
    public string $token;
    public string $plainPassword;

    private function __construct()
    {
    }

    public static function create(): self
    {
        return new self();
    }

    public static function with(string $token): self
    {
        $dto = new self();
        $dto->token = $token;
        return $dto;
    }
}
