<?php

declare(strict_types=1);

namespace User\Presentation\CreatingUser;

use Common\Messenger\Command;
use Common\Messenger\CommandBusInterface;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Twig\Environment;
use User\Application\CreatingUser\CreateUser;

class CreateUserViewService
{
    private FlashBagInterface $flashBag;

    public function __construct(
        private CommandBusInterface $commandBus,
        private Environment $twig,
        private FormFactoryInterface $formFactory,
        private UrlGeneratorInterface $urlGenerator,
        RequestStack $requestStack
    ) {
        $this->flashBag = $requestStack->getSession()->getFlashBag();
    }

    public function create(Request $request): Response
    {
        $formDto = new FormDto();
        $form = $this->formFactory->create(CreateUserType::class, $formDto);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->runCommand(CreateUser::new($formDto->toUserDto()));
            $this->createdSuccessfullyFlashMsg($formDto->login);

            return $this->redirectTo('list_users');
        }

        return new Response($this->createView($form));
    }

    private function createView(FormInterface $form): string
    {
        return $this->twig->render('@user/add_user.html.twig', [
            'userForm' => $form->createView()
        ]);
    }

    private function runCommand(Command $command): void
    {
        $this->commandBus->dispatch($command);
    }

    private function redirectTo(string $routeName): RedirectResponse
    {
        return new RedirectResponse($this->urlGenerator->generate($routeName));
    }

    private function createdSuccessfullyFlashMsg(string $login): void
    {
        $this->flashBag->add(
            'success',
            sprintf('Użytkownik <strong>%s</strong> został poprawnie utworzony', $login)
        );
    }
}
