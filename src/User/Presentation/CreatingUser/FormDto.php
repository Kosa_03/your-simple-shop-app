<?php

declare(strict_types=1);

namespace User\Presentation\CreatingUser;

use User\Domain\UserDto;

class FormDto
{
    public string $login;
    public string $email;
    public string $plainPassword;
    public string $firstName;
    public string $lastName;
    /** @var array<int, string> $roles */
    public array $roles = [];
    public bool $active = false;

    public function __construct()
    {
    }

    public function toUserDto(): UserDto
    {
        return UserDto::create(
            $this->login,
            $this->email,
            $this->firstName,
            $this->lastName,
            $this->plainPassword,
            $this->roles,
            $this->active
        );
    }
}
