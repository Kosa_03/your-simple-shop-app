<?php

declare(strict_types=1);

namespace User\Presentation\DeletingUser;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;

class ConfirmType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('delete', SubmitType::class, [
                'label' => 'Usuń'
            ])
            ->add('goBack', SubmitType::class, [
                'label' => 'Wróć'
            ]);
    }
}
