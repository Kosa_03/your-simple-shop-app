<?php

declare(strict_types=1);

namespace User\Presentation\DeletingUser;

use Common\Messenger\Command;
use Common\Messenger\CommandBusInterface;
use Common\Messenger\QueryBusInterface;
use Symfony\Component\Form\ClickableInterface;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Twig\Environment;
use User\Application\DeletingUser\DeleteUser;
use User\Application\FindingById\FindById;
use User\Domain\User;
use User\Domain\UserDto;

class DeleteUserViewService
{
    private FlashBagInterface $flashBag;

    public function __construct(
        private CommandBusInterface $commandBus,
        private QueryBusInterface $queryBus,
        private Environment $twig,
        private FormFactoryInterface $formFactory,
        private UrlGeneratorInterface $urlGenerator,
        RequestStack $requestStack
    ) {
        $this->flashBag = $requestStack->getSession()->getFlashBag();
    }

    public function delete(int $userId, Request $request): Response
    {
        $userDto = $this->findByIdQuery($userId);
        if (null === $userDto) {
            $this->userNotFoundFlashMsg();
            return $this->redirectTo('list_users');
        }

        $form = $this->formFactory->create(ConfirmType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            if ($this->isButtonClicked($form, 'delete')) {
                $this->runCommand(DeleteUser::new($userId));
                $this->deletedSuccessfullyFlashMsg($userDto->login);
            }
            if ($this->isButtonClicked($form, 'goBack')) {
                $this->goBackFlashMsg($userDto->login);
            }
            return $this->redirectTo('list_users');
        }

        return new Response($this->renderView($userDto, $form));
    }

    private function renderView(UserDto $userDto, FormInterface $form): string
    {
        return $this->twig->render('@user/delete_user.html.twig', [
            'user' => $userDto,
            'form' => $form->createView()
        ]);
    }

    private function findByIdQuery(int $userId): ?UserDto
    {
        return $this->queryBus->run(FindById::new($userId));
    }

    private function runCommand(Command $command): void
    {
        $this->commandBus->dispatch($command);
    }

    private function isButtonClicked(FormInterface $form, string $fieldName): bool
    {
        $field = $form->get($fieldName);
        if ($field instanceof ClickableInterface) {
            return $field->isClicked();
        }
        return false;
    }

    private function redirectTo(string $routeName): RedirectResponse
    {
        return new RedirectResponse($this->urlGenerator->generate($routeName));
    }

    private function userNotFoundFlashMsg(): void
    {
        $this->flashBag->add('danger', 'Podany użytkownik nie istnieje');
    }

    private function deletedSuccessfullyFlashMsg(string $login): void
    {
        $this->flashBag->add('success', sprintf('Użytkownik <strong>%s</strong> został poprawnie usunięty', $login));
    }

    private function goBackFlashMsg(string $login): void
    {
        $this->flashBag->add('warning', sprintf('Użytkownik <strong>%s</strong> nie został usunięty', $login));
    }
}
