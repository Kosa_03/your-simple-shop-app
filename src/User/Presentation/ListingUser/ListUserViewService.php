<?php

declare(strict_types=1);

namespace User\Presentation\ListingUser;

use Common\Pagination;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Twig\Environment;
use User\Domain\User;
use User\Infrastructure\UserRepository;

class ListUserViewService
{
    private const PAGINATION_SESSION_NAME = 'list_users/pagination';

    private SessionInterface $session;

    public function __construct(
        private UserRepository $userRepository,
        private Environment $twig,
        private FormFactoryInterface $formFactory,
        RequestStack $requestStack
    ) {
        $this->session = $requestStack->getSession();
    }

    public function page(int $page, Request $request): Response
    {
        $pagination = $this->getPaginationFromSession();

        $form = $this->formFactory->create(FilterListType::class, $pagination);
        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            $this->saveSessionFor($pagination);
        }

        $userList = $this->userRepository->findUsersByPagination($page, $pagination);

        return new Response($this->renderView($userList, $form, $page, $pagination));
    }

    /**
     * @param array<int, User> $userList
     */
    private function renderView(array $userList, FormInterface $form, int $page, Pagination $pagination): string
    {
        return $this->twig->render('@user/list_user.html.twig', [
            'userList' => $userList,
            'filterForm' => $form->createView(),
            'currentPage' => $page,
            'lastPage' => $pagination->lastPage()
        ]);
    }

    private function getPaginationFromSession(): Pagination
    {
        $pagination = unserialize($this->session->get(self::PAGINATION_SESSION_NAME, ''));
        if (false === $pagination) {
            $pagination = Pagination::create('login', 'ASC', 10);
        }
        return $pagination;
    }

    private function saveSessionFor(Pagination $pagination): void
    {
        $this->session->set(self::PAGINATION_SESSION_NAME, serialize($pagination));
    }
}
