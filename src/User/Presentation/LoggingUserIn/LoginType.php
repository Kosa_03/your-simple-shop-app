<?php

declare(strict_types=1);

namespace User\Presentation\LoggingUserIn;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class LoginType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('login', TextType::class, [
                'label' => 'Login',
                'required' => false
            ])
            ->add('password', PasswordType::class, [
                'label' => 'Hasło',
                'required' => false
            ])
            ->add('loginFormSubmit', SubmitType::class, [
                'label' => 'Zaloguj'
            ]);
    }
}
