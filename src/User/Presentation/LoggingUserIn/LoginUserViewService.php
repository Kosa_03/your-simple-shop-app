<?php

declare(strict_types=1);

namespace User\Presentation\LoggingUserIn;

use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Exception\BadCredentialsException;
use Symfony\Component\Security\Core\Exception\InvalidCsrfTokenException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Twig\Environment;

class LoginUserViewService
{
    public function __construct(
        private Environment $twig,
        private FormFactoryInterface $formFactory,
        private UrlGeneratorInterface $urlGenerator
    ) {
    }

    public function login(?UserInterface $user, AuthenticationUtils $authenticationUtils): Response
    {
        if (null !== $user) {
            return $this->redirectTo('app_home');
        }

        $form = $this->formFactory->create(LoginType::class);

        return new Response($this->renderView($form, $authenticationUtils));
    }

    private function renderView(FormInterface $form, AuthenticationUtils $authenticationUtils): string
    {
        return $this->twig->render('@user/login.html.twig', [
            'loginForm' => $form->createView(),
            'authErrorMessage' => $this->loginErrorMessage($authenticationUtils)
        ]);
    }

    private function loginErrorMessage(AuthenticationUtils $utils): ?string
    {
        $exception = $utils->getLastAuthenticationError();
        if (null === $exception) {
            return null;
        }

        if ($exception instanceof BadCredentialsException) {
            return 'Niepoprawny login lub hasło';
        }
        if ($exception instanceof InvalidCsrfTokenException) {
            return 'Niepoprawny CSRF token';
        }

        return $exception->getMessage();
    }

    private function redirectTo(string $routeName): RedirectResponse
    {
        return new RedirectResponse($this->urlGenerator->generate($routeName));
    }
}
