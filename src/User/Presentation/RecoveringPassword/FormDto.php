<?php

declare(strict_types=1);

namespace User\Presentation\RecoveringPassword;

class FormDto
{
    public string $login;
    public string $email;

    private function __construct()
    {
    }

    public static function create(): self
    {
        return new self();
    }
}
