<?php

declare(strict_types=1);

namespace User\Presentation\RecoveringPassword;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class RecoverPasswordType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('login', TextType::class, [
                'label' => 'Login',
                'required' => false,
                'empty_data' => ''
            ])
            ->add('email', TextType::class, [
                'label' => 'E-mail',
                'required' => false,
                'empty_data' => ''
            ])
            ->add('submit', SubmitType::class, [
                'label' => 'Wyślij e-mail'
            ]);
    }
}
