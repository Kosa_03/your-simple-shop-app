<?php

declare(strict_types=1);

namespace User\Presentation\RecoveringPassword;

use Common\AppResult;
use Common\Messenger\Command;
use Common\Messenger\CommandBusInterface;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Twig\Environment;
use User\Application\RecoveringPassword\RecoverPassword;
use User\Domain\User;

class RecoverPasswordViewService
{
    public function __construct(
        private CommandBusInterface $commandBus,
        private Environment $twig,
        private FormFactoryInterface $formFactory
    ) {
    }

    public function recoverPassword(Request $request): Response
    {
        $formDto = FormDto::create();

        $form = $this->formFactory->create(RecoverPasswordType::class, $formDto);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $flashBag = $this->recover($formDto->login, $formDto->email);
        } else {
            $flashBag = null;
        }

        return new Response($this->renderView($form, $flashBag));
    }

    /**
     * @return array<string, string>
     */
    private function recover(string $login, string $email): array
    {
        $result = $this->runCommand(RecoverPassword::new($login, $email));
        if (in_array($result->reason(), [User::REASON_USER_NOT_FOUND, User::REASON_ACTIVE_TOKEN])) {
            return $this->incorrectUserDataFlashMessage();
        }

        return $this->tokenHasBeenCreatedFlashMsg();
    }

    /**
     * @param array<string, string>|null $flashBag
     */
    public function renderView(FormInterface $form, ?array $flashBag): string
    {
        return $this->twig->render('@user/recover_password.html.twig', [
            'recoverPasswordForm' => $form->createView(),
            'flashBag' => $flashBag
        ]);
    }

    private function runCommand(Command $command): AppResult
    {
        return $this->commandBus->dispatch($command);
    }

    /**
     * @return array<string, string>
     */
    private function tokenHasBeenCreatedFlashMsg(): array
    {
        return ['type' => 'success', 'message' => 'Wiadomość została wysłana'];
    }

    /**
     * @return array<string, string>
     */
    public function incorrectUserDataFlashMessage(): array
    {
        return ['type' => 'danger', 'message' => 'Podane dane są nieprawidłowe'];
    }
}
