<?php

declare(strict_types=1);

namespace User\Presentation\UpdatingUser;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Regex;
use User\Presentation\UserRoles;

class EditBasicInfoType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('firstName', TextType::class, [
                'label' => 'Imię',
                'required' => false,
                'empty_data' => '',
                'constraints' => [
                    new NotBlank([
                        'message' => 'Pole nie może być puste'
                    ]),
                    new Length([
                        'max' => 25,
                        'maxMessage' => 'Pole nie powinno zawierać więcej niż 25 znaków'
                    ]),
                    new Regex([
                        'pattern' => '/^[a-zA-ZąćęłńóśźżĄĆĘŁŃÓŹŻ]+$/i',
                        'message' => 'Pole powinno składać się ze znaków a-z, A-Z'
                    ])
                ]
            ])
            ->add('lastName', TextType::class, [
                'label' => 'Nazwisko',
                'required' => false,
                'empty_data' => '',
                'constraints' => [
                    new NotBlank([
                        'message' => 'Pole nie może być puste'
                    ]),
                    new Length([
                        'max' => 100,
                        'maxMessage' => 'Pole nie powinno zawierać więcej niż 100 znaków'
                    ]),
                    new Regex([
                        'pattern' => '/^[a-zA-ZąćęłńóśźżĄĆĘŁŃÓŹŻ]+$/i',
                        'message' => 'Pole powinno składać się ze znaków a-z, A-Z'
                    ])
                ]
            ])
            ->add('roles', ChoiceType::class, [
                'label' => 'Uprawnienia',
                'empty_data' => [],
                'choices' => UserRoles::roles(),
                'expanded' => true,
                'multiple' => true
            ])
            ->add('active', ChoiceType::class, [
                'label' => 'Aktywne',
                'empty_data' => 0,
                'choices' => [
                    'Tak' => 1,
                    'Nie' => 0,
                ],
            ])
            ->add('submit', SubmitType::class, [
                'label' => 'Zapisz'
            ]);
    }
}
