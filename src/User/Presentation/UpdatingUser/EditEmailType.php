<?php

declare(strict_types=1);

namespace User\Presentation\UpdatingUser;

use Common\Validator\UniqueEntity;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\Email;
use User\Domain\User;

class EditEmailType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('email', RepeatedType::class, [
                'type' => TextType::class,
                'required' => false,
                'invalid_message' => 'Pola "E-mail" i "Powtórz e-mail" nie są takie same.',
                'first_options'  => [
                    'label' => 'E-mail',
                    'empty_data' => ''
                ],
                'second_options' => [
                    'label' => 'Powtórz e-mail',
                    'empty_data' => ''
                ],
                'constraints' => [
                    new Email([
                        'message' => 'Podany e-mail nie jest prawidłowy'
                    ]),
                    new UniqueEntity([
                        'entityClass' => User::class,
                        'entityField' => 'email',
                        'message' => 'Podany email istnieje w bazie danych',
                    ])
                ]
            ])
            ->add('submit', SubmitType::class, [
                'label' => 'Zmień e-mail'
            ]);
    }
}
