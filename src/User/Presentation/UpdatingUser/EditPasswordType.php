<?php

declare(strict_types=1);

namespace User\Presentation\UpdatingUser;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

class EditPasswordType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('plainPassword', RepeatedType::class, [
                'type' => PasswordType::class,
                'required' => false,
                'invalid_message' => 'Pola "Hasło" i "Powtórz hasło" nie są takie same',
                'first_options'  => [
                    'label' => 'Hasło',
                    'empty_data' => ''
                ],
                'second_options' => [
                    'label' => 'Powtórz hasło',
                    'empty_data' => ''
                ],
                'constraints' => [
                    new NotBlank([
                        'message' => 'Pole nie może być puste'
                    ]),
                    new Length([
                        'min' => 10,
                        'minMessage' => 'Pole powinno zawierać co najmniej 10 znaków'
                    ])
                ]
            ])
            ->add('submit', SubmitType::class, [
                'label' => 'Zmień hasło'
            ]);
    }
}
