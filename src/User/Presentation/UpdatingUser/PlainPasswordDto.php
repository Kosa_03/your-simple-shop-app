<?php

declare(strict_types=1);

namespace User\Presentation\UpdatingUser;

class PlainPasswordDto
{
    public string $plainPassword;

    private function __construct()
    {
    }

    public static function create(): self
    {
        return new self();
    }
}
