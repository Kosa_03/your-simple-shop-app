<?php

declare(strict_types=1);

namespace User\Presentation\UpdatingUser;

use Common\Messenger\Command;
use Common\Messenger\CommandBusInterface;
use Common\Messenger\QueryBusInterface;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Twig\Environment;
use User\Application\FindingById\FindById;
use User\Application\UpdatingBasicInfo\UpdateBasicInfo;
use User\Application\UpdatingEmail\UpdateEmail;
use User\Application\UpdatingPassword\UpdatePassword;
use User\Domain\UserDto;

class UpdateUserViewService
{
    private FlashBagInterface $flashBag;

    public function __construct(
        private CommandBusInterface $commandBus,
        private QueryBusInterface $queryBus,
        private Environment $twig,
        private FormFactoryInterface $formFactory,
        private UrlGeneratorInterface $urlGenerator,
        RequestStack $requestStack
    ) {
        $this->flashBag = $requestStack->getSession()->getFlashBag();
    }

    public function update(int $userId, Request $request): Response
    {
        $userDto = $this->findByIdQuery($userId);
        if (null === $userDto) {
            $this->userNotFoundFlashMsg();
            return $this->redirectTo('list_users');
        }

        $basicInfoForm = $this->formFactory->create(EditBasicInfoType::class, $userDto);
        $basicInfoForm->handleRequest($request);
        if ($basicInfoForm->isSubmitted() && $basicInfoForm->isValid()) {
            $this->runCommand(UpdateBasicInfo::new(
                $userId,
                $userDto->firstName,
                $userDto->lastName,
                $userDto->roles,
                $userDto->active
            ));
            $this->userSavedSuccessfullyFlashMsg($userDto->login);

            return $this->redirectTo('edit_user', ['userId' => $userId]);
        }

        $emailForm = $this->formFactory->create(EditEmailType::class, $userDto);
        $emailForm->handleRequest($request);
        if ($emailForm->isSubmitted() && $emailForm->isValid()) {
            $this->runCommand(UpdateEmail::new($userId, $userDto->email));
            $this->userSavedSuccessfullyFlashMsg($userDto->login);

            return $this->redirectTo('edit_user', ['userId' => $userId]);
        }

        $passwordDto = PlainPasswordDto::create();
        $passwordForm = $this->formFactory->create(EditPasswordType::class, $passwordDto);
        $passwordForm->handleRequest($request);
        if ($passwordForm->isSubmitted() && $passwordForm->isValid()) {
            $this->runCommand(UpdatePassword::new($userId, $passwordDto->plainPassword));
            $this->userSavedSuccessfullyFlashMsg($userDto->login);

            return $this->redirectTo('edit_user', ['userId' => $userId]);
        }

        return new Response($this->renderView($basicInfoForm, $emailForm, $passwordForm));
    }

    private function renderView(
        FormInterface $basicInfoForm,
        FormInterface $emailForm,
        FormInterface $passwordForm
    ): string {
        return $this->twig->render('@user/update_user.html.twig', [
            'editUserBasicForm' => $basicInfoForm->createView(),
            'editEmailForm' => $emailForm->createView(),
            'editPasswordForm' => $passwordForm->createView()
        ]);
    }

    private function findByIdQuery(int $userId): ?UserDto
    {
        return $this->queryBus->run(FindById::new($userId));
    }

    private function runCommand(Command $command): void
    {
        $this->commandBus->dispatch($command);
    }

    /**
     * @param array<string, int|string> $params
     */
    private function redirectTo(string $routeName, array $params = []): RedirectResponse
    {
        return new RedirectResponse($this->urlGenerator->generate($routeName, $params));
    }

    private function userNotFoundFlashMsg(): void
    {
        $this->flashBag->add('danger', 'Podany użytkownik nie istnieje');
    }

    private function userSavedSuccessfullyFlashMsg(?string $login): void
    {
        $this->flashBag->add('success', sprintf('Użytkownik <strong>%s</strong> został poprawnie zapisany', $login));
    }
}
