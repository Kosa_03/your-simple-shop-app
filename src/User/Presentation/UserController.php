<?php

declare(strict_types=1);

namespace User\Presentation;

use LogicException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use User\Presentation\CreatingUser\CreateUserViewService;
use User\Presentation\CreatingNewPassword\CreatePasswordViewService;
use User\Presentation\DeletingUser\DeleteUserViewService;
use User\Presentation\UpdatingUser\UpdateUserViewService;
use User\Presentation\ListingUser\ListUserViewService;
use User\Presentation\LoggingUserIn\LoginUserViewService;
use User\Presentation\RecoveringPassword\RecoverPasswordViewService;

class UserController extends AbstractController
{
    public function __construct(
        private CreateUserViewService $createUserViewService,
        private CreatePasswordViewService $createPasswordViewService,
        private DeleteUserViewService $deleteUserViewService,
        private UpdateUserViewService $updateUserViewService,
        private ListUserViewService $listUserViewService,
        private LoginUserViewService $loginUserViewService,
        private RecoverPasswordViewService $recoverPasswordViewService
    ) {
    }

    #[Route('/login', name: 'app_login')]
    public function login(AuthenticationUtils $authenticationUtils): Response
    {
        return $this->loginUserViewService->login($this->getUser(), $authenticationUtils);
    }

    #[Route('/logout', name: 'app_logout')]
    public function logout(): Response
    {
        throw new LogicException(
            'This method can be blank - it will be intercepted by the logout key on your firewall.'
        );
    }

    #[Route(
        '/user/list/{page}',
        name: 'list_users',
        requirements: ['page' => '\d+'],
        defaults: ['page' => 1]
    )]
    public function listUser(int $page, Request $request): Response
    {
        return $this->listUserViewService->page($page, $request);
    }

    #[Route('/user/add', name: 'add_user')]
    public function addUser(Request $request): Response
    {
        return $this->createUserViewService->create($request);
    }

    #[Route('/user/{userId}/edit', name: 'edit_user', requirements: ['userId' => '\d+'])]
    public function editUser(int $userId, Request $request): Response
    {
        return $this->updateUserViewService->update($userId, $request);
    }

    #[Route('/user/{userId}/delete', name: 'delete_user', requirements: ['userId' => '\d+'])]
    public function deleteUser(int $userId, Request $request): Response
    {
        return $this->deleteUserViewService->delete($userId, $request);
    }

    #[Route('/create_password/{token}', name: 'app_create_password', requirements: ['token' => '^[a-z0-9]{5,20}$'])]
    public function createPassword(string $token, Request $request): Response
    {
        return $this->createPasswordViewService->createPassword($token, $request);
    }

    #[Route('/recover_password', name: 'app_recover_password')]
    public function recoverPassword(Request $request): Response
    {
        return $this->recoverPasswordViewService->recoverPassword($request);
    }
}
