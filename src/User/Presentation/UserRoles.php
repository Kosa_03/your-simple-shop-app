<?php

declare(strict_types=1);

namespace User\Presentation;

class UserRoles
{
    /**
     * @return array<string, string>
     */
    public static function roles(): array
    {
        return [
            'Administrator' => 'ROLE_ADMIN',
            'Właściciel' => 'ROLE_COMPANY_OWNER',
            'Użytkownik' => 'ROLE_USER'
        ];
    }
}
