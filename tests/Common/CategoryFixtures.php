<?php

declare(strict_types=1);

namespace Test\Common;

use CategoryOlx\Domain\Attribute;
use CategoryOlx\Domain\AttributeStorageInterface;
use CategoryOlx\Domain\Category;
use CategoryOlx\Domain\CategoryStorageInterface;

class CategoryFixtures
{
    public function __construct(
        private CategoryStorageInterface $categoryStorage,
        private AttributeStorageInterface $attributeStorage
    ) {
    }

    public function aCategory(int $categoryId, string $name, int $parentId, int $photosLimit, bool $isLeaf): Category
    {
        $category = Category::create(
            $categoryId,
            $name,
            $parentId,
            $photosLimit,
            $isLeaf
        );
        $this->categoryStorage->persist($category);
        $this->categoryStorage->flush();
        return $category;
    }

    public function anAttribute(
        int $categoryId,
        string $code,
        string $label,
        ?string $unit,
        string $type,
        bool $required,
        bool $numeric,
        ?int $min,
        ?int $max,
        bool $multiple,
        array $values
    ): Attribute {
        $attribute = Attribute::create(
            $categoryId,
            $code,
            $label,
            $unit,
            $type,
            $required,
            $numeric,
            $min,
            $max,
            $multiple,
            $values
        );
        $this->attributeStorage->persist($attribute);
        $this->attributeStorage->flush();
        return $attribute;
    }
}
