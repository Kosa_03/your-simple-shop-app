<?php

declare(strict_types=1);

namespace Test\Common\Event;

use Common\Messenger\AsyncTransportInterface;
use EventPublisher\Shared\EventId;

class AsyncEvent implements AsyncTransportInterface
{
    private EventId $eventId;

    private function __construct()
    {
    }

    public static function new(EventId $eventId): self
    {
        $event = new self();
        $event->eventId = $eventId;
        return $event;
    }

    public function eventId(): EventId
    {
        return $this->eventId;
    }
}
