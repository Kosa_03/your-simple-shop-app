<?php

declare(strict_types=1);

namespace Test\Common\Event;

use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class AsyncEventHandler implements MessageHandlerInterface
{
    public function __invoke(AsyncEvent $event): void
    {
        // TODO: Implement __invoke() method.
    }
}
