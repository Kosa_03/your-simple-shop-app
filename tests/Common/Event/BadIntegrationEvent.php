<?php

declare(strict_types=1);

namespace Test\Common\Event;

use EventPublisher\Core\IntegrationEvent\IntegrationEvent;
use EventPublisher\Shared\EventId;

class BadIntegrationEvent implements IntegrationEvent
{
    private EventId $eventId;

    private function __construct()
    {
    }

    public static function new(EventId $eventId): self
    {
        $event = new self();
        $event->eventId = $eventId;
        return $event;
    }

    public function eventId(): EventId
    {
        return $this->eventId;
    }

    public function __unserialize(array $data): void
    {
        throw new \Exception('Bad integration event');
    }
}
