<?php

declare(strict_types=1);

namespace Test\Common\Event;

use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class FirstIntegrationEventHandler implements MessageHandlerInterface
{
    public function __invoke(FirstIntegrationEvent $event): void
    {
        // TODO: Implement __invoke() method.
    }
}
