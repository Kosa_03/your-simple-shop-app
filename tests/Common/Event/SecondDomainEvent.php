<?php

declare(strict_types=1);

namespace Test\Common\Event;

use Common\Messenger\AsyncTransportInterface;
use EventPublisher\Core\DomainEvent\DomainEvent;

class SecondDomainEvent implements DomainEvent, AsyncTransportInterface
{
    private function __construct()
    {
    }

    public static function new(): self
    {
        return new self();
    }
}
