<?php

declare(strict_types=1);

namespace Test\Common\Event;

use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class SecondIntegrationEventHandler implements MessageHandlerInterface
{
    public function __invoke(SecondIntegrationEvent $event): void
    {
        // TODO: Implement __invoke() method.
    }
}
