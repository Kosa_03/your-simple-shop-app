<?php

declare(strict_types=1);

namespace Test\Common;

use Common\Clock;
use DateTimeImmutable;

class FixedClock implements Clock
{
    private ?DateTimeImmutable $dateTime = null;

    public function now(string $string = 'now'): DateTimeImmutable
    {
        if (null === $this->dateTime) {
            return new DateTimeImmutable($string);
        }
        if ('now' !== $string) {
            return $this->dateTime->modify($string);
        }
        return $this->dateTime;
    }

    public function setNow(string $dateTime): DateTimeImmutable
    {
        $this->dateTime = new DateTimeImmutable($dateTime);
        return $this->dateTime;
    }
}
