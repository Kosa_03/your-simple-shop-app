<?php

declare(strict_types=1);

namespace Test\Common;

use AccountOlx\Domain\AccessToken;
use AccountOlx\Domain\Account;
use AccountOlx\Domain\AccountDto;
use AccountOlx\Domain\AccountStorageInterface;
use AccountOlx\Domain\RefreshToken;
use AccountOlx\Domain\StateToken;
use Common\Clock;
use DateTimeImmutable;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use User\Domain\RecoveryToken;
use User\Domain\RecoveryTokenStorageInterface;
use User\Domain\Roles;
use User\Domain\User;
use User\Domain\UserStorageInterface;

class Fixtures
{
    public function __construct(
        private Clock $clock,
        private AccountStorageInterface $accountStorage,
        private RecoveryTokenStorageInterface $recoveryTokenStorage,
        private UserStorageInterface $userStorage,
        private UserPasswordHasherInterface $passwordHasher
    ) {
    }

    public function anActiveUser(string $login, string $email, string $firstName, string $lastName): User
    {
        $user = User::create(
            $login,
            $email,
            $firstName,
            $lastName,
            Roles::create(['ROLE_USER']),
            true,
            $this->passwordHasher,
            'password123'
        );
        return $this->userStorage->save($user);
    }

    public function aTestUser(): User
    {
        $user = User::create(
            'test',
            'test@abc.dd',
            'test',
            'test',
            Roles::create(['ROLE_ADMIN']),
            true,
            $this->passwordHasher,
            'password123'
        );
        return $this->userStorage->save($user);
    }

    public function aRecoveryToken(User $user, string $token, DateTimeImmutable $expiryAt): RecoveryToken
    {
        $recoveryToken = RecoveryToken::create($user, $token, $expiryAt);
        return $this->recoveryTokenStorage->save($recoveryToken);
    }

    public function anAccount(string $name, int $clientId, string $clientSecret, string $redirectUrl): Account
    {
        $account = Account::create(
            $name,
            $clientId,
            $clientSecret,
            RefreshToken::create('refresh_token'),
            AccessToken::create('access_token', $this->clock->now('+24 hours')),
            $redirectUrl,
            $this->clock->now()
        );
        return $this->accountStorage->save($account);
    }

    public function anAccountWithAccessToken(
        string $name,
        int $clientId,
        string $clientSecret,
        string $redirectUrl,
        string $accessToken,
        DateTimeImmutable $accessTokenExpiryAt
    ): Account {
        $account = $this->anAccount($name, $clientId, $clientSecret, $redirectUrl);
        $account->updateAccessToken(AccessToken::create($accessToken, $accessTokenExpiryAt));
        return $this->accountStorage->save($account);
    }

    public function anAccountWithStateToken(
        string $name,
        int $clientId,
        string $clientSecret,
        string $redirectUrl,
        StateToken $stateToken
    ): Account {
        $account = $this->anAccount($name, $clientId, $clientSecret, $redirectUrl);
        $account->createStateToken($stateToken);
        return $this->accountStorage->save($account);
    }

    public function anAccountDto(string $name, int $clientId, string $clientSecret, string $redirectUrl): AccountDto
    {
        return AccountDto::entity($this->anAccount($name, $clientId, $clientSecret, $redirectUrl));
    }
}
