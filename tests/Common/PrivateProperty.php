<?php

declare(strict_types=1);

namespace Test\Common;

use ReflectionClass;
use User\Domain\Password;
use User\Domain\User;

class PrivateProperty
{
    public static function setPassword(Password $password, User $object): void
    {
        $reflection = new ReflectionClass($object);
        $property = $reflection->getProperty('password');
        $property->setAccessible(true);
        $property->setValue($object, $password);
    }
}
