<?php

declare(strict_types=1);

namespace Test\Functional\AccountOlx;

use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Test\Common\Fixtures;

class ConnectingAccountToOlxTest extends WebTestCase
{
    private Fixtures $fixtures;
    private KernelBrowser $client;
    private UrlGeneratorInterface $urlGenerator;

    protected function setUp(): void
    {
        $this->client = self::createClient();

        $this->fixtures = $this->getContainer()->get(Fixtures::class);
        $this->urlGenerator = $this->getContainer()->get(UrlGeneratorInterface::class);
    }

    /**
     * @test
     */
    public function canRedirectToOlxWebService(): void
    {
        // given
        $account = $this->fixtures->anAccount('acc', 123, 'client_secret', 'redirect_url');

        // when
        $this->client->request('GET', $this->testedPath($account->getId()));

        // then
        $response = $this->client->getResponse();
        self::assertTrue($response->isRedirection());
        self::assertStringStartsWith('www.olx.', $this->parseHost($response));
    }

    /**
     * @test
     */
    public function canRedirectToOlxWithSpecificQueryParameters(): void
    {
        // given
        $account = $this->fixtures->anAccount('acc', 123, 'client_secret', 'redirect_url');

        // when
        $this->client->request('GET', $this->testedPath($account->getId()));

        // then
        $query = $this->parseQuery($this->client->getResponse());
        self::assertEquals($account->getClientId(), $query['client_id']);
        self::assertEquals('code', $query['response_type']);
        self::assertEquals('v2 read write', $query['scope']);
        self::assertEquals($account->getRedirectUrl(), $query['redirect_url']);
        self::assertEquals($account->getStateToken(), $query['state']);
    }

    /**
     * @test
     */
    public function shouldRedirectToAccountListPageWhenAnAccountWasNotFound(): void
    {
        // when
        $this->client->request('GET', $this->testedPath(666));

        // then
        $response = $this->client->getResponse();
        self::assertTrue($response->isRedirection());
        self::assertTrue($response->isRedirect($this->path('rest_api_olx_account_list')));
    }

    private function testedPath(int $accountId): string
    {
        return $this->urlGenerator->generate('rest_api_olx_auth_connect', ['accountId' => $accountId]);
    }

    private function path(string $name): string
    {
        return $this->urlGenerator->generate($name);
    }

    private function parseHost(Response $response): string
    {
        return parse_url($response->getTargetUrl(), PHP_URL_HOST);
    }

    private function parseQuery(Response $response): array
    {
        parse_str(parse_url($response->getTargetUrl(), PHP_URL_QUERY), $query);
        return $query;
    }
}
