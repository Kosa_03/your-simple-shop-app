<?php

declare(strict_types=1);

namespace Test\Functional\AccountOlx;

use AccountOlx\Domain\Account;
use AccountOlx\Domain\AccountStorageInterface;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Test\Common\Fixtures;

class CreatingAccountTest extends WebTestCase
{
    private AccountStorageInterface $accountStorage;
    private Fixtures $fixtures;
    private KernelBrowser $client;
    private UrlGeneratorInterface $urlGenerator;

    protected function setUp(): void
    {
        $this->client = self::createClient();

        $this->accountStorage = $this->getContainer()->get(AccountStorageInterface::class);
        $this->fixtures = $this->getContainer()->get(Fixtures::class);
        $this->urlGenerator = $this->getContainer()->get(UrlGeneratorInterface::class);
    }

    /**
     * @test
     */
    public function canCreateAnAccount(): void
    {
        // given
        $this->loginUser();
        // and
        $this->client->request('GET', $this->testedPath());

        // when
        $this->client->submitForm('add_account[submit]', [
            'add_account[name]' => 'account',
            'add_account[clientId]' => 666,
            'add_account[clientSecret]' => 'secret',
            'add_account[redirectUrl]' => 'url',
            'add_account[refreshToken]' => 'refresh_token123',
            'add_account[accessToken]' => 'access_token321',
            'add_account[accessTokenExpiryAt]' => '2023-01-15T14:23'
        ]);

        // then
        $account = $this->accountStorage->findOneBy(['clientId' => 666]);
        self::assertInstanceOf(Account::class, $account);
        $response = $this->client->getResponse();
        self::assertTrue($response->isRedirection());
        self::assertTrue($response->isRedirect($this->path('rest_api_olx_account_list')));
    }

    private function loginUser(): void
    {
        $this->client->loginUser($this->fixtures->aTestUser());
    }

    private function testedPath(): string
    {
        return $this->urlGenerator->generate('rest_api_olx_account_add');
    }

    private function path(string $name): string
    {
        return $this->urlGenerator->generate($name);
    }
}
