<?php

declare(strict_types=1);

namespace Test\Functional\AccountOlx;

use AccountOlx\Application\FindingById\FindById;
use AccountOlx\Domain\AccountDto;
use Common\Messenger\QueryBusInterface;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Test\Common\Fixtures;

class DeletingAccountTest extends WebTestCase
{
    private QueryBusInterface $queryBus;
    private Fixtures $fixtures;
    private KernelBrowser $client;
    private UrlGeneratorInterface $urlGenerator;

    protected function setUp(): void
    {
        $this->client = self::createClient();

        $this->queryBus = $this->getContainer()->get(QueryBusInterface::class);
        $this->fixtures = $this->getContainer()->get(Fixtures::class);
        $this->urlGenerator = $this->getContainer()->get(UrlGeneratorInterface::class);
    }

    /**
     * @test
     */
    public function canDeleteAccountAfterClickOnDeleteButton(): void
    {
        // given
        $this->loginUser();
        // and
        $account = $this->fixtures->anAccount('acc', 666, 'secret', 'url');
        // and
        $accountId = $account->getId();
        // and
        $this->client->request('GET', $this->testedPath($account->getId()));

        // when
        $this->client->submitForm('confirm[delete]');

        // then
        $accountDto = $this->findByIdQuery($accountId);
        self::assertNull($accountDto);
        $response = $this->client->getResponse();
        self::assertTrue($response->isRedirection());
        self::assertTrue($response->isRedirect($this->path('rest_api_olx_account_list')));
    }

    /**
     * @test
     */
    public function cannotDeleteAccountAfterClickOnGoBackButton(): void
    {
        // given
        $this->loginUser();
        // and
        $account = $this->fixtures->anAccount('acc', 666, 'secret', 'url');
        // and
        $accountId = $account->getId();
        // and
        $this->client->request('GET', $this->testedPath($account->getId()));

        // when
        $this->client->submitForm('confirm[goBack]');

        // then
        $accountDto = $this->findByIdQuery($accountId);
        self::assertInstanceOf(AccountDto::class, $accountDto);
        $response = $this->client->getResponse();
        self::assertTrue($response->isRedirection());
        self::assertTrue($response->isRedirect($this->path('rest_api_olx_account_list')));
    }

    /**
     * @test
     */
    public function shouldRedirectToAccountListPageWhenAnAccountWasNotFound(): void
    {
        // given
        $this->loginUser();

        // when
        $this->client->request('GET', $this->testedPath(666));

        // then
        $response = $this->client->getResponse();
        self::assertTrue($response->isRedirection());
        self::assertTrue($response->isRedirect($this->path('rest_api_olx_account_list')));
    }

    private function findByIdQuery(int $accountId): ?AccountDto
    {
        return $this->queryBus->run(FindById::new($accountId));
    }

    private function loginUser(): void
    {
        $this->client->loginUser($this->fixtures->aTestUser());
    }

    private function testedPath(int $accountId): string
    {
        return $this->urlGenerator->generate('rest_api_olx_account_delete', ['accountId' => $accountId]);
    }

    private function path(string $name): string
    {
        return $this->urlGenerator->generate($name);
    }
}
