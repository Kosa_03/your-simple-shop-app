<?php

declare(strict_types=1);

namespace Test\Functional\AccountOlx;

use AccountOlx\Application\FindingById\FindById;
use AccountOlx\Domain\AccountDto;
use Common\Clock;
use Common\Messenger\QueryBusInterface;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpClient\MockHttpClient;
use Symfony\Component\HttpClient\Response\MockResponse;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Test\Common\FixedClock;
use Test\Common\Fixtures;

class RefreshingTokensTest extends WebTestCase
{
    private KernelBrowser $client;
    private MockHttpClient $httpClient;
    private Fixtures $fixtures;
    private UrlGeneratorInterface $urlGenerator;
    private FixedClock $clock;
    private QueryBusInterface $queryBus;

    protected function setUp(): void
    {
        $this->client = self::createClient();

        $this->clock = $this->getContainer()->get(Clock::class);
        $this->httpClient = $this->getContainer()->get(HttpClientInterface::class);
        $this->fixtures = $this->getContainer()->get(Fixtures::class);
        $this->urlGenerator = $this->getContainer()->get(UrlGeneratorInterface::class);
        $this->queryBus = $this->getContainer()->get(QueryBusInterface::class);
    }

    /**
     * @test
     */
    public function canSaveRefreshedTokensForAnAccount(): void
    {
        // given
        $account = $this->fixtures->anAccount('acc', 123, 'secret', 'url');
        // and
        $this->configureAuthResponse();
        // and
        $now = $this->clock->setNow('2021-12-06 11:34:59');

        // when
        $this->client->request('GET', $this->testedPath($account->getId()));

        // then
        $dto = $this->findByIdQuery($account->getId());
        self::assertEquals('new_token_access', $dto->accessToken);
        self::assertEquals($now->modify('+86400 seconds'), $dto->accessTokenExpiryAt);
        self::assertEquals('new_token_refresh', $dto->refreshToken);
        $response = $this->client->getResponse();
        self::assertTrue($response->isRedirection());
        self::assertTrue($response->isRedirect($this->path('rest_api_olx_account_list')));
    }

    /**
     * @test
     */
    public function shouldRedirectToAccountListWhenAnAccountWasNotFound(): void
    {
        // when
        $this->client->request('GET', $this->testedPath(666));

        // then
        $response = $this->client->getResponse();
        self::assertTrue($response->isRedirection());
        self::assertTrue($response->isRedirect($this->path('rest_api_olx_account_list')));
    }

    private function findByIdQuery(int $accountId): ?AccountDto
    {
        return $this->queryBus->run(FindById::new($accountId));
    }

    private function testedPath(int $accountId): string
    {
        return $this->urlGenerator->generate('rest_api_olx_auth_refresh_tokens', ['accountId' => $accountId]);
    }

    private function path(string $routeName): string
    {
        return $this->urlGenerator->generate($routeName);
    }

    private function configureAuthResponse(): void
    {
        $content = [
            'access_token' => 'new_token_access',
            'expires_in' => 86400,
            'token_type' => 'bearer',
            'scope' => 'v2 read write',
            'refresh_token' => 'new_token_refresh'
        ];
        $this->httpClient->setResponseFactory(new MockResponse(json_encode($content)));
    }
}
