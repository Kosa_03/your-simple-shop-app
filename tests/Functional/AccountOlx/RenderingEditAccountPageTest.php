<?php

declare(strict_types=1);

namespace Test\Functional\AccountOlx;

use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Test\Common\Fixtures;

class RenderingEditAccountPageTest extends WebTestCase
{
    private Fixtures $fixtures;
    private KernelBrowser $client;
    private UrlGeneratorInterface $urlGenerator;

    protected function setUp(): void
    {
        $this->client = self::createClient();

        $this->fixtures = $this->getContainer()->get(Fixtures::class);
        $this->urlGenerator = $this->getContainer()->get(UrlGeneratorInterface::class);
    }

    /**
     * @test
     */
    public function shouldRenderEditAccountPageWithStatus2xx(): void
    {
        // given
        $user = $this->fixtures->aTestUser();
        // and
        $this->client->loginUser($user);
        // and
        $olxAccount = $this->fixtures->anAccount('acc', 666, 'secret', 'url');

        // when
        $this->client->request('GET', $this->path('rest_api_olx_account_edit', $olxAccount->getId()));

        // then
        self::assertResponseIsSuccessful();
    }

    private function path(string $name, int $accountId): string
    {
        return $this->urlGenerator->generate($name, ['accountId' => $accountId]);
    }
}
