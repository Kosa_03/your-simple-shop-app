<?php

declare(strict_types=1);

namespace Test\Functional\AccountOlx;

use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Test\Common\Fixtures;

class RenderingListAccountPageTest extends WebTestCase
{
    private Fixtures $fixtures;
    private KernelBrowser $client;
    private UrlGeneratorInterface $urlGenerator;

    protected function setUp(): void
    {
        $this->client = self::createClient();

        $this->fixtures = $this->getContainer()->get(Fixtures::class);
        $this->urlGenerator = $this->getContainer()->get(UrlGeneratorInterface::class);
    }

    /**
     * @test
     */
    public function shouldRenderListAccountPageWithStatus2xx(): void
    {
        // given
        $user = $this->fixtures->aTestUser();
        // and
        $this->client->loginUser($user);
        // and
        $this->fixtures->anAccount('acc1', 666, 'secret', 'url');
        $this->fixtures->anAccount('acc2', 667, 'secret', 'url');
        $this->fixtures->anAccount('acc3', 668, 'secret', 'url');

        // when
        $this->client->request('GET', $this->path('rest_api_olx_account_list'));

        // then
        self::assertResponseIsSuccessful();
    }

    private function path(string $name): string
    {
        return $this->urlGenerator->generate($name);
    }
}
