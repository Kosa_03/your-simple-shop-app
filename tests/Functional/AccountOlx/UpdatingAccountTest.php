<?php

declare(strict_types=1);

namespace Test\Functional\AccountOlx;

use AccountOlx\Application\FindingById\FindById;
use AccountOlx\Domain\AccountDto;
use Common\Messenger\QueryBusInterface;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Test\Common\Fixtures;

class UpdatingAccountTest extends WebTestCase
{
    private QueryBusInterface $queryBus;
    private Fixtures $fixtures;
    private KernelBrowser $client;
    private UrlGeneratorInterface $urlGenerator;

    protected function setUp(): void
    {
        $this->client = self::createClient();

        $this->queryBus = $this->getContainer()->get(QueryBusInterface::class);
        $this->fixtures = $this->getContainer()->get(Fixtures::class);
        $this->urlGenerator = $this->getContainer()->get(UrlGeneratorInterface::class);
    }

    /**
     * @test
     */
    public function canUpdateAccountBasicInfo(): void
    {
        // given
        $this->loginUser();
        // and
        $account = $this->fixtures->anAccount('acc', 666, 'secret', 'url');
        // and
        $this->client->request('GET', $this->testedPath($account->getId()));

        // when
        $this->client->submitForm('edit_basic_info[submit]', [
            'edit_basic_info[name]' => 'awesome_seba',
            'edit_basic_info[clientId]' => 99999,
            'edit_basic_info[clientSecret]' => 'safety_seba',
            'edit_basic_info[redirectUrl]' => 'new_redirect_url'
        ]);

        // then
        $accountDto = $this->findByIdQuery($account->getId());
        self::assertEquals('awesome_seba', $accountDto->name);
        self::assertEquals(99999, $accountDto->clientId);
        self::assertEquals('safety_seba', $accountDto->clientSecret);
        self::assertEquals('https://new_redirect_url', $accountDto->redirectUrl);
        $response = $this->client->getResponse();
        self::assertTrue($response->isRedirection());
        self::assertTrue($response->isRedirect($this->testedPath($account->getId())));
    }

    /**
     * @test
     */
    public function canUpdateAccountTokens(): void
    {
        // given
        $this->loginUser();
        // and
        $account = $this->fixtures->anAccount('acc', 666, 'secret', 'url');
        // and
        $this->client->request('GET', $this->testedPath($account->getId()));

        // when
        $this->client->submitForm('edit_tokens[submit]', [
            'edit_tokens[refreshToken]' => 'new_refresh_token_666',
            'edit_tokens[accessToken]' => 'new_access_token_666',
            'edit_tokens[accessTokenExpiryAt]' => '2023-09-12T23:59'
        ]);

        // then
        $accountDto = $this->findByIdQuery($account->getId());
        self::assertEquals('new_refresh_token_666', $accountDto->refreshToken);
        self::assertEquals('new_access_token_666', $accountDto->accessToken);
        self::assertEquals(new \DateTimeImmutable('2023-09-12 23:59:00'), $accountDto->accessTokenExpiryAt);
        $response = $this->client->getResponse();
        self::assertTrue($response->isRedirection());
        self::assertTrue($response->isRedirect($this->testedPath($account->getId())));
    }

    /**
     * @test
     */
    public function shouldRedirectToAccountListPageWhenAnAccountWasNotFound(): void
    {
        // given
        $this->loginUser();

        // when
        $this->client->request('GET', $this->testedPath(666));

        // then
        $response = $this->client->getResponse();
        self::assertTrue($response->isRedirection());
        self::assertTrue($response->isRedirect($this->path('rest_api_olx_account_list')));
    }

    private function findByIdQuery(int $accountId): ?AccountDto
    {
        return $this->queryBus->run(FindById::new($accountId));
    }

    private function loginUser(): void
    {
        $this->client->loginUser($this->fixtures->aTestUser());
    }

    private function testedPath(int $accountId): string
    {
        return $this->urlGenerator->generate('rest_api_olx_account_edit', ['accountId' => $accountId]);
    }

    /**
     * @param array<string, mixed> $params
     */
    private function path(string $name, array $params = []): string
    {
        return $this->urlGenerator->generate($name, $params);
    }
}
