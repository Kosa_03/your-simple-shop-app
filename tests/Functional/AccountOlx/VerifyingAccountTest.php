<?php

declare(strict_types=1);

namespace Test\Functional\AccountOlx;

use AccountOlx\Application\FindingById\FindById;
use AccountOlx\Domain\AccountDto;
use AccountOlx\Domain\StateToken;
use Common\Clock;
use Common\Messenger\QueryBusInterface;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpClient\MockHttpClient;
use Symfony\Component\HttpClient\Response\MockResponse;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Test\Common\FixedClock;
use Test\Common\Fixtures;

class VerifyingAccountTest extends WebTestCase
{
    private KernelBrowser $client;
    private MockHttpClient $httpClient;
    private Fixtures $fixtures;
    private UrlGeneratorInterface $urlGenerator;
    private FixedClock $clock;
    private QueryBusInterface $queryBus;

    protected function setUp(): void
    {
        $this->client = self::createClient();

        $this->clock = $this->getContainer()->get(Clock::class);
        $this->httpClient = $this->getContainer()->get(HttpClientInterface::class);
        $this->fixtures = $this->getContainer()->get(Fixtures::class);
        $this->urlGenerator = $this->getContainer()->get(UrlGeneratorInterface::class);
        $this->queryBus = $this->getContainer()->get(QueryBusInterface::class);
    }

    /**
     * @test
     */
    public function canSaveTokensForAccountAfterVerification(): void
    {
        // given
        $stateToken = StateToken::create('token', new \DateTimeImmutable());
        // and
        $account = $this->fixtures->anAccountWithStateToken('acc', 123, 'secret', 'url', $stateToken);
        // and
        $this->configureAuthResponse();
        // and
        $now = $this->clock->setNow('2021-12-06 11:34:59');

        // when
        $this->client->request('GET', $this->testedPath($account->getStateToken(), 'code'));

        // then
        $dto = $this->findByIdQuery($account->getId());
        self::assertEquals('token_access', $dto->accessToken);
        self::assertEquals($now->modify('+86400 seconds'), $dto->accessTokenExpiryAt);
        self::assertEquals('token_refresh', $dto->refreshToken);
        self::assertNull($dto->stateToken);
        self::assertNull($dto->stateTokenCreatedAt);
        $response = $this->client->getResponse();
        self::assertTrue($response->isRedirection());
        self::assertTrue($response->isRedirect($this->path('rest_api_olx_account_list')));
    }

    /**
     * @test
     */
    public function shouldRedirectToAccountListPageWhenAnAccountWasNotFound(): void
    {
        // when
        $this->client->request('GET', $this->testedPath('abc', 'code'));

        // then
        $response = $this->client->getResponse();
        self::assertTrue($response->isRedirection());
        self::assertTrue($response->isRedirect($this->path('rest_api_olx_account_list')));
    }

    private function findByIdQuery(int $accountId): ?AccountDto
    {
        return $this->queryBus->run(FindById::new($accountId));
    }

    private function configureAuthResponse(): void
    {
        $content = [
            'access_token' => 'token_access',
            'expires_in' => 86400,
            'token_type' => 'bearer',
            'scope' => 'v2 read write',
            'refresh_token' => 'token_refresh',
        ];
        $this->httpClient->setResponseFactory(new MockResponse(json_encode($content)));
    }

    private function testedPath(string $token, string $code): string
    {
        return $this->urlGenerator->generate('rest_api_olx_auth_verify', ['state' => $token, 'code' => $code]);
    }

    private function path(string $routeName): string
    {
        return $this->urlGenerator->generate($routeName);
    }
}
