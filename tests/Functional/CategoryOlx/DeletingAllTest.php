<?php

declare(strict_types=1);

namespace Test\Functional\CategoryOlx;

use CategoryOlx\Domain\AttributeStorageInterface;
use CategoryOlx\Domain\CategoryStorageInterface;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Test\Common\CategoryFixtures;
use Test\Common\Fixtures;

class DeletingAllTest extends WebTestCase
{
    private KernelBrowser $client;
    private Fixtures $fixtures;
    private CategoryFixtures $categoryFixtures;
    private AttributeStorageInterface $attributeStorage;
    private CategoryStorageInterface $categoryStorage;
    private UrlGeneratorInterface $urlGenerator;

    protected function setUp(): void
    {
        $this->client = self::createClient();

        $this->fixtures = $this->getContainer()->get(Fixtures::class);
        $this->categoryFixtures = $this->getContainer()->get(CategoryFixtures::class);
        $this->attributeStorage = $this->getContainer()->get(AttributeStorageInterface::class);
        $this->categoryStorage = $this->getContainer()->get(CategoryStorageInterface::class);
        $this->urlGenerator = $this->getContainer()->get(UrlGeneratorInterface::class);
    }

    /**
     * @test
     */
    public function canDeleteAllAfterClickOnDeleteButton(): void
    {
        // given
        $this->loginUser();
        // and
        $this->categoryFixtures->aCategory(1, 'abc', 0, 10, false);
        // and
        $this->categoryFixtures->anAttribute(1, '', 'label', null, 'type', false, false, null, null, false, []);
        // and
        $this->client->request('GET', $this->testedPath());

        // when
        $this->client->submitForm('confirm[deleteAll]');

        // then
        $attributes = $this->attributeStorage->findAll();
        self::assertEquals(0, $attributes->count());
        $categories = $this->categoryStorage->findAll();
        self::assertEquals(0, $categories->count());
        $response = $this->client->getResponse();
        self::assertTrue($response->isRedirection());
        self::assertTrue($response->isRedirect($this->path('rest_api_olx_category')));
    }

    /**
     * @test
     */
    public function cannotDeleteAllAfterClickOnGoBackButton(): void
    {
        // given
        $this->loginUser();
        // and
        $this->categoryFixtures->aCategory(1, 'abc', 0, 10, false);
        // and
        $this->categoryFixtures->anAttribute(1, '', 'label', null, 'type', false, false, null, null, false, []);
        // and
        $this->client->request('GET', $this->testedPath());

        // when
        $this->client->submitForm('confirm[goBack]');

        // then
        $attributes = $this->attributeStorage->findAll();
        self::assertEquals(1, $attributes->count());
        $categories = $this->categoryStorage->findAll();
        self::assertEquals(1, $categories->count());
        $response = $this->client->getResponse();
        self::assertTrue($response->isRedirection());
        self::assertTrue($response->isRedirect($this->path('rest_api_olx_category')));
    }

    private function loginUser(): void
    {
        $this->client->loginUser($this->fixtures->aTestUser());
    }

    private function testedPath(): string
    {
        return $this->urlGenerator->generate('rest_api_olx_category_delete_all');
    }

    private function path(string $routeName): string
    {
        return $this->urlGenerator->generate($routeName);
    }
}
