<?php

declare(strict_types=1);

namespace Test\Functional\CategoryOlx;

use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Test\Common\Fixtures;

class RenderingDeleteAllPageTest extends WebTestCase
{
    private KernelBrowser $client;
    private Fixtures $fixtures;
    private UrlGeneratorInterface $urlGenerator;

    protected function setUp(): void
    {
        $this->client = self::createClient();

        $this->fixtures = $this->getContainer()->get(Fixtures::class);
        $this->urlGenerator = $this->getContainer()->get(UrlGeneratorInterface::class);
    }

    /**
     * @test
     */
    public function shouldRenderDeleteAllPageWithStatus2xx(): void
    {
        // given
        $this->loginUser();

        // when
        $this->client->request('GET', $this->path('rest_api_olx_category_delete_all'));

        // then
        self::assertResponseIsSuccessful();
    }

    private function loginUser(): void
    {
        $this->client->loginUser($this->fixtures->aTestUser());
    }

    private function path(string $routeName): string
    {
        return $this->urlGenerator->generate($routeName);
    }
}
