<?php

declare(strict_types=1);

namespace Test\Functional\Home;

use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class RenderingHomePageTest extends WebTestCase
{
    private KernelBrowser $client;
    private UrlGeneratorInterface $urlGenerator;

    protected function setUp(): void
    {
        $this->client = self::createClient();

        $this->urlGenerator = $this->getContainer()->get(UrlGeneratorInterface::class);
    }

    /**
     * @test
     */
    public function shouldRenderAddAccountPageWithStatus2xx(): void
    {
        // when
        $this->client->request('GET', $this->path('app_home'));

        // then
        $httpCode = $this->client->getResponse()->getStatusCode();
        self::assertGreaterThanOrEqual(200, $httpCode);
        self::assertLessThanOrEqual(299, $httpCode);
    }

    private function path(string $name): string
    {
        return $this->urlGenerator->generate($name);
    }
}
