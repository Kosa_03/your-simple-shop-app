<?php

declare(strict_types=1);

namespace Test\Functional\User;

use Common\Messenger\QueryBusInterface;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Test\Common\Fixtures;
use User\Application\FindingByLogin\FindByLogin;
use User\Domain\UserDto;

class CreatingUserTest extends WebTestCase
{
    private KernelBrowser $client;
    private QueryBusInterface $queryBus;
    private Fixtures $fixtures;
    private UrlGeneratorInterface $urlGenerator;

    protected function setUp(): void
    {
        $this->client = self::createClient();

        $this->queryBus = $this->getContainer()->get(QueryBusInterface::class);
        $this->fixtures = $this->getContainer()->get(Fixtures::class);
        $this->urlGenerator = $this->getContainer()->get(UrlGeneratorInterface::class);
    }

    /**
     * @test
     */
    public function canCreateUser(): void
    {
        // given
        $this->loginUser();
        // and
        $this->client->request('GET', $this->testedPath());

        // when
        $this->client->submitForm('create_user[submit]', [
            'create_user[login]' => 'karyna93',
            'create_user[email]' => [
                'first' => 'karcia@buziaczek.eu',
                'second' => 'karcia@buziaczek.eu'
            ],
            'create_user[plainPassword]' => [
                'first' => 'HasloSilneJakMojSeba!',
                'second' => 'HasloSilneJakMojSeba!'
            ],
            'create_user[firstName]' => 'Caroline',
            'create_user[lastName]' => 'Xyz',
            'create_user[roles]' => ['ROLE_ADMIN'],
            'create_user[active]' => 1
        ]);

        // then
        $userDto = $this->findByLoginQuery('karyna93');
        self::assertInstanceOf(UserDto::class, $userDto);
        $response = $this->client->getResponse();
        self::assertTrue($response->isRedirection());
        self::assertTrue($response->isRedirect($this->path('list_users')));
    }

    private function findByLoginQuery(string $login): ?UserDto
    {
        return $this->queryBus->run(FindByLogin::new($login));
    }

    private function loginUser(): void
    {
        $this->client->loginUser($this->fixtures->aTestUser());
    }
    
    private function testedPath(): string
    {
        return $this->urlGenerator->generate('add_user');
    }

    private function path(string $name): string
    {
        return $this->urlGenerator->generate($name);
    }
}
