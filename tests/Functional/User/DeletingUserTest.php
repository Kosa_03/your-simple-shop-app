<?php

declare(strict_types=1);

namespace Test\Functional\User;

use Common\Messenger\QueryBusInterface;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Test\Common\Fixtures;
use User\Application\FindingById\FindById;
use User\Domain\UserDto;

class DeletingUserTest extends WebTestCase
{
    private QueryBusInterface $queryBus;
    private Fixtures $fixtures;
    private KernelBrowser $client;
    private UrlGeneratorInterface $urlGenerator;

    protected function setUp(): void
    {
        $this->client = self::createClient();

        $this->queryBus = $this->getContainer()->get(QueryBusInterface::class);
        $this->fixtures = $this->getContainer()->get(Fixtures::class);
        $this->urlGenerator = $this->getContainer()->get(UrlGeneratorInterface::class);
    }

    /**
     * @test
     */
    public function canDeleteAUserAfterClickOnDeleteButton(): void
    {
        // given
        $this->loginUser();
        // and
        $user = $this->fixtures->anActiveUser('seba', 'seba@xyz.dd', 'first', 'last');
        // and
        $userId = $user->getId();
        // and
        $this->client->request('GET', $this->testedPath($user->getId()));

        // when
        $this->client->submitForm('confirm[delete]');

        // then
        $userDto = $this->findByIdQuery($userId);
        self::assertNull($userDto);
        $response = $this->client->getResponse();
        self::assertTrue($response->isRedirection());
        self::assertTrue($response->isRedirect($this->path('list_users')));
    }

    /**
     * @test
     */
    public function cannotDeleteAUserAfterClickOnGoBackButton(): void
    {
        // given
        $this->loginUser();
        // and
        $user = $this->fixtures->anActiveUser('dżessika', 'jessica@xyz.dd', 'first', 'last');
        // and
        $userId = $user->getId();
        // and
        $this->client->request('GET', $this->testedPath($user->getId()));

        // when
        $this->client->submitForm('confirm[goBack]');

        // then
        $userDto = $this->findByIdQuery($userId);
        self::assertInstanceOf(UserDto::class, $userDto);
        $response = $this->client->getResponse();
        self::assertTrue($response->isRedirection());
        self::assertTrue($response->isRedirect($this->path('list_users')));
    }

    /**
     * @test
     */
    public function shouldRedirectToUserListPageWhenAUserWasNotFound(): void
    {
        // given
        $this->loginUser();

        // when
        $this->client->request('GET', $this->testedPath(666));

        // then
        $response = $this->client->getResponse();
        self::assertTrue($response->isRedirection());
        self::assertTrue($response->isRedirect($this->path('list_users')));
    }

    private function findByIdQuery(int $userId): ?UserDto
    {
        return $this->queryBus->run(FindById::new($userId));
    }

    private function loginUser(): void
    {
        $this->client->loginUser($this->fixtures->aTestUser());
    }

    private function testedPath(int $userId): string
    {
        return $this->urlGenerator->generate('delete_user', ['userId' => $userId]);
    }

    private function path(string $name): string
    {
        return $this->urlGenerator->generate($name);
    }
}
