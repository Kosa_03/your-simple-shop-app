<?php

declare(strict_types=1);

namespace Test\Functional\User;

use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Test\Common\Fixtures;

class RenderingCreateUserPageTest extends WebTestCase
{
    private Fixtures $fixtures;
    private KernelBrowser $client;
    private UrlGeneratorInterface $urlGenerator;

    protected function setUp(): void
    {
        $this->client = self::createClient();

        $this->fixtures = $this->getContainer()->get(Fixtures::class);
        $this->urlGenerator = $this->getContainer()->get(UrlGeneratorInterface::class);
    }

    /**
     * @test
     */
    public function shouldRenderAddUserPageWithStatus2xx(): void
    {
        // given
        $user = $this->fixtures->aTestUser();
        // and
        $this->client->loginUser($user);

        // when
        $this->client->request('GET', $this->path('add_user'));

        // then
        self::assertResponseIsSuccessful();
    }

    private function path(string $name): string
    {
        return $this->urlGenerator->generate($name);
    }
}
