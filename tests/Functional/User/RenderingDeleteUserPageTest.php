<?php

declare(strict_types=1);

namespace Test\Functional\User;

use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Test\Common\Fixtures;

class RenderingDeleteUserPageTest extends WebTestCase
{
    private Fixtures $fixtures;
    private KernelBrowser $client;
    private UrlGeneratorInterface $urlGenerator;

    protected function setUp(): void
    {
        $this->client = self::createClient();

        $this->fixtures = $this->getContainer()->get(Fixtures::class);
        $this->urlGenerator = $this->getContainer()->get(UrlGeneratorInterface::class);
    }

    /**
     * @test
     */
    public function shouldRenderDeleteUserPageWithStatus2xx(): void
    {
        // given
        $user = $this->fixtures->aTestUser();
        // and
        $this->client->loginUser($user);
        // and
        $user = $this->fixtures->anActiveUser('login', 'aaa@abc.pl', 'first', 'last');

        // when
        $this->client->request('GET', $this->path('delete_user', $user->getId()));

        // then
        self::assertResponseIsSuccessful();
    }

    private function path(string $name, int $userId): string
    {
        return $this->urlGenerator->generate($name, ['userId' => $userId]);
    }
}
