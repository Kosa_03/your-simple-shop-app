<?php

declare(strict_types=1);

namespace Test\Functional\User;

use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Test\Common\Fixtures;

class RenderingListUserPageTest extends WebTestCase
{
    private Fixtures $fixtures;
    private KernelBrowser $client;
    private UrlGeneratorInterface $urlGenerator;

    protected function setUp(): void
    {
        $this->client = self::createClient();

        $this->fixtures = $this->getContainer()->get(Fixtures::class);
        $this->urlGenerator = $this->getContainer()->get(UrlGeneratorInterface::class);
    }

    /**
     * @test
     */
    public function shouldReturnListUserPageWIthStatus2xx(): void
    {
        // given
        $user = $this->fixtures->aTestUser();
        // and
        $this->client->loginUser($user);
        // and
        $this->fixtures->anActiveUser('user1', 'user1@abc.dd', 'first', 'last');
        $this->fixtures->anActiveUser('user2', 'user2@abc.dd', 'first', 'last');
        $this->fixtures->anActiveUser('user666', 'user666@abc.dd', 'first', 'last');
        $this->fixtures->anActiveUser('user7777', 'user7777@abc.dd', 'first', 'last');

        // when
        $this->client->request('GET', $this->path('list_users'));

        // then
        self::assertResponseIsSuccessful();
    }

    private function path(string $name): string
    {
        return $this->urlGenerator->generate($name);
    }
}
