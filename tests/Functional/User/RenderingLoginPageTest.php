<?php

declare(strict_types=1);

namespace Test\Functional\User;

use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class RenderingLoginPageTest extends WebTestCase
{
    private KernelBrowser $client;
    private UrlGeneratorInterface $urlGenerator;

    protected function setUp(): void
    {
        $this->client = self::createClient();

        $this->urlGenerator = $this->getContainer()->get(UrlGeneratorInterface::class);
    }

    /**
     * @test
     */
    public function shouldReturnLoginPageWithStatus2xx(): void
    {
        // when
        $this->client->request('GET', $this->path('app_login'));

        // then
        self::assertResponseIsSuccessful();
    }

    private function path(string $routeName): string
    {
        return $this->urlGenerator->generate($routeName);
    }
}
