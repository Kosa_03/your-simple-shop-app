<?php

declare(strict_types=1);

namespace Test\Functional\User;

use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Test\Common\Fixtures;

class RenderingUpdateUserPageTest extends WebTestCase
{
    private KernelBrowser $client;
    private Fixtures $fixtures;
    private UrlGeneratorInterface $urlGenerator;

    protected function setUp(): void
    {
        $this->client = self::createClient();

        $this->fixtures = $this->getContainer()->get(Fixtures::class);
        $this->urlGenerator = $this->getContainer()->get(UrlGeneratorInterface::class);
    }

    /**
     * @test
     */
    public function shouldRenderEditUserPageWithStatus2xx(): void
    {
        // given
        $testUser = $this->fixtures->aTestUser();
        // and
        $this->client->loginUser($testUser);
        // and
        $user = $this->fixtures->anActiveUser('kasia', 'kasia@buziaczek.pl', 'kasia', 'unknown');

        // when
        $this->client->request('GET', $this->path('edit_user', $user->getId()));

        // then
        self::assertResponseIsSuccessful();
    }

    private function path(string $routeName, int $userId): string
    {
        return $this->urlGenerator->generate($routeName, ['userId' => $userId]);
    }
}
