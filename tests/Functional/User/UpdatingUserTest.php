<?php

declare(strict_types=1);

namespace Test\Functional\User;

use Common\Messenger\QueryBusInterface;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Test\Common\Fixtures;
use User\Application\FindingById\FindById;
use User\Domain\UserDto;
use User\Domain\UserStorageInterface;

class UpdatingUserTest extends WebTestCase
{
    private QueryBusInterface $queryBus;
    private Fixtures $fixtures;
    private KernelBrowser $client;
    private UserPasswordHasherInterface $passwordHasher;
    private UrlGeneratorInterface $urlGenerator;
    private UserStorageInterface $userStorage;

    protected function setUp(): void
    {
        $this->client = self::createClient();

        $this->queryBus = $this->getContainer()->get(QueryBusInterface::class);
        $this->fixtures = $this->getContainer()->get(Fixtures::class);
        $this->passwordHasher = $this->getContainer()->get(UserPasswordHasherInterface::class);
        $this->urlGenerator = $this->getContainer()->get(UrlGeneratorInterface::class);
        $this->userStorage = $this->getContainer()->get(UserStorageInterface::class);
    }

    /**
     * @test
     */
    public function canUpdateUserBasicInfo(): void
    {
        // given
        $this->loginUser();
        // and
        $user = $this->fixtures->anActiveUser('login', 'email@xyz.dd', 'first', 'last');
        // and
        $crawler = $this->client->request('GET', $this->testedPath($user->getId()));
        // and
        $form = $crawler->selectButton('edit_basic_info[submit]')->form();

        // when
        $form['edit_basic_info[firstName]'] = 'kasia';
        $form['edit_basic_info[lastName]'] = 'pilarczyk';
        $form['edit_basic_info[active]'] = 1;
        $form['edit_basic_info[roles][0]']->tick();
        $form['edit_basic_info[roles][1]']->untick();
        $form['edit_basic_info[roles][2]']->untick();
        $this->client->submit($form);

        // then
        $userDto = $this->findByIdQuery($user->getId());
        self::assertEquals('kasia', $userDto->firstName);
        self::assertEquals('pilarczyk', $userDto->lastName);
        self::assertTrue($userDto->active);
        self::assertEquals(['ROLE_ADMIN'], $userDto->roles);
        $response = $this->client->getResponse();
        self::assertTrue($response->isRedirection());
        self::assertTrue($response->isRedirect($this->path('edit_user', ['userId' => $user->getId()])));
    }

    /**
     * @test
     */
    public function canUpdateUserEmail(): void
    {
        // given
        $this->loginUser();
        // and
        $user = $this->fixtures->anActiveUser('login', 'email@xyz.dd', 'first', 'last');
        // and
        $this->client->request('GET', $this->testedPath($user->getId()));

        // when
        $this->client->submitForm('edit_email[submit]', [
            'edit_email[email]' => [
                'first' => 'katarzyna.puchalska@abc.dd',
                'second' => 'katarzyna.puchalska@abc.dd'
            ]
        ]);

        // then
        $userDto = $this->findByIdQuery($user->getId());
        self::assertEquals('katarzyna.puchalska@abc.dd', $userDto->email);
        $response = $this->client->getResponse();
        self::assertTrue($response->isRedirection());
        self::assertTrue($response->isRedirect($this->path('edit_user', ['userId' => $user->getId()])));
    }

    /**
     * @test
     */
    public function canUpdateUserPassword(): void
    {
        // given
        $this->loginUser();
        // and
        $user = $this->fixtures->anActiveUser('login', 'email@xyz.dd', 'first', 'last');
        // and
        $this->client->request('GET', $this->testedPath($user->getId()));

        // when
        $this->client->submitForm('edit_password[submit]', [
            'edit_password[plainPassword]' => [
                'first' => 'KajJeToHaslo???',
                'second' => 'KajJeToHaslo???'
            ]
        ]);

        // then
        $user = $this->userStorage->find($user->getId());
        self::assertTrue($this->passwordHasher->isPasswordValid($user, 'KajJeToHaslo???'));
        $response = $this->client->getResponse();
        self::assertTrue($response->isRedirection());
        self::assertTrue($response->isRedirect($this->path('edit_user', ['userId' => $user->getId()])));
    }

    /**
     * @test
     */
    public function shouldRedirectToUserListPageWhenAUserWasNotFound(): void
    {
        // given
        $this->loginUser();

        // when
        $this->client->request('GET', $this->testedPath(666));

        // then
        $response = $this->client->getResponse();
        self::assertTrue($response->isRedirection());
        self::assertTrue($response->isRedirect($this->path('list_users')));
    }

    private function findByIdQuery(int $userId): ?UserDto
    {
        return $this->queryBus->run(FindById::new($userId));
    }

    private function loginUser(): void
    {
        $this->client->loginUser($this->fixtures->aTestUser());
    }

    private function testedPath(int $userId): string
    {
        return $this->urlGenerator->generate('edit_user', ['userId' => $userId]);
    }

    private function path(string $name, array $params = []): string
    {
        return $this->urlGenerator->generate($name, $params);
    }
}
