<?php

declare(strict_types=1);

namespace Test\Integration\ACL;

use AccountOlx\Domain\Account;
use ACL\AccountOlxToCategoryOlx\AccountFacadeInterface;
use Common\Clock;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Test\Common\FixedClock;
use Test\Common\Fixtures;

class AccountOlxToCategoryOlxTest extends KernelTestCase
{
    private FixedClock $clock;
    private Fixtures $fixtures;
    private AccountFacadeInterface $accountAclFacade;

    protected function setUp(): void
    {
        $this->clock = $this->getContainer()->get(Clock::class);
        $this->fixtures = $this->getContainer()->get(Fixtures::class);
        $this->accountAclFacade = $this->getContainer()->get(AccountFacadeInterface::class);
    }

    /**
     * @test
     */
    public function canFindAccessTokenForAccountWithId(): void
    {
        // given
        $account = $this->anAccountWithAccessToken('at_token_7777');

        // when
        $dto = $this->accountAclFacade->findAccessTokenFor($account->getId());

        // then
        self::assertEquals('at_token_7777', $dto->accessToken);
    }

    /**
     * @test
     */
    public function shouldReturnNullWhenAccountWwasNotFound(): void
    {
        // when
        $dto = $this->accountAclFacade->findAccessTokenFor(997);

        // then
        self::assertNull($dto);
    }

    /**
     * @test
     */
    public function canReturnCorrectAccountListForSelectType(): void
    {
        // given
        $account1 = $this->fixtures->anAccount('acc1', 101, 'secret', 'url');
        $account2 = $this->fixtures->anAccount('acc2', 102, 'secret', 'url');
        $account3 = $this->fixtures->anAccount('acc3', 103, 'secret', 'url');

        // when
        $list = $this->accountAclFacade->accountListForSelectType();

        // then
        $nameList = array_keys($list);
        $idList = array_values($list);
        self::assertEquals($account1->getId(), $idList[0]);
        self::assertEquals($account1->getName(), $nameList[0]);
        self::assertEquals($account2->getId(), $idList[1]);
        self::assertEquals($account2->getName(), $nameList[1]);
        self::assertEquals($account3->getId(), $idList[2]);
        self::assertEquals($account3->getName(), $nameList[2]);
    }

    public function anAccountWithAccessToken(string $accessToken): Account
    {
        return $this->fixtures->anAccountWithAccessToken(
            'name',
            666,
            'secret',
            'url',
            $accessToken,
            $this->clock->now()
        );
    }
}
