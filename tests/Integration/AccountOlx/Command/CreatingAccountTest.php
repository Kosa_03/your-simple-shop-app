<?php

declare(strict_types=1);

namespace Test\Integration\AccountOlx\Command;

use AccountOlx\Application\CreatingAccount\CreateAccount;
use AccountOlx\Domain\AccountDto;
use AccountOlx\Domain\AccountStorageInterface;
use Common\Messenger\Command;
use Common\Messenger\CommandBusInterface;
use DateTimeImmutable;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class CreatingAccountTest extends KernelTestCase
{
    private AccountStorageInterface $accountStorage;
    private CommandBusInterface $commandBus;

    protected function setUp(): void
    {
        $this->accountStorage = $this->getContainer()->get(AccountStorageInterface::class);
        $this->commandBus = $this->getContainer()->get(CommandBusInterface::class);
    }

    /**
     * @test
     */
    public function canCreateAccount(): void
    {
        // given
        $accountDto = $this->getAccountDto();

        // when
        $this->runCommand(CreateAccount::new($accountDto));

        // then
        $account = $this->accountStorage->findOneBy(['clientId' => 123456]);
        self::assertNotNull($account->getId());
        self::assertNotNull($account->getCreatedAt());
        self::assertEquals('olx_account', $account->getName());
        self::assertEquals('client_secret', $account->getClientSecret());
        self::assertEquals('access_token', $account->getAccessToken());
        self::assertEquals('refresh_token', $account->getRefreshToken());
        self::assertEquals('redirect_url', $account->getRedirectUrl());
    }

    private function getAccountDto(): AccountDto
    {
        return AccountDto::create(
            'olx_account',
            123456,
            'client_secret',
            'refresh_token',
            'access_token',
            new DateTimeImmutable(),
            'redirect_url'
        );
    }

    private function runCommand(Command $command): void
    {
        $this->commandBus->dispatch($command);
    }
}
