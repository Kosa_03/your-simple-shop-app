<?php

declare(strict_types=1);

namespace Test\Integration\AccountOlx\Command;

use AccountOlx\Application\CreatingStateToken\CreateStateToken;
use AccountOlx\Application\FindingById\FindById;
use AccountOlx\Domain\Account;
use AccountOlx\Domain\AccountDto;
use AccountOlx\Domain\StateToken;
use Common\Clock;
use Common\Messenger\Command;
use Common\Messenger\CommandBusInterface;
use Common\Messenger\QueryBusInterface;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Test\Common\FixedClock;
use Test\Common\Fixtures;

class CreatingStateTokenTest extends KernelTestCase
{
    private FixedClock $clock;
    private Fixtures $fixtures;
    private CommandBusInterface $commandBus;
    private QueryBusInterface $queryBus;

    protected function setUp(): void
    {
        $this->clock = $this->getContainer()->get(Clock::class);
        $this->fixtures = $this->getContainer()->get(Fixtures::class);
        $this->commandBus = $this->getContainer()->get(CommandBusInterface::class);
        $this->queryBus = $this->getContainer()->get(QueryBusInterface::class);
    }

    /**
     * @test
     */
    public function canCreateStateTokenWhenTokenDoesntExistBefore(): void
    {
        // given
        $beforeDto = $this->fixtures->anAccountDto('account', 123, 'secret', 'url');

        // when
        $this->runCommand(CreateStateToken::new($beforeDto->id));

        // then
        $freshDto = $this->findByIdQuery($beforeDto->id);
        self::assertNotEquals($beforeDto->stateToken, $freshDto->stateToken);
        self::assertNull($beforeDto->stateTokenCreatedAt);
        self::assertNotNull($freshDto->stateTokenCreatedAt);
    }

    /**
     * @test
     */
    public function canChangeStateTokenWhenTokenExists(): void
    {
        // given
        $stateToken = StateToken::create('random', $this->clock->setNow('2002-09-21 13:41:12'));
        // and
        $account = $this->anAccountWithState($stateToken);
        // and
        $now = $this->clock->setNow('2012-01-21 09:25:12');

        // when
        $this->runCommand(CreateStateToken::new($account->getId()));

        // then
        $accountDto = $this->findByIdQuery($account->getId());
        self::assertNotEquals($stateToken->getToken(), $accountDto->stateToken);
        self::assertNotEquals($stateToken->getCreatedAt(), $accountDto->stateTokenCreatedAt);
        self::assertEquals($now, $accountDto->stateTokenCreatedAt);
    }

    private function anAccountWithState(StateToken $stateToken): Account
    {
        return $this->fixtures->anAccountWithStateToken('account', 123, 'secret', 'url', $stateToken);
    }

    private function findByIdQuery(?int $accountId): ?AccountDto
    {
        return $this->queryBus->run(FindById::new($accountId));
    }

    private function runCommand(Command $command): void
    {
        $this->commandBus->dispatch($command);
    }
}
