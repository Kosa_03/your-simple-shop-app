<?php

declare(strict_types=1);

namespace Test\Integration\AccountOlx\Command;

use AccountOlx\Application\DeletingAccount\DeleteAccount;
use AccountOlx\Domain\AccountStorageInterface;
use Common\Messenger\Command;
use Common\Messenger\CommandBusInterface;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Test\Common\Fixtures;

class DeletingAccountTest extends KernelTestCase
{
    private Fixtures $fixtures;
    private AccountStorageInterface $accountStorage;
    private CommandBusInterface $commandBus;

    protected function setUp(): void
    {
        $this->fixtures = $this->getContainer()->get(Fixtures::class);
        $this->accountStorage = $this->getContainer()->get(AccountStorageInterface::class);
        $this->commandBus = $this->getContainer()->get(CommandBusInterface::class);
    }

    /**
     * @test
     */
    public function canDeleteAccount(): void
    {
        // given
        $account = $this->fixtures->anAccount('account', 123, 'secret', 'url');
        // and
        $accountId = $account->getId();

        // when
        $this->runCommand(DeleteAccount::new($account->getId()));

        // then
        $account = $this->accountStorage->find($accountId);
        self::assertNull($account);
    }

    private function runCommand(Command $command): void
    {
        $this->commandBus->dispatch($command);
    }
}
