<?php

declare(strict_types=1);

namespace Test\Integration\AccountOlx\Command;

use AccountOlx\Application\FindingById\FindById;
use AccountOlx\Application\SavingTokensAfterConnection\SaveTokensAfterConnection;
use AccountOlx\Domain\AccountDto;
use AccountOlx\Domain\StateToken;
use Common\AppResult;
use Common\Clock;
use Common\Messenger\Command;
use Common\Messenger\CommandBusInterface;
use Common\Messenger\QueryBusInterface;
use DateTimeImmutable;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Test\Common\FixedClock;
use Test\Common\Fixtures;

class SavingTokensAfterConnectionTest extends KernelTestCase
{
    private FixedClock $clock;
    private Fixtures $fixtures;
    private CommandBusInterface $commandBus;
    private QueryBusInterface $queryBus;

    protected function setUp(): void
    {
        $this->clock = $this->getContainer()->get(Clock::class);
        $this->fixtures = $this->getContainer()->get(Fixtures::class);
        $this->commandBus = $this->getContainer()->get(CommandBusInterface::class);
        $this->queryBus = $this->getContainer()->get(QueryBusInterface::class);
    }

    /**
     * @test
     */
    public function canSaveAccessRefreshTokensAndResetStateToken(): void
    {
        // given
        $stateToken = StateToken::create('random', $this->clock->now());
        // and
        $account = $this->fixtures->anAccountWithStateToken('account', 123, 'secret', 'url', $stateToken);
        // and
        $expiryAt = $this->expireIn3Days();

        // when
        $this->runCommand(SaveTokensAfterConnection::new($account->getId(), 'new_refresh', 'new_access', $expiryAt));

        // then
        $accountDto = $this->findByIdQuery($account->getId());
        self::assertEquals('new_refresh', $accountDto->refreshToken);
        self::assertEquals('new_access', $accountDto->accessToken);
        self::assertEquals($expiryAt, $accountDto->accessTokenExpiryAt);
        self::assertNull($accountDto->stateToken);
        self::assertNull($accountDto->stateTokenCreatedAt);
    }

    /**
     * @test
     */
    public function canBeSuccessfulWhenUserWasNotFound(): void
    {
        // given
        $expiryAt = $this->expireIn3Days();

        // when
        $result = $this->runCommand(SaveTokensAfterConnection::new(-1, 'new_refresh', 'new_access', $expiryAt));

        // then
        self::assertTrue($result->isSuccessful());
    }

    private function findByIdQuery(?int $accountId): ?AccountDto
    {
        return $this->queryBus->run(FindById::new($accountId));
    }

    private function runCommand(Command $command): AppResult
    {
        return $this->commandBus->dispatch($command);
    }

    private function expireIn3Days(): DateTimeImmutable
    {
        return $this->clock->setNow('+72 hours');
    }
}
