<?php

declare(strict_types=1);

namespace Test\Integration\AccountOlx\Command;

use AccountOlx\Application\FindingById\FindById;
use AccountOlx\Application\UpdatingAccessRefreshTokens\UpdateAccessRefreshTokens;
use AccountOlx\Domain\AccountDto;
use Common\Clock;
use Common\Messenger\Command;
use Common\Messenger\CommandBusInterface;
use Common\Messenger\QueryBusInterface;
use DateTimeImmutable;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Test\Common\FixedClock;
use Test\Common\Fixtures;

class UpdatingAccessAndRefreshTokensTest extends KernelTestCase
{
    private FixedClock $clock;
    private Fixtures $fixtures;
    private CommandBusInterface $commandBus;
    private QueryBusInterface $queryBus;

    protected function setUp(): void
    {
        $this->clock = $this->getContainer()->get(Clock::class);
        $this->fixtures = $this->getContainer()->get(Fixtures::class);
        $this->commandBus = $this->getContainer()->get(CommandBusInterface::class);
        $this->queryBus = $this->getContainer()->get(QueryBusInterface::class);
    }

    /**
     * @test
     */
    public function canUpdateTokenFieldForAccount(): void
    {
        // given
        $accountDto = $this->fixtures->anAccountDto('account', 123, 'secret', 'url');
        // and
        $expiryAt = $this->expireIn3Days();

        // when
        $this->runCommand(UpdateAccessRefreshTokens::new($accountDto->id, 'new_refresh', 'new_access', $expiryAt));

        // then
        $freshDto = $this->findByIdQuery($accountDto->id);
        self::assertEquals('new_refresh', $freshDto->refreshToken);
        self::assertEquals('new_access', $freshDto->accessToken);
        self::assertEquals($expiryAt, $freshDto->accessTokenExpiryAt);
    }

    /**
     * @test
     */
    public function canUpdateTokenFieldsWithNullValues(): void
    {
        // given
        $accountDto = $this->fixtures->anAccountDto('account', 123, 'secret', 'url');

        // when
        $this->runCommand(UpdateAccessRefreshTokens::new($accountDto->id, null, null, null));

        // then
        $freshDto = $this->findByIdQuery($accountDto->id);
        self::assertNull($freshDto->refreshToken);
        self::assertNull($freshDto->accessToken);
        self::assertNull($freshDto->accessTokenExpiryAt);
    }

    private function findByIdQuery(?int $accountId): ?AccountDto
    {
        return $this->queryBus->run(FindById::new($accountId));
    }

    private function runCommand(Command $command): void
    {
        $this->commandBus->dispatch($command);
    }

    private function expireIn3Days(): DateTimeImmutable
    {
        return $this->clock->setNow('+72 hours');
    }
}
