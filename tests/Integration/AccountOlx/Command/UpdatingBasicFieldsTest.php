<?php

declare(strict_types=1);

namespace Test\Integration\AccountOlx\Command;

use AccountOlx\Application\FindingById\FindById;
use AccountOlx\Application\UpdatingBasicFields\UpdateBasicInfo;
use AccountOlx\Domain\AccountDto;
use Common\Messenger\Command;
use Common\Messenger\CommandBusInterface;
use Common\Messenger\QueryBusInterface;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Test\Common\Fixtures;

class UpdatingBasicFieldsTest extends KernelTestCase
{
    private Fixtures $fixtures;
    private CommandBusInterface $commandBus;
    private QueryBusInterface $queryBus;

    protected function setUp(): void
    {
        $this->fixtures = $this->getContainer()->get(Fixtures::class);
        $this->commandBus = $this->getContainer()->get(CommandBusInterface::class);
        $this->queryBus = $this->getContainer()->get(QueryBusInterface::class);
    }

    /**
     * @test
     */
    public function canUpdateBasicField(): void
    {
        // given
        $accountDto = $this->fixtures->anAccountDto('account', 123, 'secret', 'url');

        // when
        $this->runCommand(UpdateBasicInfo::new($accountDto->id, 'olx_acc', 456, 'new_secret', 'new_url'));

        // then
        $freshDto = $this->findByIdQuery($accountDto->id);
        self::assertEquals('olx_acc', $freshDto->name);
        self::assertEquals(456, $freshDto->clientId);
        self::assertEquals('new_secret', $freshDto->clientSecret);
        self::assertEquals('new_url', $freshDto->redirectUrl);
    }

    /**
     * @test
     */
    public function cannotUpdateNonExistsAccount(): void
    {
        // given
        $accountDto = $this->fixtures->anAccountDto('account', 123, 'secret', 'url');

        // when
        $this->runCommand(UpdateBasicInfo::new(-1, 'olx_acc', 456, 'new_secret', 'new_url'));

        // then
        self::assertNull($this->findByIdQuery(-1));
    }

    private function findByIdQuery(?int $accountId): ?AccountDto
    {
        return $this->queryBus->run(FindById::new($accountId));
    }

    private function runCommand(Command $command): void
    {
        $this->commandBus->dispatch($command);
    }
}
