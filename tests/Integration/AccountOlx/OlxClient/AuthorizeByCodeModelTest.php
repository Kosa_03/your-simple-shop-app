<?php

declare(strict_types=1);

namespace Test\Integration\AccountOlx\OlxClient;

use AccountOlx\Infrastructure\OlxClientAdapter;
use AccountOlx\Infrastructure\OlxModel\Authorization;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\HttpClient\MockHttpClient;
use Symfony\Component\HttpClient\Response\MockResponse;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class AuthorizeByCodeModelTest extends KernelTestCase
{
    private MockHttpClient $httpClient;
    private OlxClientAdapter $olxClient;

    protected function setUp(): void
    {
        $this->httpClient = $this->getContainer()->get(HttpClientInterface::class);
        $this->olxClient = $this->getContainer()->get(OlxClientAdapter::class);
    }

    /**
     * @test
     */
    public function canReturnAuthorizationModelWhenAuthorizeByCode(): void
    {
        // given
        $this->configureAuthResponse();

        // when
        $model = $this->olxClient->authorizeByCode(12345, 'secret', 'code');

        // then
        self::assertInstanceOf(Authorization::class, $model);
        self::assertEquals('new_token_access', $model->accessToken());
        self::assertEquals(86400, $model->expiresIn());
        self::assertEquals('bearer', $model->tokenType());
        self::assertEquals('v2 read write', $model->scope());
        self::assertEquals('new_token_refresh', $model->refreshToken());
    }

    private function configureAuthResponse(): void
    {
        $content = [
            'access_token' => 'new_token_access',
            'expires_in' => 86400,
            'token_type' => 'bearer',
            'scope' => 'v2 read write',
            'refresh_token' => 'new_token_refresh'
        ];
        $this->httpClient->setResponseFactory(new MockResponse(json_encode($content)));
    }
}
