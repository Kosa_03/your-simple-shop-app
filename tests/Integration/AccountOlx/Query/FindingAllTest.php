<?php

declare(strict_types=1);

namespace Test\Integration\AccountOlx\Query;

use AccountOlx\Application\FindingAll\FindAll;
use AccountOlx\Domain\AccountDto;
use Common\Messenger\QueryBusInterface;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Test\Common\Fixtures;

class FindingAllTest extends KernelTestCase
{
    private Fixtures $fixtures;
    private QueryBusInterface $queryBus;

    protected function setUp(): void
    {
        $this->fixtures = $this->getContainer()->get(Fixtures::class);
        $this->queryBus = $this->getContainer()->get(QueryBusInterface::class);
    }

    /**
     * @test
     */
    public function canFindAllAccounts(): void
    {
        // given
        $this->fixtures->anAccount('acc1', 111, 'secret', 'url');
        $this->fixtures->anAccount('acc2', 122, 'secret', 'url');
        $this->fixtures->anAccount('acc3', 133, 'secret', 'url');
        $this->fixtures->anAccount('acc4', 144, 'secret', 'url');
        $this->fixtures->anAccount('acc5', 155, 'secret', 'url');

        // when
        $allAccounts = $this->findAllAccountsQuery();

        // then
        self::assertCount(5, $allAccounts);
    }

    /**
     * @return array<int, AccountDto>
     */
    private function findAllAccountsQuery(): array
    {
        return $this->queryBus->run(FindAll::new());
    }
}
