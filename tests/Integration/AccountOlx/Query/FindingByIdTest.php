<?php

declare(strict_types=1);

namespace Test\Integration\AccountOlx\Query;

use AccountOlx\Application\FindingById\FindById;
use AccountOlx\Domain\AccountDto;
use Common\Messenger\QueryBusInterface;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Test\Common\Fixtures;

class FindingByIdTest extends KernelTestCase
{
    private Fixtures $fixtures;
    private QueryBusInterface $queryBus;

    protected function setUp(): void
    {
        $this->fixtures = $this->getContainer()->get(Fixtures::class);
        $this->queryBus = $this->getContainer()->get(QueryBusInterface::class);
    }

    /**
     * @test
     */
    public function canFindAccountById(): void
    {
        // given
        $account = $this->fixtures->anAccount('acc', 666, 'secret', 'url');

        // when
        $dto = $this->findByIdQuery($account->getId());

        // then
        self::assertInstanceOf(AccountDto::class, $dto);
    }

    /**
     * @test
     */
    public function shouldReturnNullWhenAccountWasNotFound(): void
    {
        // when
        $dto = $this->findByIdQuery(666);

        // then
        self::assertNull($dto);
    }

    private function findByIdQuery(int $accountId): ?AccountDto
    {
        return $this->queryBus->run(FindById::new($accountId));
    }
}
