<?php

declare(strict_types=1);

namespace Test\Integration\AccountOlx\Query;

use AccountOlx\Application\FindingByStateToken\FindByStateToken;
use AccountOlx\Domain\Account;
use AccountOlx\Domain\AccountDto;
use AccountOlx\Domain\StateToken;
use Common\Messenger\QueryBusInterface;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Test\Common\Fixtures;

class FindingByStateTokenTest extends KernelTestCase
{
    private Fixtures $fixtures;
    private QueryBusInterface $queryBus;

    protected function setUp(): void
    {
        $this->fixtures = $this->getContainer()->get(Fixtures::class);
        $this->queryBus = $this->getContainer()->get(QueryBusInterface::class);
    }

    /**
     * @test
     */
    public function canFindAccountByStateToken(): void
    {
        // given
        $account = $this->anAccountWithStateToken(StateToken::create('random_token', new \DateTimeImmutable()));

        // when
        $accountDto = $this->findByStateTokenQuery('random_token');

        // then
        self::assertInstanceOf(AccountDto::class, $accountDto);
        self::assertEquals($account->getId(), $accountDto->id);
        self::assertEquals('random_token', $accountDto->stateToken);
    }

    /**
     * @test
     */
    public function shouldReturnNullWhenStateTokenIsNull(): void
    {
        // given
        $account = $this->fixtures->anAccount('acc', 666, 'secret', 'red_url');

        // when
        $accountDto = $this->findByStateTokenQuery(null);

        // then
        self::assertNull($accountDto);
    }

    /**
     * @test
     */
    public function shouldReturnNullWhenAccountWasNotFound(): void
    {
        // when
        $accountDto = $this->findByStateTokenQuery('state_token');

        // then
        self::assertNull($accountDto);
    }

    private function anAccountWithStateToken(StateToken $stateToken): Account
    {
        return $this->fixtures->anAccountWithStateToken('acc', 123, 'secret', 'url', $stateToken);
    }

    private function findByStateTokenQuery(?string $token): ?AccountDto
    {
        return $this->queryBus->run(FindByStateToken::new($token));
    }
}
