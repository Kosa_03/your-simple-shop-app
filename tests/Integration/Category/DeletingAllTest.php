<?php

declare(strict_types=1);

namespace Test\Integration\Category;

use CategoryOlx\Application\DeletingAll\DeleteAll;
use CategoryOlx\Domain\AttributeStorageInterface;
use CategoryOlx\Domain\CategoryStorageInterface;
use Common\Messenger\Command;
use Common\Messenger\CommandBusInterface;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Test\Common\CategoryFixtures;

class DeletingAllTest extends KernelTestCase
{
    private CategoryFixtures $categoryFixtures;
    private CommandBusInterface $commandBus;
    private AttributeStorageInterface $attributeStorage;
    private CategoryStorageInterface $categoryStorage;

    protected function setUp(): void
    {
        $this->categoryFixtures = $this->getContainer()->get(CategoryFixtures::class);
        $this->commandBus = $this->getContainer()->get(CommandBusInterface::class);
        $this->attributeStorage = $this->getContainer()->get(AttributeStorageInterface::class);
        $this->categoryStorage = $this->getContainer()->get(CategoryStorageInterface::class);
    }

    /**
     * @test
     */
    public function canDeleteAllCategories(): void
    {
        // given
        $this->categoryFixtures->aCategory(1, 'abc', 0, 10, false);
        $this->categoryFixtures->aCategory(2, 'abc', 0, 10, false);
        $this->categoryFixtures->aCategory(3, 'abc', 0, 10, false);
        $this->categoryFixtures->aCategory(4, 'abc', 0, 10, false);
        $this->categoryFixtures->aCategory(5, 'abc', 0, 10, false);

        // when
        $this->runCommand(DeleteAll::new());

        // then
        $collection = $this->categoryStorage->findAll();
        self::assertEquals(0, $collection->count());
    }

    /**
     * @test
     */
    public function canDeleteAllAttributes(): void
    {
        // given
        $this->categoryFixtures->anAttribute(1, '', 'label', null, 'type', false, false, null, null, false, []);
        $this->categoryFixtures->anAttribute(1, '', 'label', null, 'type', false, false, null, null, false, []);
        $this->categoryFixtures->anAttribute(1, '', 'label', null, 'type', false, false, null, null, false, []);
        $this->categoryFixtures->anAttribute(1, '', 'label', null, 'type', false, false, null, null, false, []);
        $this->categoryFixtures->anAttribute(1, '', 'label', null, 'type', false, false, null, null, false, []);
        $this->categoryFixtures->anAttribute(1, '', 'label', null, 'type', false, false, null, null, false, []);
        $this->categoryFixtures->anAttribute(1, '', 'label', null, 'type', false, false, null, null, false, []);
        $this->categoryFixtures->anAttribute(1, '', 'label', null, 'type', false, false, null, null, false, []);

        // when
        $this->runCommand(DeleteAll::new());

        // then
        $collection = $this->attributeStorage->findAll();
        self::assertEquals(0, $collection->count());
    }

    private function runCommand(Command $command): void
    {
        $this->commandBus->dispatch($command);
    }
}
