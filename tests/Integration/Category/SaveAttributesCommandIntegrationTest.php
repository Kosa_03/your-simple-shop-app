<?php

declare(strict_types=1);

namespace Test\Integration\Category;

use CategoryOlx\Application\SavingAttributes\SaveAttributes;
use CategoryOlx\Application\SavingAttributes\SaveAttributesHandler;
use CategoryOlx\Infrastructure\AttributeRepository;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Messenger\Transport\TransportInterface;

class SaveAttributesCommandIntegrationTest extends KernelTestCase
{
    private MessageBusInterface $commandBus;
    private AttributeRepository $attributeRepository;
    private SaveAttributesHandler $saveAttributesHandler;
    private TransportInterface $inMemoryTransport;

    protected function setUp(): void
    {
        $this->commandBus = $this->getContainer()->get('command.bus');
        $this->inMemoryTransport = $this->getContainer()->get('messenger.transport.async');
        $this->attributeRepository = $this->getContainer()->get(AttributeRepository::class);
        $this->saveAttributesHandler = $this->getContainer()->get(SaveAttributesHandler::class);
    }

    /**
     * @test
     *
     * @dataProvider attributesJsonDataProvider
     */
    public function canSendMessageToAsyncTransport(int $categoryId, array $data): void
    {
        // when
        $this->commandBus->dispatch(new SaveAttributes($categoryId, $data['data']));

        // then
        self::assertCount(1, $this->inMemoryTransport->getSent());
    }

    /**
     * @test
     *
     * @dataProvider attributesJsonDataProvider
     */
    public function canSaveAttributesByCommandHandler(int $categoryId, array $data, int $count): void
    {
        // when
        $handler = $this->saveAttributesHandler;
        $handler(new SaveAttributes($categoryId, $data['data']));

        // then
        self::assertEquals($count, $this->attributeRepository->findAll()->count());
    }

    public function attributesJsonDataProvider(): array
    {
        return [
            0 => [
                4,
                json_decode(file_get_contents(__DIR__ . '/Json/category_4_attributes.json'), true),
                13
            ],
            1 => [
                184,
                json_decode(file_get_contents(__DIR__ . '/Json/category_184_attributes.json'), true),
                14
            ],
            2 => [
                1529,
                json_decode(file_get_contents(__DIR__ . '/Json/category_1529_attributes.json'), true),
                13
            ],
            3 => [
                1890,
                json_decode(file_get_contents(__DIR__ . '/Json/category_1890_attributes.json'), true),
                1
            ]
        ];
    }
}
