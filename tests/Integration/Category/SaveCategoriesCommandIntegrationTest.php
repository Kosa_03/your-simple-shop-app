<?php

declare(strict_types=1);

namespace Test\Integration\Category;

use CategoryOlx\Application\SavingCategories\SaveCategories;
use CategoryOlx\Application\SavingCategories\SaveCategoriesHandler;
use CategoryOlx\Infrastructure\CategoryRepository;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Messenger\Transport\TransportInterface;

class SaveCategoriesCommandIntegrationTest extends KernelTestCase
{
    private MessageBusInterface $commandBus;
    private CategoryRepository $categoryRepository;
    private SaveCategoriesHandler $saveCategoriesHandler;
    private TransportInterface $inMemoryTransport;

    protected function setUp(): void
    {
        $this->commandBus = $this->getContainer()->get('command.bus');
        $this->inMemoryTransport = $this->getContainer()->get('messenger.transport.async');
        $this->categoryRepository = $this->getContainer()->get(CategoryRepository::class);
        $this->saveCategoriesHandler = $this->getContainer()->get(SaveCategoriesHandler::class);
    }

    /**
     * @test
     */
    public function canSendMessageToAsyncTransport(): void
    {
        // given
        $data = $this->getCategoriesJsonData();

        // when
        $this->commandBus->dispatch(new SaveCategories($data['data']));

        // then
        self::assertCount(1, $this->inMemoryTransport->getSent());
    }

    /**
     * @test
     */
    public function canSaveCategoriesByCommandHandler(): void
    {
        // given
        $data = $this->getCategoriesJsonData();

        // when
        $handler = $this->saveCategoriesHandler;
        $handler(new SaveCategories($data['data']));

        // then
        self::assertEquals(452, $this->categoryRepository->findAll()->count());
    }

    private function getCategoriesJsonData(): array
    {
        return json_decode(file_get_contents(__DIR__ . '/Json/categories.json'), true);
    }
}
