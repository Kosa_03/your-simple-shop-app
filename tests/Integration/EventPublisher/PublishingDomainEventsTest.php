<?php

declare(strict_types=1);

namespace Test\Integration\EventPublisher;

use EventPublisher\Core\EventPublisherInterface;
use EventPublisher\DomainEventPublisherInterface;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Messenger\Transport\TransportInterface;
use Test\Common\Event\FirstDomainEvent;
use Test\Common\Event\SecondDomainEvent;

class PublishingDomainEventsTest extends KernelTestCase
{
    private TransportInterface $asyncTransport;
    private EventPublisherInterface $eventPublisher;

    protected function setUp(): void
    {
        $this->asyncTransport = $this->getContainer()->get('messenger.transport.async');
        $this->eventPublisher = $this->getContainer()->get(EventPublisherInterface::class);
    }

    /**
     * @test
     */
    public function canPublishDomainEvents(): void
    {
        // when
        $this->eventPublisher->publish([
            FirstDomainEvent::new(),
            SecondDomainEvent::new()
        ]);

        // then
        self::assertCount(2, $this->asyncTransport->get());
    }
}
