<?php

declare(strict_types=1);

namespace Test\Integration\EventPublisher;

use Common\Clock;
use DateTimeImmutable;
use EventPublisher\Core\IntegrationEvent\IntegrationEventPublisher;
use EventPublisher\Core\IntegrationEvent\OutboxMessageRepository;
use EventPublisher\Shared\EventId;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Messenger\Transport\TransportInterface;
use Test\Common\Event\BadIntegrationEvent;
use Test\Common\Event\FirstIntegrationEvent;
use Test\Common\Event\SecondIntegrationEvent;
use Test\Common\FixedClock;
use Throwable;

class PublishingIntegrationEventsTest extends KernelTestCase
{
    private FixedClock $clock;
    private TransportInterface $inMemoryTransport;
    private IntegrationEventPublisher $eventPublisher;
    private OutboxMessageRepository $outboxMessageRepository;

    protected function setUp(): void
    {
        $this->clock = $this->getContainer()->get(Clock::class);
        $this->inMemoryTransport = $this->getContainer()->get('messenger.transport.async');
        $this->eventPublisher = $this->getContainer()->get(IntegrationEventPublisher::class);
        $this->outboxMessageRepository = $this->getContainer()->get(OutboxMessageRepository::class);
    }

    /**
     * @test
     */
    public function canPublishIntegrationEvents(): void
    {
        // given
        $this->eventPublisher->storeToOutbox([
            FirstIntegrationEvent::new(EventId::v4()),
            SecondIntegrationEvent::new(EventId::v4())
        ]);
        // and
        $now = $this->now('2005-12-06 17:54:01');

        // when
        $this->eventPublisher->publishNotProcessedYet();

        // then
        $messageList = $this->outboxMessageRepository->findAll();
        self::assertCount(2, $this->inMemoryTransport->get());
        self::assertEquals($now, $messageList[0]->getProcessedAt());
        self::assertEquals($now, $messageList[1]->getProcessedAt());
    }

    /**
     * @test
     */
    public function canFindOnlyNotPublishedIntegrationEvents(): void
    {
        // given
        $this->eventPublisher->storeToOutbox([
            FirstIntegrationEvent::new(EventId::v4()),
            SecondIntegrationEvent::new(EventId::v4())
        ]);
        // and
        $this->eventPublisher->publishNotProcessedYet();
        // and
        $this->eventPublisher->storeToOutbox([
            FirstIntegrationEvent::new(EventId::v4())
        ]);

        // then
        $messageList = $this->outboxMessageRepository->findNotProcessed();
        self::assertCount(1, $messageList);
        self::assertNull($messageList[0]->getProcessedAt());
    }

    /**
     * @test
     */
    public function cannotPublishAnyIntegrationEventsWhenExceptionWasThrown(): void
    {
        // given
        $this->eventPublisher->storeToOutbox([
            FirstIntegrationEvent::new(EventId::v4()),
            BadIntegrationEvent::new(EventId::v4()),
            SecondIntegrationEvent::new(EventId::v4())
        ]);

        // when
        try {
            $this->eventPublisher->publishNotProcessedYet();
        } catch (Throwable $e) {
        }

        // then
        $messageList = $this->outboxMessageRepository->findAll();
        self::assertCount(3, $messageList);
        self::assertNull($messageList[0]->getProcessedAt());
        self::assertNull($messageList[1]->getProcessedAt());
        self::assertNull($messageList[2]->getProcessedAt());
    }

    private function now(string $dateTime): DateTimeImmutable
    {
        return $this->clock->setNow($dateTime);
    }
}
