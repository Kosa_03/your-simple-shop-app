<?php

declare(strict_types=1);

namespace Test\Integration\EventPublisher;

use Common\Clock;
use EventPublisher\Core\EventPublisherInterface;
use EventPublisher\Core\IntegrationEvent\IntegrationEvent;
use EventPublisher\Core\IntegrationEvent\OutboxMessageRepository;
use EventPublisher\Shared\EventId;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Test\Common\Event\AsyncEvent;
use Test\Common\Event\FirstDomainEvent;
use Test\Common\Event\FirstIntegrationEvent;
use Test\Common\Event\SecondIntegrationEvent;
use Test\Common\FixedClock;

class SavingToOutboxIntegrationEventsTest extends KernelTestCase
{
    private FixedClock $clock;
    private EventPublisherInterface $eventPublisher;
    private OutboxMessageRepository $outboxMessageRepository;

    protected function setUp(): void
    {
        $this->clock = $this->getContainer()->get(Clock::class);
        $this->eventPublisher = $this->getContainer()->get(EventPublisherInterface::class);
        $this->outboxMessageRepository = $this->getContainer()->get(OutboxMessageRepository::class);
    }

    /**
     * @test
     */
    public function canSaveToOutboxIntegrationEvent(): void
    {
        // given
        $now = $this->clock->setNow('1997-07-18 12:09:12');
        // and
        $event = FirstIntegrationEvent::new(EventId::v4());

        // when
        $this->eventPublisher->publish([$event]);

        // then
        $messageList = $this->outboxMessageRepository->findAll();
        self::assertCount(1, $messageList);
        $message = $messageList[0];
        self::assertEquals($event->eventId()->toString(), $message->getEventId());
        self::assertInstanceOf(IntegrationEvent::class, $message->getEvent());
        self::assertEquals($now, $message->getOccurredAt());
        self::assertNull($message->getProcessedAt());
    }

    /**
     * @test
     */
    public function canSaveToOutboxOnlyIntegrationEvents(): void
    {
        // given
        $events = [
            FirstIntegrationEvent::new(EventId::v4()),
            AsyncEvent::new(EventId::v4()),
            SecondIntegrationEvent::new(EventId::v4()),
            FirstDomainEvent::new()
        ];

        // when
        $this->eventPublisher->publish($events);

        // then
        self::assertCount(2, $this->outboxMessageRepository->findAll());
    }
}
