<?php

declare(strict_types=1);

namespace Test\Integration\Mail;

use Mail\EmailFactory;
use Mail\InvalidEmailException;
use Mail\Type\EmailInterface;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Test\Integration\Mail\Type\CorrectEmail;
use Test\Integration\Mail\Type\WrongEmail;

class CreateEmailTest extends KernelTestCase
{
    private EmailFactory $emailFactory;

    protected function setUp(): void
    {
        $this->emailFactory = $this->getContainer()->get(EmailFactory::class);
    }

    /**
     * @test
     */
    public function canCreateEmail(): void
    {
        // when
        $email = $this->emailFactory->create(
            CorrectEmail::class,
            ['to@abc.dd' => 'To'],
            ['from@abc.dd' => 'From'],
            'subject'
        );

        // then
        self::assertEquals('to@abc.dd', array_keys($email->sendTo())[0]);
        self::assertEquals('To', array_values($email->sendTo())[0]);
        self::assertEquals('from@abc.dd', array_keys($email->sendFrom())[0]);
        self::assertEquals('From', array_values($email->sendFrom())[0]);
        self::assertEquals('subject', $email->subject());
        self::assertEquals('plain_text', $email->plainEmail());
        self::assertEquals('html_text', $email->htmlEmail());
    }

    /**
     * @test
     */
    public function shouldThrownAnExceptionWhenEmailClassDoesntExists(): void
    {
        // expects
        self::assertExceptionIsThrownBy(
            InvalidEmailException::class,
            '"Abc/Abc" is invalid!',
            fn () => $this->emailFactory->create('Abc/Abc', [], [], 'subject', [])
        );
    }

    /**
     * @test
     */
    public function shouldThrownAnExceptionWhenEmailClassDoesntImplementEmailInterface(): void
    {
        // expects
        self::assertExceptionIsThrownBy(
            InvalidEmailException::class,
            EmailInterface::class,
            fn () => $this->emailFactory->create(WrongEmail::class, [], [], 'subject', [])
        );
    }

    private static function assertExceptionIsThrownBy(string $exception, string $message, callable $callable): void
    {
        $throwable = null;
        try {
            $callable();
        } catch (\Throwable $throwable) {
        }

        self::assertInstanceOf($exception, $throwable);
        self::assertStringContainsString($message, $throwable->getMessage());
    }
}
