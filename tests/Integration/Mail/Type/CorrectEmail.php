<?php

declare(strict_types=1);

namespace Test\Integration\Mail\Type;

use Mail\AbstractEmail;

class CorrectEmail extends AbstractEmail
{
    public function plainEmail(): string
    {
        return 'plain_text';
    }

    public function htmlEmail(): string
    {
        return 'html_text';
    }
}
