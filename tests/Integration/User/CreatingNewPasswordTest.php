<?php

declare(strict_types=1);

namespace Test\Integration\User;

use Common\AppResult;
use Common\Clock;
use Common\Messenger\Command;
use Common\Messenger\CommandBusInterface;
use Common\Messenger\QueryBusInterface;
use DateTimeImmutable;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Test\Common\FixedClock;
use Test\Common\Fixtures;
use User\Application\CreatingNewPassword\CreateNewPassword;
use User\Application\FindingById\FindById;
use User\Domain\Token\TokenGenerator;
use User\Domain\User;
use User\Domain\UserDto;

class CreatingNewPasswordTest extends KernelTestCase
{
    private CommandBusInterface $commandBus;
    private QueryBusInterface $queryBus;
    private FixedClock $clock;
    private Fixtures $fixtures;
    private UserPasswordHasherInterface $passwordHasher;

    protected function setUp(): void
    {
        $this->commandBus = $this->getContainer()->get(CommandBusInterface::class);
        $this->queryBus = $this->getContainer()->get(QueryBusInterface::class);
        $this->clock = $this->getContainer()->get(Clock::class);
        $this->fixtures = $this->getContainer()->get(Fixtures::class);
        $this->passwordHasher = $this->getContainer()->get(UserPasswordHasherInterface::class);
    }

    /**
     * @test
     */
    public function canCreatePasswordWhenContainsAtLeastTenLetters(): void
    {
        // given
        $user = $this->fixtures->anActiveUser('seba', 'seba@kozak.pl', 'sebastian', 'nowak');
        // and
        $randomToken = TokenGenerator::random();
        // and
        $token = $this->fixtures->aRecoveryToken($user, $randomToken, $this->expireInOneDay());

        // when
        $result = $this->runCommand(CreateNewPassword::new($user->getLogin(), $randomToken, '1234567890'));

        // then
        $userDto = $this->findByIdQuery($user->getId());
        self::assertNull($userDto->recoveryToken);
        self::assertFalse($this->passwordHasher->isPasswordValid($user, 'password123'));
        self::assertTrue($this->passwordHasher->isPasswordValid($user, '1234567890'));
        self::assertTrue($result->isSuccessful());
        self::assertEquals('ok', $result->reason());
    }

    /**
     * @test
     */
    public function cannotCreatePasswordWhenItIsTooShort(): void
    {
        // when
        $user = $this->fixtures->anActiveUser('seba', 'seba@kozak.pl', 'sebastian', 'nowak');
        // and
        $randomToken = TokenGenerator::random();
        // and
        $this->fixtures->aRecoveryToken($user, $randomToken, $this->expireInOneDay());

        // when
        $result = $this->runCommand(CreateNewPassword::new($user->getLogin(), $randomToken, 'new45pass'));

        // then
        self::assertTrue($this->passwordHasher->isPasswordValid($user, 'password123'));
        self::assertFalse($this->passwordHasher->isPasswordValid($user, 'new45pass'));
        self::assertTrue($result->isFailure());
        self::assertEquals(User::REASON_PASSWORD_TOO_SHORT, $result->reason());
    }

    /**
     * @test
     */
    public function cannotCreatePasswordWhenUserNotFound(): void
    {
        // when
        $result = $this->runCommand(CreateNewPassword::new('user1', 'token123', 'new_password'));

        // then
        self::assertTrue($result->isFailure());
        self::assertEquals(User::REASON_USER_NOT_FOUND, $result->reason());
    }

    /**
     * @test
     */
    public function cannotCreateNewPasswordWithoutToken(): void
    {
        // given
        $user = $this->fixtures->anActiveUser('seba', 'seba@kozak.pl', 'sebastian', 'nowak');
        // and
        $token = $user->getRecoveryToken();

        // when
        $result = $this->runCommand(CreateNewPassword::new($user->getLogin(), 'token123', 'new_password'));

        // then
        self::assertNull($token);
        self::assertTrue($this->passwordHasher->isPasswordValid($user, 'password123'));
        self::assertTrue($result->isFailure());
        self::assertEquals(User::REASON_NON_EXISTING_TOKEN, $result->reason());
    }

    /**
     * @test
     */
    public function cannotCreatePasswordWhenTokenIsExpired(): void
    {
        // when
        $user = $this->fixtures->anActiveUser('seba', 'seba@kozak.pl', 'sebastian', 'nowak');
        // and
        $randomToken = TokenGenerator::random();
        // and
        $this->fixtures->aRecoveryToken($user, $randomToken, $this->clock->setNow('-10 minutes'));

        // when
        $result = $this->runCommand(CreateNewPassword::new($user->getLogin(), $randomToken, 'new_password'));

        // then
        $userDto = $this->findByIdQuery($user->getId());
        self::assertNull($userDto->recoveryToken);
        self::assertTrue($this->passwordHasher->isPasswordValid($user, 'password123'));
        self::assertFalse($this->passwordHasher->isPasswordValid($user, 'new_password'));
        self::assertTrue($result->isFailure());
        self::assertEquals(User::REASON_EXPIRED_TOKEN, $result->reason());
    }

    /**
     * @test
     */
    public function cannotCreatePasswordWhenTokenIsIncorrect(): void
    {
        // when
        $user = $this->fixtures->anActiveUser('seba', 'seba@kozak.pl', 'sebastian', 'nowak');
        // and
        $randomToken = TokenGenerator::random();
        // and
        $token = $this->fixtures->aRecoveryToken($user, $randomToken, $this->expireInOneDay());

        // when
        $result = $this->runCommand(CreateNewPassword::new($user->getLogin(), 'token123', 'new_password'));

        // then
        self::assertTrue($this->passwordHasher->isPasswordValid($user, 'password123'));
        self::assertFalse($this->passwordHasher->isPasswordValid($user, 'new_password'));
        self::assertTrue($result->isFailure());
        self::assertEquals(User::REASON_INCORRECT_TOKEN, $result->reason());
    }

    private function runCommand(Command $command): AppResult
    {
        return $this->commandBus->dispatch($command);
    }

    private function findByIdQuery(int $userId): UserDto
    {
        return $this->queryBus->run(FindById::new($userId));
    }

    private function expireInOneDay(): DateTimeImmutable
    {
        return $this->clock->now('+24 hours');
    }
}
