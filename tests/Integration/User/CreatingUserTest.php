<?php

declare(strict_types=1);

namespace Test\Integration\User;

use Common\Messenger\Command;
use Common\Messenger\CommandBusInterface;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use User\Application\CreatingUser\CreateUser;
use User\Domain\UserDto;
use User\Infrastructure\UserRepository;

class CreatingUserTest extends KernelTestCase
{
    private CommandBusInterface $commandBus;
    private UserRepository $userRepository;

    protected function setUp(): void
    {
        $this->commandBus = $this->getContainer()->get(CommandBusInterface::class);
        $this->userRepository = $this->getContainer()->get(UserRepository::class);
    }

    /**
     * @test
     */
    public function canCreateUser(): void
    {
        // when
        $this->runCommand(CreateUser::new($this->userDto()));

        // then
        $user = $this->userRepository->findOneBy([]);
        self::assertEquals('sebasty', $user->getLogin());
        self::assertEquals('wpierdol@tuiteraz.dd', $user->getEmail());
        self::assertEquals('sebastian', $user->getFirstName());
        self::assertEquals('nowak', $user->getLastName());
        self::assertNotNull($user->getPassword());
        self::assertEquals(['ROLE_USER'], $user->getRoles());
        self::assertTrue($user->isActive());
    }

    private function userDto(): UserDto
    {
        return UserDto::create(
            'sebasty',
            'wpierdol@tuiteraz.dd',
            'sebastian',
            'nowak',
            'xyz123456zaq',
            ['ROLE_USER'],
            true
        );
    }

    private function runCommand(Command $command): void
    {
        $this->commandBus->dispatch($command);
    }
}
