<?php

declare(strict_types=1);

namespace Test\Integration\User;

use Common\Messenger\Command;
use Common\Messenger\CommandBusInterface;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Test\Common\Fixtures;
use User\Application\DeletingUser\DeleteUser;
use User\Infrastructure\UserRepository;

class DeletingUserTest extends KernelTestCase
{
    private CommandBusInterface $commandBus;
    private Fixtures $fixtures;
    private UserRepository $userRepository;

    protected function setUp(): void
    {
        $this->commandBus = $this->getContainer()->get(CommandBusInterface::class);
        $this->fixtures = $this->getContainer()->get(Fixtures::class);
        $this->userRepository = $this->getContainer()->get(UserRepository::class);
    }

    /**
     * @test
     */
    public function canDeleteAUser(): void
    {
        // given
        $user = $this->fixtures->anActiveUser('xx_seba_xx', 'wpierdol@tuiteraz.dd', 'sebastian', 'dupny');
        // and
        $userId = $user->getId();

        // when
        $this->runCommand(DeleteUser::new($userId));

        // then
        self::assertNull($this->userRepository->find($userId));
    }

    /**
     * @test
     */
    public function canDeleteANonExistingUser(): void
    {
        // when
        $this->runCommand(DeleteUser::new(-1));

        // then
        self::assertNull($this->userRepository->find(-1));
    }

    private function runCommand(Command $command): void
    {
        $this->commandBus->dispatch($command);
    }
}
