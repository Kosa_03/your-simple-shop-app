<?php

declare(strict_types=1);

namespace Test\Integration\User\Domain;

use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Throwable;
use User\Domain\Password;
use User\Domain\User;

class PasswordTest extends KernelTestCase
{
    private UserPasswordHasherInterface $passwordHasher;

    protected function setUp(): void
    {
        $this->passwordHasher = $this->getContainer()->get(UserPasswordHasherInterface::class);
    }

    /**
     * @test
     */
    public function canCreatePassword(): void
    {
        // when
        $password = Password::create(new User(), $this->passwordHasher, 'password10');

        // then
        self::assertInstanceOf(Password::class, $password);
    }

    /**
     * @test
     */
    public function cannotCreateWhenPasswordIsTooShort(): void
    {
        // expects
        self::assertExceptionIsThrownBy(\InvalidArgumentException::class,
            fn() => Password::create(new User(), $this->passwordHasher, 'password1')
        );
    }

    private static function assertExceptionIsThrownBy(string $exception, callable $callable): void
    {
        $throwable = null;
        try {
            $callable();
        } catch (Throwable $throwable) {
        }

        self::assertInstanceOf($exception, $throwable);
    }
}
