<?php

declare(strict_types=1);

namespace Test\Integration\User;

use Common\Messenger\QueryBusInterface;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Test\Common\Fixtures;
use User\Application\FindingById\FindById;
use User\Domain\UserDto;

class FindingByIdTest extends KernelTestCase
{
    private Fixtures $fixtures;
    private QueryBusInterface $queryBus;

    protected function setUp(): void
    {
        $this->fixtures = $this->getContainer()->get(Fixtures::class);
        $this->queryBus = $this->getContainer()->get(QueryBusInterface::class);
    }

    /**
     * @test
     */
    public function canFindAUserById(): void
    {
        // given
        $user = $this->fixtures->anActiveUser('login', 'email@bb.cc', 'name', 'last');

        // when
        $dto = $this->findByIdQuery($user->getId());

        // then
        self::assertInstanceOf(UserDto::class, $dto);
    }

    /**
     * @test
     */
    public function shouldReturnNullWhenAUserWasNotFound(): void
    {
        // when
        $dto = $this->findByIdQuery(-1);

        // then
        self::assertNull($dto);
    }

    private function findByIdQuery(int $userId): ?UserDto
    {
        return $this->queryBus->run(FindById::new($userId));
    }
}
