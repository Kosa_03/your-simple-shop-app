<?php

declare(strict_types=1);

namespace Test\Integration\User;

use Common\Messenger\QueryBusInterface;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Test\Common\Fixtures;
use User\Application\FindingByLogin\FindByLogin;
use User\Domain\UserDto;

class FindingByLoginTest extends KernelTestCase
{
    private Fixtures $fixtures;
    private QueryBusInterface $queryBus;

    protected function setUp(): void
    {
        $this->fixtures = $this->getContainer()->get(Fixtures::class);
        $this->queryBus = $this->getContainer()->get(QueryBusInterface::class);
    }

    /**
     * @test
     */
    public function canFindAUserByLogin(): void
    {
        // given
        $this->fixtures->anActiveUser('seba', 'seba@kozak.pl', 'sebastian', 'nowak');

        // when
        $dto = $this->findByLoginQuery('seba');

        // then
        self::assertEquals('seba', $dto->login);
        self::assertEquals('seba@kozak.pl', $dto->email);
    }

    /**
     * @test
     */
    public function shouldReturnNullWhenAUserWasNotFound(): void
    {
        // when
        $dto = $this->findByLoginQuery('invalid_login');

        // then
        self::assertNull($dto);
    }

    private function findByLoginQuery(string $login): ?UserDto
    {
        return $this->queryBus->run(FindByLogin::new($login));
    }
}
