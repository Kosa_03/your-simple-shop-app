<?php

declare(strict_types=1);

namespace Test\Integration\User;

use Common\AppResult;
use Common\Clock;
use Common\Messenger\Command;
use Common\Messenger\CommandBusInterface;
use Common\Messenger\QueryBusInterface;
use DateTimeImmutable;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Test\Common\FixedClock;
use Test\Common\Fixtures;
use User\Application\FindingById\FindById;
use User\Application\RecoveringPassword\RecoverPassword;
use User\Domain\Token\TokenGenerator;
use User\Domain\User;
use User\Domain\UserDto;

class RecoveringPasswordTest extends KernelTestCase
{
    private CommandBusInterface $commandBus;
    private QueryBusInterface $queryBus;
    private FixedClock $clock;
    private Fixtures $fixtures;

    protected function setUp(): void
    {
        $this->commandBus = $this->getContainer()->get(CommandBusInterface::class);
        $this->queryBus = $this->getContainer()->get(QueryBusInterface::class);
        $this->clock = $this->getContainer()->get(Clock::class);
        $this->fixtures = $this->getContainer()->get(Fixtures::class);
    }

    /**
     * @test
     */
    public function canCreateRecoveryTokenWhenUserHasntAnyTokenYet(): void
    {
        // given
        $user = $this->fixtures->anActiveUser('seba', 'seba@kozak.pl', 'sebastian', 'nowak');
        // and
        $tokenBefore = $user->getRecoveryToken();
        // and
        $now = $this->clock->setNow('1995-09-14 19:17:21');

        // when
        $result = $this->runCommand(RecoverPassword::new('seba', 'seba@kozak.pl'));

        // then
        $userDto = $this->findByIdQuery($user->getId());
        self::assertTrue($result->isSuccessful());
        self::assertNull($tokenBefore);
        self::assertNotNull($userDto->recoveryToken);
        self::assertEquals($now->modify('+24 hours'), $userDto->recoveryToken->expiryAt);
    }

    /**
     * @test
     */
    public function canCreateRecoveryTokenWhenItIsExpired(): void
    {
        // given
        $user = $this->fixtures->anActiveUser('seba', 'seba@kozak.pl', 'sebastian', 'nowak');
        // and
        $before = clone $this->fixtures->aRecoveryToken($user, 'token_abc', $this->expiredTenMinutesAgo());
        // and
        $now = $this->clock->setNow('+5 minutes');

        // when
        $result = $this->runCommand(RecoverPassword::new('seba', 'seba@kozak.pl'));

        // then
        $after = $this->findByIdQuery($user->getId());
        self::assertTrue($result->isSuccessful());
        self::assertNotEquals($before->getToken(), $after->recoveryToken->token);
        self::assertEquals($now->modify('+24 hours'), $after->recoveryToken->expiryAt);
    }

    /**
     * @test
     */
    public function cannotCreateRecoveryTokenWhenUserWasNotFound(): void
    {
        // when
        $result = $this->runCommand(RecoverPassword::new('login', 'email'));

        // then
        self::assertTrue($result->isFailure());
        self::assertEquals(User::REASON_USER_NOT_FOUND, $result->reason());
    }

    /**
     * @test
     */
    public function cannotCreateNewRecoveryTokenWhenItExistsAndIsActive(): void
    {
        // given
        $user = $this->fixtures->anActiveUser('seba', 'seba@kozak.pl', 'sebastian', 'nowak');
        // and
        $token = TokenGenerator::random();
        // and
        $nonExpired = $this->nonExpired();
        // and
        $this->fixtures->aRecoveryToken($user, $token, $nonExpired);

        // when
        $result = $this->runCommand(RecoverPassword::new('seba', 'seba@kozak.pl'));

        // then
        $userDto = $this->findByIdQuery($user->getId());
        self::assertTrue($result->isFailure());
        self::assertEquals(User::REASON_ACTIVE_TOKEN, $result->reason());
        self::assertEquals($token, $userDto->recoveryToken->token);
        self::assertEquals($nonExpired, $userDto->recoveryToken->expiryAt);
    }

    private function findByIdQuery(int $userId): UserDto
    {
        return $this->queryBus->run(FindById::new($userId));
    }

    private function runCommand(Command $command): AppResult
    {
        return $this->commandBus->dispatch($command);
    }

    private function nonExpired(): DateTimeImmutable
    {
        return $this->clock->setNow('+13 hours');
    }

    private function expiredTenMinutesAgo(): DateTimeImmutable
    {
        return $this->clock->setNow('-10 minutes');
    }
}
