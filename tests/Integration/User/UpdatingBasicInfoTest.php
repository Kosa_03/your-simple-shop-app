<?php

declare(strict_types=1);

namespace Test\Integration\User;

use Common\AppResult;
use Common\Clock;
use Common\Messenger\Command;
use Common\Messenger\CommandBusInterface;
use Common\Messenger\QueryBusInterface;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Test\Common\FixedClock;
use Test\Common\Fixtures;
use User\Application\FindingById\FindById;
use User\Application\UpdatingBasicInfo\UpdateBasicInfo;
use User\Domain\Roles;
use User\Domain\UserDto;

class UpdatingBasicInfoTest extends KernelTestCase
{
    private CommandBusInterface $commandBus;
    private QueryBusInterface $queryBus;
    private FixedClock $clock;
    private Fixtures $fixtures;

    protected function setUp(): void
    {
        $this->commandBus = $this->getContainer()->get(CommandBusInterface::class);
        $this->queryBus = $this->getContainer()->get(QueryBusInterface::class);
        $this->clock = $this->getContainer()->get(Clock::class);
        $this->fixtures = $this->getContainer()->get(Fixtures::class);
    }

    /**
     * @test
     */
    public function canUpdateBasicInfo(): void
    {
        // when
        $user = $this->fixtures->anActiveUser('seba', 'seba@kozak.pl', 'sebastian', 'nowak');
        // and
        $now = $this->clock->setNow('1990-10-10 09:00:01');

        // when
        $this->runCommand(UpdateBasicInfo::new($user->getId(), 'jessica', 'nowak', [Roles::ROLE_ADMIN], false));

        // then
        $userDto = $this->findByIdQuery($user->getId());
        self::assertEquals('jessica', $userDto->firstName);
        self::assertEquals('nowak', $userDto->lastName);
        self::assertEquals([Roles::ROLE_ADMIN], $userDto->roles);
        self::assertFalse($userDto->active);
        self::assertEquals($now, $userDto->updatedAt);
    }

    /**
     * @test
     */
    public function canBeSuccessfulWhenUserIsNotFound(): void
    {
        // when
        $result = $this->runCommand(UpdateBasicInfo::new(-1, 'first', 'last', [Roles::ROLE_USER], false));

        // then
        self::assertTrue($result->isSuccessful());
    }

    private function findByIdQuery(int $userId): UserDto
    {
        return $this->queryBus->run(FindById::new($userId));
    }

    private function runCommand(Command $command): AppResult
    {
        return $this->commandBus->dispatch($command);
    }
}
