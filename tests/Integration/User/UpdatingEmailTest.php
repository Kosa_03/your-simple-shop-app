<?php

declare(strict_types=1);

namespace Test\Integration\User;

use Common\AppResult;
use Common\Clock;
use Common\Messenger\Command;
use Common\Messenger\CommandBusInterface;
use Common\Messenger\QueryBusInterface;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Test\Common\FixedClock;
use Test\Common\Fixtures;
use User\Application\FindingById\FindById;
use User\Application\UpdatingEmail\UpdateEmail;
use User\Domain\UserDto;

class UpdatingEmailTest extends KernelTestCase
{
    private CommandBusInterface $commandBus;
    private QueryBusInterface $queryBus;
    private FixedClock $clock;
    private Fixtures $fixtures;

    protected function setUp(): void
    {
        $this->commandBus = $this->getContainer()->get(CommandBusInterface::class);
        $this->queryBus = $this->getContainer()->get(QueryBusInterface::class);
        $this->clock = $this->getContainer()->get(Clock::class);
        $this->fixtures = $this->getContainer()->get(Fixtures::class);
    }

    /**
     * @test
     */
    public function canUpdateEmail(): void
    {
        // when
        $user = $this->fixtures->anActiveUser('seba', 'seba@kozak.pl', 'sebastian', 'nowak');
        // and
        $now = $this->clock->setNow('1990-10-10 09:00:01');

        // when
        $this->runCommand(UpdateEmail::new($user->getId(), 'seba@maslak.uk'));

        // then
        $dto = $this->findByIdQuery($user->getId());
        self::assertEquals('seba@maslak.uk', $dto->email);
        self::assertEquals($now, $dto->updatedAt);
    }

    /**
     * @test
     */
    public function canBeSuccessfulWhenUserIsNotFound(): void
    {
        // when
        $result = $this->runCommand(UpdateEmail::new(-1, 'fake-email@abc.dd'));

        // then
        self::assertTrue($result->isSuccessful());
    }

    private function findByIdQuery(int $userId): UserDto
    {
        return $this->queryBus->run(FindById::new($userId));
    }

    private function runCommand(Command $command): AppResult
    {
        return $this->commandBus->dispatch($command);
    }
}
