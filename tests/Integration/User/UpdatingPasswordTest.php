<?php

declare(strict_types=1);

namespace Test\Integration\User;

use Common\AppResult;
use Common\Clock;
use Common\Messenger\Command;
use Common\Messenger\CommandBusInterface;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Test\Common\FixedClock;
use Test\Common\Fixtures;
use User\Application\UpdatingPassword\UpdatePassword;

class UpdatingPasswordTest extends KernelTestCase
{
    private CommandBusInterface $commandBus;
    private Fixtures $fixtures;
    private FixedClock $clock;
    private UserPasswordHasherInterface $passwordHasher;

    protected function setUp(): void
    {
        $this->commandBus = $this->getContainer()->get(CommandBusInterface::class);
        $this->clock = $this->getContainer()->get(Clock::class);
        $this->fixtures = $this->getContainer()->get(Fixtures::class);
        $this->passwordHasher = $this->getContainer()->get(UserPasswordHasherInterface::class);
    }

    /**
     * @test
     */
    public function canUpdatePassword(): void
    {
        // when
        $user = $this->fixtures->anActiveUser('seba', 'seba@kozak.pl', 'sebastian', 'nowak');
        // and
        $now = $this->clock->setNow('1995-03-12 15:21:23');

        // when
        $this->runCommand(UpdatePassword::new($user->getId(), 'kasia_milosci_moja_<3'));

        // then
        self::assertFalse($this->passwordHasher->isPasswordValid($user, 'password123'));
        self::assertTrue($this->passwordHasher->isPasswordValid($user, 'kasia_milosci_moja_<3'));
        self::assertEquals($now, $user->getUpdatedAt());
    }

    /**
     * @test
     */
    public function canBeSuccessfulWhenUserIsNotFound(): void
    {
        // when
        $result = $this->runCommand(UpdatePassword::new(-1, 'new_password22'));

        // then
        self::assertTrue($result->isSuccessful());
    }
    
    private function runCommand(Command $command): AppResult
    {
        return $this->commandBus->dispatch($command);
    }
}
