<?php

declare(strict_types=1);

namespace Test\Unit\AccountOlx;

use AccountOlx\Domain\AccessToken;
use Monolog\Test\TestCase;

class AccessTokenTest extends TestCase
{
    /**
     * @test
     */
    public function canCreateEmptyAccessToken(): void
    {
        // when
        $token = AccessToken::empty();

        // then
        self::assertNull($token->getToken());
        self::assertNull($token->getExpiryAt());
    }

    /**
     * @test
     */
    public function canCreateAccessTokenWithSpecificValues(): void
    {
        // given
        $expiryAt = new \DateTimeImmutable('+24 hours');

        // when
        $token = AccessToken::create('my_token', $expiryAt);

        // then
        self::assertEquals('my_token', $token->getToken());
        self::assertEquals($expiryAt, $token->getExpiryAt());
    }
}
