<?php

declare(strict_types=1);

namespace Test\Unit\AccountOlx;

use AccountOlx\Domain\RefreshToken;
use PHPUnit\Framework\TestCase;

class RefreshTokenTest extends TestCase
{
    /**
     * @test
     */
    public function canCreateEmptyRefreshToken(): void
    {
        // when
        $token = RefreshToken::empty();

        // then
        self::assertNull($token->getToken());
    }

    /**
     * @test
     */
    public function canCreateRefreshTokenWithSpecificValue(): void
    {
        // when
        $token = RefreshToken::create('my_awesome_token_666');

        // then
        self::assertEquals('my_awesome_token_666', $token->getToken());
    }

    /**
     * @test
     */
    public function canCreateRefreshTokenWithNullValue(): void
    {
        // when
        $token = RefreshToken::create(null);

        // then
        self::assertNull($token->getToken());
    }
}
