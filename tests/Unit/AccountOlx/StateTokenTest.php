<?php

declare(strict_types=1);

namespace Test\Unit\AccountOlx;

use AccountOlx\Domain\StateToken;
use PHPUnit\Framework\TestCase;

class StateTokenTest extends TestCase
{
    /**
     * @test
     */
    public function canCreateEmptyStateToken(): void
    {
        // when
        $token = StateToken::empty();

        // then
        self::assertNull($token->getToken());
        self::assertNull($token->getCreatedAt());
    }

    /**
     * @test
     */
    public function canCreateStateToken(): void
    {
        // given
        $createdAt = new \DateTimeImmutable();

        // when
        $token = StateToken::create('random_token', $createdAt);

        // then
        self::assertEquals('random_token', $token->getToken());
        self::assertEquals($createdAt, $token->getCreatedAt());
    }
}
