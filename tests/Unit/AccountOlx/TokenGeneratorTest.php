<?php

declare(strict_types=1);

namespace Test\Unit\AccountOlx;

use AccountOlx\Domain\Token\TokenGenerator;
use PHPUnit\Framework\TestCase;

class TokenGeneratorTest extends TestCase
{
    /**
     * @test
     */
    public function canGenerateRandomToken(): void
    {
        // when
        $token = TokenGenerator::random();

        // then
        self::assertEquals(10, strlen($token));
    }
}
