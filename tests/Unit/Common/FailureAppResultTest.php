<?php

declare(strict_types=1);

namespace Test\Unit\Common;

use Common\AppResult;
use PHPUnit\Framework\TestCase;

class FailureAppResultTest extends TestCase
{
    /**
     * @test
     */
    public function successfulShouldReturnFalseWhenFailure(): void
    {
        // given
        $result = AppResult::failure('reason');

        // expects
        self::assertFalse($result->isSuccessful());
    }

    /**
     * @test
     */
    public function failureShouldReturnTrueWhenFailure(): void
    {
        // given
        $result = AppResult::failure('reason');

        // expects
        self::assertTrue($result->isFailure());
    }

    /**
     * @test
     */
    public function canReturnSpecificReason(): void
    {
        // given
        $result = AppResult::failure('reason');

        // expects
        self::assertEquals('reason', $result->reason());
    }
}
