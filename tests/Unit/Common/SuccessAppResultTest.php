<?php

declare(strict_types=1);

namespace Test\Unit\Common;

use Common\AppResult;
use PHPUnit\Framework\TestCase;

class SuccessAppResultTest extends TestCase
{
    /**
     * @test
     */
    public function successfulShouldReturnTrueWhenSuccess(): void
    {
        // given
        $result = AppResult::success();

        // expects
        self::assertTrue($result->isSuccessful());
    }

    /**
     * @test
     */
    public function failureShouldReturnFalseWhenSuccess(): void
    {
        // given
        $result = AppResult::success();

        // expects
        self::assertFalse($result->isFailure());
    }

    /**
     * @test
     */
    public function canReturnSuccessfulReason(): void
    {
        // given
        $result = AppResult::success();

        // expects
        self::assertEquals('ok', $result->reason());
    }
}
