<?php

declare(strict_types=1);

namespace Test\Unit\EventPublisher;

use EventPublisher\Shared\EventId;
use PHPUnit\Framework\TestCase;

class EventIdTest extends TestCase
{
    /**
     * @test
     */
    public function canCreateEventIdWithRandomUuidV4(): void
    {
        // given
        $eventId = EventId::v4();

        // expects
        self::assertEquals(36, strlen($eventId->toString()));
    }

    /**
     * @test
     */
    public function shouldReturnTrueWhenEventIdsAreEquals(): void
    {
        // given
        $token = EventId::v4();

        // expects
        self::assertTrue($token->isEquals($token));
    }

    /**
     * @test
     */
    public function canReturnFalseWhenEventIdsAreNotEquals(): void
    {
        // given
        $token1 = EventId::v4();
        // and
        $token2 = EventId::v4();

        // expects
        self::assertFalse($token1->isEquals($token2));
    }
}
