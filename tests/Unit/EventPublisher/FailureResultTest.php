<?php

declare(strict_types=1);

namespace Test\Unit\EventPublisher;

use EventPublisher\Shared\Result;
use PHPUnit\Framework\TestCase;

class FailureResultTest extends TestCase
{
    /**
     * @test
     */
    public function shouldReturnEmptyListWhenFailure(): void
    {
        // when
        $result = Result::failure('reason');

        // then
        self::assertCount(0, $result->events());
    }

    /**
     * @test
     */
    public function shouldReturnReasonWhenFailure(): void
    {
        // when
        $result = Result::failure('Something went wrong...');

        // then
        self::assertEquals('Something went wrong...', $result->reason());
    }

    /**
     * @test
     */
    public function shouldReturnTrueWhenFailure(): void
    {
        // when
        $result = Result::failure('reason');

        // then
        self::assertTrue($result->isFailure());
    }

    /**
     * @test
     */
    public function shouldReturnFalseWhenFailure(): void
    {
        // when
        $result = Result::failure('reason');

        // then
        self::assertFalse($result->isSuccessful());
    }
}
