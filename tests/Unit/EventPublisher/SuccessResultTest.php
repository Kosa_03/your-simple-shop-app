<?php

declare(strict_types=1);

namespace Test\Unit\EventPublisher;

use EventPublisher\Shared\EventId;
use EventPublisher\Shared\Result;
use PHPUnit\Framework\TestCase;
use Test\Common\Event\AsyncEvent;
use Test\Common\Event\FirstDomainEvent;
use Test\Common\Event\FirstIntegrationEvent;
use Test\Common\Event\SecondIntegrationEvent;

class SuccessResultTest extends TestCase
{
    /**
     * @test
     */
    public function shouldReturnDomainAndIntegrationEventsWhenSuccessful(): void
    {
        // when
        $result = Result::success([
            FirstIntegrationEvent::new(EventId::v4()),
            AsyncEvent::new(EventId::v4()),
            SecondIntegrationEvent::new(EventId::v4()),
            FirstDomainEvent::new()
        ]);

        // then
        self::assertCount(3, $result->events());
    }

    /**
     * @test
     */
    public function shouldReturnSuccessReasonWhenSuccessful(): void
    {
        // when
        $result = Result::success([
            FirstIntegrationEvent::new(EventId::v4())
        ]);

        // then
        self::assertEquals('OK', $result->reason());
    }

    /**
     * @test
     */
    public function shouldReturnTrueWhenSuccessful(): void
    {
        // when
        $result = Result::success([
            FirstIntegrationEvent::new(EventId::v4())
        ]);

        // then
        self::assertTrue($result->isSuccessful());
    }

    /**
     * @test
     */
    public function shouldReturnFalseWhenSuccessful(): void
    {
        // when
        $result = Result::success([
            FirstIntegrationEvent::new(EventId::v4())
        ]);

        // then
        self::assertFalse($result->isFailure());
    }
}
