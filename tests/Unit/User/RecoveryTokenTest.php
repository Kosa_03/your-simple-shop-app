<?php

declare(strict_types=1);

namespace Test\Unit\User;

use DateTimeImmutable;
use PHPUnit\Framework\TestCase;
use User\Domain\RecoveryToken;
use User\Domain\Token\TokenGenerator;
use User\Domain\User;

class RecoveryTokenTest extends TestCase
{
    /**
     * @test
     */
    public function canCreateRecoveryToken(): void
    {
        // given
        $expiryAt = new DateTimeImmutable('+24 hours');
        // and
        $randomToken = TokenGenerator::random();
        // and
        $token = RecoveryToken::create(new User(), $randomToken, $expiryAt);

        // then
        self::assertNotNull($token->getToken());
        self::assertEquals($randomToken, $token->getToken());
        self::assertEquals($expiryAt, $token->getExpiryAt());
    }

    /**
     * @test
     */
    public function isExpiredShouldBeTrueWhenTokenIsExpired(): void
    {
        // given
        $randomToken = TokenGenerator::random();
        // and
        $token = RecoveryToken::create(new User(), $randomToken, new DateTimeImmutable('-10 minutes'));

        // expect
        self::assertTrue($token->isExpired());
    }

    /**
     * @test
     */
    public function isExpiredShouldBeFalseWhenTokenIsActive(): void
    {
        // given
        $randomToken = TokenGenerator::random();
        // and
        $token = RecoveryToken::create(new User(), $randomToken, new DateTimeImmutable('+24 hours'));

        // expect
        self::assertFalse($token->isExpired());
    }

    /**
     * @test
     */
    public function isCorrectShouldBeTrueWhenTokenIsActiveAndCorrect(): void
    {
        // given
        $randomToken = TokenGenerator::random();
        // and
        $token = RecoveryToken::create(new User(), $randomToken, new DateTimeImmutable('+24 hours'));

        // expect
        self::assertTrue($token->isCorrect($randomToken));
    }

    /**
     * @test
     */
    public function isCorrectShouldBeFalseWhenTokenIsActiveAndInvalid(): void
    {
        // given
        $randomToken = TokenGenerator::random();
        // and
        $token = RecoveryToken::create(new User(), $randomToken, new DateTimeImmutable('+24 hours'));

        // expect
        self::assertFalse($token->isCorrect('invalid'));
    }

    /**
     * @test
     */
    public function isCorrectShouldBeFalseWhenTokenIsExpiredAndCorrect(): void
    {
        // given
        $randomToken = TokenGenerator::random();
        // and
        $token = RecoveryToken::create(new User(), $randomToken, new DateTimeImmutable('-15 minutes'));

        // expect
        self::assertFalse($token->isCorrect($randomToken));
    }
}
