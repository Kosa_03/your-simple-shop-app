<?php

declare(strict_types=1);

namespace Test\Unit\User;

use PHPUnit\Framework\TestCase;
use User\Domain\Token\TokenGenerator;

class TokenGeneratorTest extends TestCase
{
    /**
     * @test
     */
    public function canGenerateRandomToken(): void
    {
        // when
        $token = TokenGenerator::random();

        // then
        self::assertEquals(10, strlen($token));
    }
}
